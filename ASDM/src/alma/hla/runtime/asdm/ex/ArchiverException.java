/*
 * Created on 13th of February. 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package alma.hla.runtime.asdm.ex;

/**
 * 
 * The exception thrown by all methods of <code>Archiver</code>.
 *  
 * @author caillat
 *
 */
public class ArchiverException extends Exception {

	/**
	 * 
	 */
	public ArchiverException() {
		super();
	}

	/**
	 * @param message
	 */
	public ArchiverException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ArchiverException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ArchiverException(String message, Throwable cause) {
		super(message, cause);
	}

}
