/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File UTCCorrection.java
 */
package alma.hla.runtime.asdm.types;

/**
 * The UTCCorrestion class is used in the table of corrections to obtain UTC time
 * from TAI time.  A UTCCorrection instance has the Julian day on which a leap second
 * was added to UTC time and the cumulative number of added leap seconds at that time. 
 * 
 * @version 1.00 Nov 16, 2004
 * @author Allen Farris
 */
class UTCCorrection {
	private double jd;
	private double taiMinusUTC;
	
	/**
	 * Create an instance of a UTCCorrection.
	 * @param jd The Julian day on which the leap second was added.
	 * @param taiMinusUTC The new correction (TAI - UTC) that applies from this time forward.
	 */
	public UTCCorrection(double jd, double taiMinusUTC) {
			this.jd = jd;
			this.taiMinusUTC = taiMinusUTC;
		}

	/**
	 * Return the Julian day at which the UTC correction changed.
	 * @return The Julian day at which the UTC correction changed.
	 */
	public double getJD() {
		return jd;
	}
	
	/**
	 * Return the cumulative correction ((TAI - UTC) that applies from this time forward.
	 * @return The cumulative correction ((TAI - UTC) that applies from this time forward.
	 */
	public double getTAIMinusUTC() {
		return taiMinusUTC;
	}
	
}
