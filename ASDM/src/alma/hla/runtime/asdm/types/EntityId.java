/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File EntityId.java
 */
package alma.hla.runtime.asdm.types;

import alma.hla.runtime.util.BODataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import alma.asdmIDLTypes.IDLEntityId;


/**
 * description
 * 
 * @version 1.00 Nov 24, 2004
 * @author Allen Farris
 */
public class EntityId {
	static final String ALMAbasicUIDRegex = "^[uU][iI][dD]://[0-9a-zA-Z]+(/[xX][0-9a-fA-F]+){2}(#\\w{1,}){0,}$";
	static final String EVLAbasicUIDRegex = "^[uU][iI][dD]:///?evla/(bdf|sdm)/.+$";

	static public String validate(String x) {
		if ( x.matches(ALMAbasicUIDRegex) || x.matches(EVLAbasicUIDRegex) ) return null;
		return ("The UID '"+x+"' is not wellformed");
	}
	
	static public EntityId getEntityId(StringTokenizer t) {
		try {
			String value = t.nextToken();
			return new EntityId (value);
		} catch (NoSuchElementException e) {
		}
		return null;
	}
	
	private String id;
	    
	public EntityId(EntityId e) {
	    this.id = new String(e.id);   
    }
    
    public EntityId(String id) {
		String msg = validate(id);
		if (msg != null)
			throw new java.lang.IllegalArgumentException(msg);
		this.id = id;
	}
	
	public EntityId(IDLEntityId id) {
		this.id = id.value;
	}

	public String toString() {
		return id;
	}
	
	/**
	 * Write the binary representation of this into a DataOutput stream
	 * @throws IOException 
	 */
	public void toBin(DataOutputStream dos) throws IOException {
		dos.writeInt(id.length());
		dos.writeBytes(id);
	}
	
	/**
	 * Read the binary representation of an EntityId from a BODataInputStream
	 * and use the read value to set an  EntityId.
	 * @param dis the BODataInputStream to be read
	 * @return an EntityId
	 * @throws IOException
	 */
	public static EntityId fromBin(BODataInputStream dis) throws IOException {
		StringBuffer sb = new StringBuffer();
		int len = dis.readInt();
		for (int i = 0; i < len; i++)
			sb.append((char) dis.readByte());
		return  new EntityId(sb.toString());
	}

	public IDLEntityId toIDLEntityId() {
		return new IDLEntityId(id);
	}

    /**
     * Return true if and only if the specified object o is an
     * EntityId and its value is equal to this EntityId.
     * @param o The object to which this interval is being compared.
     * @return true if and only if the specified object o is an
     * EntityId and its value is equal to the one of this EntityId.
     */
    public boolean equals(Object o) {
        if (!(o instanceof EntityId))
            return false;
        return this.id.equals(((EntityId)o).id);
    }
}
