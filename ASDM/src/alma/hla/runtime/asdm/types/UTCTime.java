/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File UTCTime.java
 */
package alma.hla.runtime.asdm.types;

import alma.asdmIDLTypes.IDLArrayTime;

/**
 * The UTCTime class implements the concept of a point in time, implemented
 * as an Interval of time since 15 October 1582 00:00:00 UTC.  Internally 
 * the time is kept in units of 10 nanoseconds (10<sup>-8</sup> seconds).
 * This class is dependent for its implementation on the ArrayTime class;
 * internally, it maintains an ArrayTime object that it uses to implement
 * its methods.
 * 
 * @version 1.00 Nov 16, 2004
 * @author Allen Farris
 */
public class UTCTime {
	
	/**
	 * Return true if the specified year is a leap year.
	 * @param year the year in the Gregorian calendar.
	 * @return true if the specified year is a leap year.
	 */
	static public boolean isLeapYear(int year) {
		return ArrayTime.isLeapYear(year);
	}

	/**
	 * Return the Modified Julian day, given the Julian day.
	 * @param jd The Julian day
	 * @return The Modified Julian day
	 */
	static public double getMJD(double jd) {
		return ArrayTime.getMJD(jd);
	}

	/**
	 * Return the Julian day, given the Modified Julian day.
	 * @param mjd The modified Julian day
	 * @return the Julian day
	 */
	static public double getJD(double mjd) {
		return ArrayTime.getJD(mjd);
	}

	/**
	 * Generate a new UTCTime by adding an Interval
	 * to the specified UTCTime.
	 * @param time The base UTCTime 
	 * @param interval The interval to be added to the base time.
	 * @return A new UTCTime formed by adding an Interval
	 * to the specified Time. 
	 */
	static public UTCTime add(UTCTime time, Interval interval) {
		UTCTime t = new UTCTime(time);
		t.add(interval);
		return t;
	}

	/**
	 * Generate a new UTCTime by subtracting an Interval
	 * from the specified UTCTime.
	 * @param time The base UTCTime 
	 * @param interval The interval to be subtracted from the base time.
	 * @return A new UTCTime formed by subtracting an Interval
	 * from the specified Time. 
	 */
	static public UTCTime sub(UTCTime time, Interval interval) {
		UTCTime t = new UTCTime(time);
		t.sub(interval);
		return t;
	}
	
	private ArrayTime tai;
	
	/**
	 * Create a default UTCTime, initialized to a value of zero.
	 *
	 */
	public UTCTime() {
		tai = new ArrayTime();
	}
	
	/**
	 * Create a UTCTime from a string, which can be in one of three
	 * formats:
	 * <ul>
	 * <li>A FITS formatted string,
	 * <li>A modified Julian day, or, 
	 * <li>An integer representing the number of tenths of 
	 * microseconds since 15 October 1582 00:00:00 UTC.
	 * </ul>
	 * <p>
	 * If the format is a FITS formatted string, its format must be 
	 * of the following form:	
	 * 			"YYYY-MM-DDThh:mm:ss.ssss"
	 * Leading zeros are required if months, days, hours, minutes, or seconds 
	 * are single digits.  The value for months ranges from "01" to "12".  
	 * The "T" separting the data and time values is optional (which is a 
	 * relaxation of the strict FITS standard).  If the "T" is 
	 * not present, then a space MUST be present.
	 * <p>
	 * If the format is a modified Julian day, then the string value 
	 * must be in the form of a double which MUST include a decimal point.
	 * <p>
	 * If the format is an interger, then it MUST represent the number of 
	 * tenths of microseconds since 15 October 1582 00:00:00 UTC.
	 * 
	 * @param s The string containing the initial value.
	 */
	public UTCTime (String s) {
		tai = new ArrayTime(s);
	}
	
	/**
	 * Create a UTCTime that is initialized to a specified UTCTime.
	 * @param t
	 */
	public UTCTime(UTCTime t) {
		tai = t.tai;
	}
	
	/**
	 * Create a UTCTime from an IDL time object, which is assumed to be TAI time;
	 * leap second corrections are applied.
	 *
	 * @param t The IDL time object.
	 */
	public UTCTime (IDLArrayTime t) {
		tai = new ArrayTime(t).getUTCTime().tai;
	}
	
	/**
	 * Create a UTCTime by specifying the year, month, and day plus the fraction of a day.
	 * @param year
	 * @param month
	 * @param day
	 */
	public UTCTime(int year, int month, double day) {
		tai = new ArrayTime(year,month,day);
	}
	
	/**
	 * Create a Time by specifying the calendar date and the time.
	 * @param year
	 * @param month
	 * @param day
	 * @param hour
	 * @param minute
	 * @param second
	 */
	public UTCTime(int year, int month, int day, int hour, int minute, double second) {
		tai = new ArrayTime(year,month,day,hour,minute,second);
	}
		
	/**
	 * Create a UTCTime by specifying the modified Julian day.
	 * @param modifiedJulianDay the modified Julian day, including fractions thereof.
	 */
	public UTCTime(double modifiedJulianDay) {
		tai = new ArrayTime(modifiedJulianDay);
	}
	
	/**
	 * Create a Time by specifying the modified Julian day plus an additional factor
	 * that designates the number of seconds and fractions in a day. 
	 * @param modifiedJulianDay the Modified Julian day expressed as an interger
	 * @param secondsInADay The number of seconds (with fractions) in this day.
	 */
	public UTCTime(int modifiedJulianDay, double secondsInADay) {
		tai = new ArrayTime(modifiedJulianDay,secondsInADay);
	}
	
	/**
	 * Create a UTCTime by specifying the number of tenths of microseconds since
	 * 15 October 1582 00:00:00 UTC.
	 * @param tenthsOfMicroseconds 
	 */
	public UTCTime(long tenthsOfMicroseconds) {
		tai = new ArrayTime(tenthsOfMicroseconds);
	}
	
	/**
	 * Return the Julian day.
	 * @return The Julian day as a double.
	 */
	public double getJD() {
		return tai.getJD();
	}
	
	/**
	 * Return the Modified Julian day.
	 * @return The Modified Julian day as a double.
	 */
	public double getMJD() {
		return tai.getMJD();
	}
	
	/**
	 * Return this Time as a FITS formatted string, which is of the
	 * form "YYYY-MM-DDThh:mm:ss.ssss".
	 * @return This Time as a FITS formatted string.
	 */
	public String toFITS() {
		return tai.toFITS();
	}
	
	/**
	 * Return a IDL Time object.
	 * @return A IDL Time object.
	 */
	public IDLArrayTime toIDL() {
		return tai.toIDLArrayTime();
	}

	/**
	 * Return this time as an array of integers denoting the following:
	 * <ul>
	 * <li>year,
	 * <li>month (varies from 1 to 12),
	 * <li>day (varies from 1 to 28, 29, 30, or 31),
	 * <li>hour (varies from 0 to 23),
	 * <li>minute (varies from 0 to 59),
	 * <li>second (varies from 0 to 59), and
	 * <li>the number of tenths of a microsecond that remain in this fraction of a second.
	 * </ul>
	 * @return This time as an array of integers denoting year, month, day, hour, minute
	 * second, and fraction of a second.
	 */
	public int[] getDateTime() {
		return tai.getDateTime();
	}
	
	/**
	 * Return the time of day in hours and fractions thereof. 
	 * @return The time of day in hours.
	 */
	public double getTimeOfDay() {
		return tai.getTimeOfDay();
	}
	
	/**
	 * Return the day number of the week of this Time.
	 * The day numbers are 0-Sunday, 1-Monday, 2-Tuesday,
	 * 3-Wednesday, 4-Thursday, 5-Friday, and 6-Saturday.
	 * @return The day number of the week of this Time.
	 */
	public int getDayOfWeek() {
		return tai.getDayOfWeek();
	}
	
	/**
	 * Return the day number of the year of this Time.
	 * @return The day number of the year of this Time.
	 */
	public int getDayOfYear() {
		return tai.getDayOfYear();
	}
	
	/**
	 * Return the time of day as a string, in the form
	 * "hh:mm:ss".
	 * @return The time of day as a string.
	 */
	public String timeOfDayToString() {
		return tai.timeOfDayToString();
	}
	
	/**
	 * Return the local sidereal time for this Time
	 * in hours and fractions of an hour at the specified longitude.
	 * @param longitudeInHours The desired longitude in hours.
	 * @return The local sidereal time in hours.
	 */
	public double getLocalSiderealTime(double longitudeInHours) {
		return tai.getLocalSiderealTime(longitudeInHours);
	}
	
	/**
	 * Return the Greenwich mean sidereal time for this Time
	 * in hours and fractions of an hour.
	 * @return The Greenwich mean sidereal time in hours.
	 */
	public double getGreenwichMeanSiderealTime() {
		return tai.getGreenwichMeanSiderealTime();
	}
	
	/**
	 * Get the TAI time that corresponds to this UTC time.
	 * @return The TAI time that corresponds to this UTC time.
	 */
	public ArrayTime getArrayTime() {
		double jd = getJD();
		long value = get();
		value += (long)(ArrayTime.utcCorrection(jd) * 100000000.0);
		return new ArrayTime(value);
	}

	/**
	 * Return the value of this interval of time in units of
	 * ten nanoseconds.
	 * @return the value of this interval of time in units of
	 * ten nanoseconds.
	 */
	public long get() {
		return tai.get();
	}
	
	/**
	 * Add the specified Interval of time to this UTCTime.
	 * @param t The Interval being added to this UTCTime
	 * @return This UTCTime. 
	 */
	public UTCTime add(Interval t) {
		tai.add(t);
		return this;
	}
	
	/**
	 * Subtract the specified Interval of time from this UTCTime.
	 * @param t The Interval being substracted from this UTCTime 
	 * @return This UTCTime. 
	 */
	public UTCTime sub(Interval t) {
		tai.sub(t);
		return this;
	}
	
	/**
	 * Return true if and only if the specified object o is a
	 * UTCTime and its value is equal to this UTCTime.
	 * @param o The object to which this UTCTime is being compared.
	 * @return true if and only if the specified object o is a
	 * UTCTime and its value is equal to this UTCTime.
	 */
	public boolean equals(Object o) {
		if (!(o instanceof UTCTime))
			return false;
		return this.tai.equals((UTCTime)o);
	}
	
	/**
	 * Return a clone of this object.
	 * @return A clone of this object.
	 */
	public Object clone() {
		try { 
			return super.clone();
		} catch (CloneNotSupportedException e) { 
			// this shouldn't happen, since we are Cloneable
			throw new InternalError();
		}
	}

	/**
	 * Return true if and only if this UTCTime is zero.
	 * @return true if and only if this IUTCTime is zero.
	 */
	public boolean isZero() {
		return tai.isZero();
	}

	/**
	 * Compare this UTCTime to the specified Object, which must be
	 * a UTCTime, returning -1, 0, or +1 if this UTCTime is less 
	 * than, equal to, or greater than the specified UTCTime.
	 * @param o The Object to which this UTCTime is being compared.
	 * @return -1, 0, or +1 if this UTCTime is less than, equal to, or 
	 * greater than the specified object, which must be a UTCTime.
	 * @throws IllegalArgumentException If the object being compared is not a UTCTime.
	 */
	public int compareTo(Object o) {
		if (!(o instanceof Interval))
			throw new IllegalArgumentException("Attempt to compare a UTCTime to a non-UTCTime.");
		if (tai.get() < ((UTCTime)o).get())
			return -1;
		if (get() > ((UTCTime)o).get())
			return 1;
		return 0;
	}
	
	/**
	 * Return true if and only if this UTCTime is equal to the specified 
	 * UTCTime.
	 * @param t The UTCTime to which this UTCTime is being compared.
	 * @return True, if and only if this UTCTime is equal to the specified UTCTime.
	 */
	public boolean eq(UTCTime t) {
		return tai.get() == t.get(); 
	}
	
	/**
	 * Return true if and only if this UTCTime is not equal to the specified 
	 * UTCTime.
	 * @param t The UTCTime to which this UTCTime is being compared.
	 * @return True, if and only if this UTCTime is not equal to the specified UTCTime.
	 */
	public boolean ne(UTCTime t) {
		return tai.get() != t.get(); 
	}
	
	/**
	 * Return true if and only if this UTCTime is less than the specified 
	 * UTCTime.
	 * @param t The UTCTime to which this UTCTime is being compared.
	 * @return True, if and only if this UTCTime is less than the specified UTCTime.
	 */
	public boolean lt(UTCTime t) {
		return tai.get() < t.get(); 
	}
	
	/**
	 * Return true if and only if this UTCTime is less than or equal to the specified 
	 * UTCTime.
	 * @param t The UTCTime to which this UTCTime is being compared.
	 * @return True, if and only if this UTCTime is less than or equal to the specified UTCTime.
	 */
	public boolean le(UTCTime t) {
		return tai.get() <= t.get(); 
	}
	
	/**
	 * Return true if and only if this UTCTime is greater than the specified 
	 * UTCTime.
	 * @param t The UTCTime to which this UTCTime is being compared.
	 * @return True, if and only if this UTCTime is greater than the specified UTCTime.
	 */
	public boolean gt(UTCTime t) {
		return tai.get() > t.get(); 
	}
	
	/**
	 * Return true if and only if this UTCTime is greater than or equal to the specified 
	 * UTCTime.
	 * @param t The UTCTime to which this UTCTime is being compared.
	 * @return True, if and only if this UTCTime is greater than or equal to the specified UTCTime.
	 */
	public boolean ge(UTCTime t) {
		return tai.get() >= t.get(); 
	}
	
	
}
