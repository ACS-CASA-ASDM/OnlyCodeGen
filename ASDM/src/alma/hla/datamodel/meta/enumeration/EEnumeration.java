package alma.hla.datamodel.meta.enumeration;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.classifier.Enumeration;
import org.openarchitectureware.meta.uml.classifier.EnumerationLiteral;



import java.util.AbstractSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class EEnumeration extends Enumeration {
	public static final HashSet<String> names = new HashSet<String>();
	private boolean isInt_ = false;
	private boolean isString_ = true;
	ElementSet eenumLiteral = null;
	
	private static boolean isJavaIdentifier(String s) {
        if (s.length() == 0 || !Character.isJavaIdentifierStart(s.charAt(0))) {
            return false;
        }
        for (int i=1; i<s.length(); i++) {
            if (!Character.isJavaIdentifierPart(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }
		
	/*
	 * Initialization method.
	 */
	public void initialize() {
		if  (!EEnumeration.isJavaIdentifier(this.NameS())) Checks.error (this, "This enumeration's name '" + this.NameS() + "' does not have the syntax of an identifier");
		eenumLiteral = this.BuildLiterals();
		System.out.println(eenumLiteral.toString());
		if (eenumLiteral.size() == 0) Checks.error(this, "An EEnumeration must have at least one EENumerationLiteral");
		
		Iterator iter = eenumLiteral.iterator();
		while (iter.hasNext()) {
				((EEnumerationLiteral) iter.next()).initialize();
		}

		iter = eenumLiteral.iterator();
		isInt_ = true;
		while (iter.hasNext()) {
			isInt_ = isInt_ && ((EEnumerationLiteral) iter.next()).isInt();
			if (!isInt_) break;
		}
		
		isString_ = !isInt_;
		names.add(this.NameS());
	}

	/*
	 * An enumeration will be considered as an enumeration of integer values if and only if all its litterals
	 * can parsed into signed integer numbers.
	 */
	public boolean isInt() {
		return isInt_;
	}
	
	/*
	 * An enumeration will be considered as an enumeration of Strings values if and only if it's not considered
	 * as an enumeration of integer values.
	 */
	public boolean isString()  {
		return isString_;
	}
	
	/*
	 * Return the set of EEnumerationLiteral of this as an ElementSet.
	 * 
	 */
	public ElementSet EEnumerationLiteral() { return this.eenumLiteral; }
	
	private ElementSet BuildLiterals() {
		ElementSet eenumLiteral = new ElementSet();
		Iterator iter = this.Literal().iterator();
		
		while (iter.hasNext()) {
			Element e = (Element) iter.next();
			if (e instanceof EEnumerationLiteral)
				eenumLiteral.add(e);
		}
		
		return eenumLiteral;
	}
}
