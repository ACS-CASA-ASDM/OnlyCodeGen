/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File KeyAttribute.java
 */
package alma.hla.datamodel.meta.asdm;

import java.util.Iterator;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.ElementSet;

import alma.hla.datamodel.util.MMUtil;

/**
 * description
 * 
 * @version 1.10 Oct 1, 2004
 * @author Allen Farris
 */
public class TableKey extends ASDMAttribute {
	
	/**
	 * This is the sequence of attributes in this table that make up the key.
	 * These attributes may either be intrinsic or extrinsic attributes.  
	 * There is a 1-to-1 mapping between the field and base arrays.
	 */
	private ASDMAttribute[] field;
	
	/**
	 * This is the sequence of attributes that form the basis for the key.  These
	 * attributes may be in other tables.  These references may also be null, which 
	 * indicates there is no basis; i.e. they refer to attributes already in this table.
	 */
	private ASDMAttribute[] base;
	
	/**
	 * Create a TableKey
	 */
	public TableKey() {
		tableKey = true;
	}
	
	/**
	 * The initialize method really does the work of populating the TableKey with
	 * real values.  It uses the initial value field in UML.  The syntax of this
	 * initial value is:
	 * 					attributeName(baseTableName.baseAttributeName), ...
	 * For example:
	 * 					field1,field2(base1.attr1),field3(base2.attr1)
	 * 
	 * The baseTableName and baseAttributeName are optional.  If the baseAttributeName
	 * is not specified, this means that attributeName is the same as the name of the
	 * field attribute in the baseTable.  If baseTableName is not specified, then it
	 * is the table to which the key belongs.
	 *
	 */
	public void initialize() {
		// Parse the initial value and create the arrays of field and base.
		String[] init = this.InitValue().split(",");
		field = new ASDMAttribute [init.length];
		base = new ASDMAttribute [init.length];
		for (int i = 0; i < init.length; ++i) {
			field[i] = null;
			base[i] = null;
		}
		
		// OK, now we need to get the base table and attribute, if any.
		String name = null;
		String baseTable = null;
		String baseAttribute = null;
		int pos1 = 0;
		int pos2 = 0;
		String s = null;
		for (int i = 0; i < init.length; ++i) {
			baseTable = null;
			baseAttribute = null;
			pos1 = init[i].indexOf('(');
			if (pos1 != -1) {
				name = init[i].substring(0,pos1).trim();
				pos2 = init[i].indexOf(')');
				if (pos2 == -1) {
					Checks.error(this,"Invalid syntax in initial value [" + 
							this.InitValue() + "] of key " + NameS());
				}
				s = init[i].substring(pos1 + 1, pos2).trim();
				pos1 = s.indexOf('.');
				if (pos1 == -1) {
					baseTable = s.trim();
				} else {
					baseTable = s.substring(0,pos1).trim();
					baseAttribute = s.substring(pos1 + 1).trim();
				}
				if (baseTable.length() == 0) {
					baseTable = Class().NameS();
				}
			} else {
				name = init[i].trim();
			}
			if (name.length() == 0)
				Checks.error(this,"Invalid syntax in initial value [" + 
						this.InitValue() + "] of key " + this.NameS());
			setReference(i,name,baseTable,baseAttribute);
		}
	}
	
	private void setReference(int index, String name, String baseTable, String baseAttribute) {
		//System.out.println("name = " + name + ", baseTable = " + baseTable + "baseAttribute = " + baseAttribute);
		// 1. If baseTable and baseAttribute are both null, then it must refer to an 
		// attribute in this table.
		if (baseTable == null && baseAttribute == null) {
			Iterator iter = Class().Attribute().iterator();
			while (iter.hasNext()) {
				Object o = iter.next();
				if (o instanceof ASDMAttribute) {
					ASDMAttribute a = (ASDMAttribute)o;
					if (a.NameS().equals(name)) {
						field[index] = a;
						a.setKeyPart();
						return;
					}
				}
			}
			Checks.error(this,"The field " + name + " specified in the initial value of key " +
					NameS() + " of table " + Class().NameS() + 
					" is not an attribute in this table.");
		}
		
		// 2. Otherwise, the base table name might be this one.
		AlmaTable table = null;
		if (baseTable.equals(Class().NameS())) {
			table = (AlmaTable)Class();
		}
		// 3. If it isn't, search the meta envronment for the table name.
		Iterator iter = getMetaEnvironment().getElements("AlmaTable").iterator();
		while (iter.hasNext()) {
			AlmaTable t = (AlmaTable)iter.next();
			if (baseTable.equals(t.NameS())) {
				table = t;
				break;
			}
		}
		if (table == null)
			Checks.error(this,"The table " + baseTable + " specified in the initial value of key " +
					NameS() + " of table " + Class().NameS() + 
					" is not in the current model.");
		
		// 4. Ok, now find the attribute among that table's intrinsic attributes.
		// If we don't find it, it's an error.
		iter = table.IntrinsicAttribute().iterator();
		String s = (baseAttribute == null ? name : baseAttribute);
		while (iter.hasNext()) {
			ASDMAttribute a = (ASDMAttribute)iter.next();
			if (s.equals(a.NameS())) {
				base[index] = a;
				break;
			}
		}
		if (base[index] == null)
			Checks.error(this,"The base attribute " + s + " specified in the initial value of key " +
					NameS() + " of table " + Class().NameS() + 
					" is not an attribute in this model.");
		
		// 5. Ok, the base has been set.  We must create an extrinsic attribute,
		// add it to the table, and set the field reference to this new 
		// attribute.
		ExtrinsicAttribute ea = new ExtrinsicAttribute ();
		ea.setBase(base[index]);
		ea.setClass(table);
		ea.setType();
		ea.setName(name);
		//System.out.println("Adding an extrinsic attribute "+name+" in table "+Class());
		((AlmaTable)Class()).addExtrinsicAttribute(ea);
		field[index] = ea;
		ea.setKeyPart();
		
	}
	
	//////////////////////////////////////////////////////////////////////////////
	// The remaining methods return data about the keys in various forms.       //
	// Many of these are strings needed for the templates that generate code.   //
	//////////////////////////////////////////////////////////////////////////////
	
	/**
	 * This method return the names of the fields that make up the key as an array of strings.
	 * @return
	 */
	public String[] getNames() {
		String[] x = new String [field.length];
		for (int i = 0; i < x.length; ++i) {
			x[i] = field[i].NameS();
		}
		return x;
	}
	
	/**
	 * Return the number of fields that make up this key.
	 * @return
	 */
	public int numberFields() {
		return field.length;
	}
	
	/**
	 * Return the first name in this key.
	 * @return
	 */
	public String FirstName() {
		return field[0].NameS();
	}
	
	/**
	 * Return the last name in this key.
	 * @return
	 */
	public String LastName() {
		return field[field.length - 1].NameS();
	}
	
	/**
	 * Return the fields that make up this key.
	 * @return
	 */
	public ElementSet Field() {
		ElementSet x = new ElementSet();
		for (int i = 0; i < field.length; ++i) {
			x.add(field[i]);
		}
		return x;
	}
	public ASDMAttribute[] getField() {
		return field;
	}

	/**
	 * List of field names: "name", "sequence", "version"
	 */
	public String FieldNames1 () {
		String result = "";
		for (int i = 0; i < field.length; ++i) {
			if (i == field.length - 1)
				result += "\"" + field[i].NameS() + "\"";
			else
				result += "\"" + field[i].NameS() + "\", ";
		}
		return result;
	}
	
	/**
	 * List of field names: name + "|" + sequence + "|" + version
	 */
	public String FieldNames2 () {
		String result = "";
		for (int i = 0; i < field.length; ++i) {
			if (i == field.length - 1)
				result += field[i].NameS();
			else
				result += field[i].NameS() + " + \"|\" + ";
		}
		return result;
	}
	
	/**
	 * List of field names: x.getName(),x.getSequence(),x.getVersion()
	 */
	public String FieldNames3 () {
		String result = "";
		for (int i = 0; i < field.length; ++i) {
			if (i == field.length - 1)
				result += "x.get" + MMUtil.UpperCaseName(field[i].NameS()) + "()";
			else
				result += "x.get" + MMUtil.UpperCaseName(field[i].NameS()) + "(), ";
		}
		return result;
	}

	/**
	 * List of field names: x.getName() + "|" + x.getSequence() + "|" + x.getVersion()
	 */
	public String FieldNames4 () {
		String result = "";
		for (int i = 0; i < field.length; ++i) {
			if (i == field.length - 1)
				result += "x.get" + MMUtil.UpperCaseName(field[i].NameS()) + "()";
			else
				result += "x.get" + MMUtil.UpperCaseName(field[i].NameS()) + "() + \"|\" + ";
		}
		return result;
	}

	/**
	 * A field declaration: String name, int sequence, String version
	 */
	public String FieldDeclaration() {
		String result = "";
		for (int i = 0; i < field.length; ++i) {
			if (i == field.length - 1)
				result += field[i].JavaType() + " " + field[i].Name();
			else
				result += field[i].JavaType() + " " + field[i].Name() + ", ";
		}
		return result;
	}
	
	/**
	 * A field declaration: String[] name, int[] sequence, String[] version
	 */
	public String FieldArrayDeclaration() {
		String result = "";
		for (int i = 0; i < field.length; ++i) {
			if (i == field.length - 1)
				result += field[i].JavaType() + "[] " + field[i].Name();
			else
				result += field[i].JavaType() + "[] " + field[i].Name() + ", ";
		}
		return result;
	}
	/**
	 * A field declaration: x.getName().equals(name) && x.getSequence() == sequence && x.getVersion().equals(version)
	 */
	public String FieldComparison() {
		String result = "";
		for (int i = 0; i < field.length; ++i) {
			if (field[i].isExtendedType() ||
			    MMUtil.equalsComparison(field[i].JavaType()))
				result += "x.get" + MMUtil.UpperCaseName(field[i].NameS()) + "().equals(" + field[i].NameS() + ")";
			else
				result += "x.get" + MMUtil.UpperCaseName(field[i].NameS()) + "() == " + field[i].NameS();
			if (i < field.length -1)
				result += " && ";
		}
		return result;
	}

	/*
	 * We need to add fieldname generation methods that are based on a "count". 
	 * Here is the idea.  Suppose we want to generate the following methods: 
	 * 
	 * 		1.	XRow[] getX(String name) {... }
	 * 		2.	XRow[] getX((String name, int sequence) {...}
	 * 		3.	XRow getX(String name, int sequence, String version)
	 * 
	 * Then in the template, we would do the following:
	 * 
	 * 		foreach Field in this TableKey,
	 * 			if (field.name is equal to FirstName)
	 * 				ResetCount
	 * 			if (field.name is equal to LastName)
	 * 				generate: XRow[] getX(<<FieldDeclarationCount>>) { . . .
	 * 			else
	 * 				generate: XRow getX(<<FieldDeclarationCount>>) { . . .
	 * 			endif
	 * 			IncrementCount
	 * 		endforeach
	 * 
	 * In order to implement this technique, we need to incorporate the "count"
	 * into the generation of the strings indicated below.
	 */
	
	private int count = 0;
	public String ResetCount() {
		count = 1;
		return "";
	}
	public String IncrementCount() {
		++count;
		return "";
	}
	
	/**
	 * A field declaration based on count: String name, int sequence, String version
	 */
	public String FieldDeclarationCount() {
		if (count > field.length)
			throw new IllegalArgumentException ("Out of bounds condition on Count in TableKey.");
		String result = "";
		for (int i = 0; i < count; ++i) {
			if (i == count - 1)
				result += field[i].JavaType() + " " + field[i].Name();
			else
				result += field[i].JavaType() + " " + field[i].Name() + ", ";
		}
		return result;
	}
	/**
	 * A field comparison based on count: x.getName().equals(name) && x.getSequence() == sequence && x.getVersion().equals(version)
	 */
	public String FieldComparisonCount() {
		if (count > field.length)
			throw new IllegalArgumentException ("Out of bounds condition on Count in TableKey.");
		String result = "";
		for (int i = 0; i < count; ++i) {
			if (field[i].isExtendedType() ||
			    MMUtil.equalsComparison(field[i].JavaType()))
				result += "x.get" + MMUtil.UpperCaseName(field[i].NameS()) + "().equals(" + field[i].NameS() + ")";
			else
				result += "x.get" + MMUtil.UpperCaseName(field[i].NameS()) + "() == " + field[i].NameS();
			if (i < count -1)
				result += " && ";
		}
		return result;
	}
	/**
	 * List of field names based on count: "name", "sequence", "version"
	 */
	public String FieldNames1Count () {
		if (count > field.length)
			throw new IllegalArgumentException ("Out of bounds condition on Count in TableKey.");
		String result = "";
		for (int i = 0; i < count; ++i) {
			if (i == count - 1)
				result += "\"" + field[i].NameS() + "\"";
			else
				result += "\"" + field[i].NameS() + "\", ";
		}
		return result;
	}
	/**
	 * List of field names based on count: name + "|" + sequence + "|" + version
	 */
	public String FieldNames2Count () {
		if (count > field.length)
			throw new IllegalArgumentException ("Out of bounds condition on Count in TableKey.");
		String result = "";
		for (int i = 0; i < count; ++i) {
			if (i == count - 1)
				result += field[i].NameS();
			else
				result += field[i].NameS() + " + \"|\" + ";
		}
		return result;
	}
	
	
}
