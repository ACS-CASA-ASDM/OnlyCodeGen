/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File VerbatimCASAFiller.java
 * Date 14 juin 2009
 * Author caillat
 */
package alma.hla.datamodel.meta.asdm;

import java.util.HashMap;

import alma.hla.datamodel.enumerations.meta.AlmaEnumeration;

/**
 * @author caillat
 * 
 */
public class VerbatimCASAFiller {
	static HashMap<String, String> typeMapperInit() {
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("int", "int");
		hm.put("short", "short");
		hm.put("long", "int");
		hm.put("byte", "char");
		hm.put("char", "char");
		hm.put("boolean", "bool");
		hm.put("float", "float");
		hm.put("double", "double");

		hm.put("String", "String");
		hm.put("Complex", "String");

		hm.put("Entity", "String");
		hm.put("EntityId", "String");
		hm.put("EntityRef", "String");
		hm.put("Tag", "String");

		hm.put("Interval", "double");
		hm.put("ArrayTime", "double");
		hm.put("ArrayTimeInterval", "double");

		hm.put("Angle", "double");
		hm.put("AngularRate", "double");
		hm.put("Flux", "double");
		hm.put("Frequency", "double");
		hm.put("Humidity", "double");
		hm.put("Length", "double");
		hm.put("Pressure", "double");
		hm.put("Speed", "double");
		hm.put("Temperature", "double");
		return hm;
	}

	static HashMap<String, String> typeMapper = typeMapperInit();

	static HashMap<String, String> valueMapperInit() {
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("int", "");
		hm.put("short", "");
		hm.put("long", "");
		hm.put("byte", "");
		hm.put("char", "");
		hm.put("boolean", "");
		hm.put("float", "");
		hm.put("double", "");

		hm.put("String", "");
		hm.put("Complex", ".toString()");

		hm.put("Entity", ".toString()");
		hm.put("EntityId", ".toString()");
		hm.put("EntityRef", ".toString()");
		hm.put("Tag", ".toString()");

		hm.put("Interval", ".get()/(1.0e9)");
		hm.put("ArrayTime", ".get()/(1.0e9)");
		hm.put("ArrayTimeInterval", "NotUsedHere");

		hm.put("Angle", ".get()");
		hm.put("AngularRate", ".get()");
		hm.put("Flux", ".get()");
		hm.put("Frequency", ".get()");
		hm.put("Humidity", ".get()");
		hm.put("Length", ".get()");
		hm.put("Pressure", ".get()");
		hm.put("Speed", ".get()");
		hm.put("Temperature", ".get()");
		return hm;
	}

	static HashMap<String, String> valueMapper = valueMapperInit();

	static HashMap<String, String> arrayMapper = arrayMapperInit();

	static HashMap<String, String> arrayMapperInit() {
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("int", "basic2CASA");
		hm.put("short", "basic2CASA");
		hm.put("long", "basic2CASA");
		hm.put("byte", "basic2CASA");
		hm.put("char", "basic2CASA");
		hm.put("boolean", "basic2CASA");
		hm.put("float", "basic2CASA");
		hm.put("double", "basic2CASA");

		hm.put("String", "basic2CASA");
		hm.put("Complex", "_2CASAString");

		hm.put("Entity", "_2CASAString");
		hm.put("EntityId", "_2CASAString");
		hm.put("EntityRef", "_2CASAString");
		hm.put("Tag", "_2CASAString");

		hm.put("Interval", "interval2CASA");
		hm.put("ArrayTime", "at2CASA");
		hm.put("ArrayTimeInterval", "ati2CASA");

		hm.put("Angle", "ext2CASA");
		hm.put("AngularRate", "ext2CASA");
		hm.put("Flux", "ext2CASA");
		hm.put("Frequency", "ext2CASA");
		hm.put("Humidity", "ext2CASA");
		hm.put("Length", "ext2CASA");
		hm.put("Pressure", "ext2CASA");
		hm.put("Speed", "ext2CASA");
		hm.put("Temperature", "ext2CASA");
		return hm;
	}

	static String casaScalarType(ASDMAttribute attr) {
		if (attr.Type() instanceof AlmaEnumeration)
			return "String";
		else
			return typeMapper.get(attr.SimpleJavaType());
	}

	static String casaScalarValue(ASDMAttribute attr, String rowContext) {
		if (attr.Type() instanceof AlmaEnumeration)
			return "C" + attr.Type().NameS() + "::name(" + rowContext + "get"
					+ attr.UpperCaseName() + "())";
		else {
			if (attr.isArrayTimeIntervalType())
				return "ati2CASA1D<" + typeMapper.get(attr.SimpleJavaType())
						+ ">(" + rowContext + "get" + attr.UpperCaseName()
						+ "()" + ")";
			else
				return rowContext + "get" + attr.UpperCaseName() + "()"
						+ valueMapper.get(attr.SimpleJavaType());
		}
	}

	static String casaArrayValue(ASDMAttribute attr, String rowContext) {
		if (attr.Type() instanceof AlmaEnumeration) {
			return "enum2CASA" + attr.numberOfDimensions() + "D" + "<"
					+ attr.Type().NameS() + "," + "C" + attr.Type().NameS()
					+ ">" + "(" + rowContext + "get" + attr.UpperCaseName()
					+ "())";
		} else {
			if (attr.isArrayTimeIntervalType()) {
				return arrayMapper.get(attr.SimpleJavaType())
						+ ((Integer.parseInt(attr.numberOfDimensions())) + 1)
						+ "D" + "<" + typeMapper.get(attr.SimpleJavaType())
						+ ">(" + rowContext + "get" + attr.UpperCaseName()
						+ "()" + ")";
			} else if (attr.isArrayTimeType()) {
				return arrayMapper.get(attr.SimpleJavaType())
						+ attr.numberOfDimensions() + "D" + "<"
						+ typeMapper.get(attr.SimpleJavaType()) + ">("
						+ rowContext + "get" + attr.UpperCaseName() + "()"
						+ ")";
			} else {
				String asdmType = attr.SimpleCppType();
				if (asdmType.equals("Complex"))
					asdmType = "asdm::Complex";
				return arrayMapper.get(attr.SimpleJavaType())
						+ attr.numberOfDimensions() + "D" + "<" + asdmType
						+ "," + typeMapper.get(attr.SimpleJavaType()) + ">("
						+ rowContext + "get" + attr.UpperCaseName() + "()"
						+ ")";
			}
		}
	}

	static String casaColumn(ASDMAttribute attr) {
		if (attr.isArray() || attr.isArrayTimeIntervalType())
			return "ArrayColumn";
		else
			return "ScalarColumn";
	}

	static String casaColumnDesc(ASDMAttribute attr) {
		if (attr.isArray() || attr.isArrayTimeIntervalType())
			return "ArrayColumnDesc";
		else
			return "ScalarColumnDesc";
	}
}
