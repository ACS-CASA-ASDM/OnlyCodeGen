/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File AlmaLinkEnd.java
 */
package alma.hla.datamodel.meta.asdm;

//import de.bmiag.genfw.meta.classifier.AssociationEnd;
//import de.bmiag.genfw.meta.ElementSet;
//import de.bmiag.genfw.meta.DesignError;

import java.util.ArrayList;
import java.util.Iterator;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.classifier.AssociationEnd;

import alma.hla.datamodel.util.MMUtil;

/**
 * A TableLink is an AssociationEnd that is navigable to the table that is
 * attached to the other end. In other words, it is a navigable link to another
 * table.
 * 
 * @version 1.10 Oct 1, 2004 -- TODO Under Revision
 * @author Allen Farris
 */
public class TableLink extends AssociationEnd {

    /**
     * Is this an ordered association?
     */
    protected boolean ordered;

    /**
     * Is this a unique association?
     */
    protected boolean unique;

    /**
     * Is this link a one-to-one association?
     */
    private boolean oneToOne;

    /**
     * Is this link a one-to-many association?
     */
    private boolean oneToMany;

    private boolean oneToZeroOrMany;

    private boolean oneToOneOrMany;

    /**
     * Is this link a hasA association
     */
    private boolean hasa;

    /**
     * Is this link a slice association ?
     * 
     */
    private boolean slice;

    /**
     * Is this link a slices association ?
     */
    private boolean slices;

    /**
     * Is this link a hasmany association ?
     */
    private boolean hasmany;

    /**
     * Is this link a hasmanyAsSet association ?
     */
    private boolean hasmanyAsSet;

    /**
     * Is this link an optional association?
     */
    private boolean optional;

    /**
     * Is this link a get-only association?
     */
    private boolean isGetOnly;

    /**
     * Is this link a no-extrinsic association?
     */
    private boolean isNoExtrinsic;

    /**
     * The AlmaLink associated with this TableLink.
     */
    private AlmaLink link;

    /**
     * The AlmaTable that is attached to the opposite end of this link.
     */
    private AlmaTable target;

    /**
     * The rolename that is specified at the opposite end of this link. If there
     * is no rolename, then this is null.
     */
    private String targetRoleName;

    /**
     * The TableKey in the target table that is used to traverse the link to the
     * target table.
     */
    private TableKey targetKey;

    /**
     * The ASDMAttributes in this table, needed to traverse this table link.
     */
    private ASDMAttribute[] attribute;

    public TableLink() {
        ordered = false;
        unique = true;
        oneToOne = false;
        oneToMany = false;
        oneToZeroOrMany = false;
        oneToOneOrMany = false;
        slice = false;
        slices = false;
        hasmany = false;
        hasmanyAsSet = false;
        hasa = false;
        optional = false;
        isGetOnly = false;
        isNoExtrinsic = false;
    }
/*
    void initializeHasA() {
        hasa = true;

        // Set the association.

        link = (AlmaLink) Association();

        // Check the multiplicity of the other extremity (0..* or 1..*) ?
        // Set the amultiplicity indicators.
        int max = Integer.parseInt(Opposite().MultiplicityMax());
        int min = Integer.parseInt(Opposite().MultiplicityMin());
        if (!((min == 0 || min == 1) && (max == 1))) {
            Checks.error(this,
                    "Error ! Association end multiplicity must be 0..1 or 1..1");
        }

        // Has the target table a simple key ?

        StringBuffer keyBuffer = new StringBuffer();
        String keyNames[] = ((TableKey) target.Key().get(0)).getNames();
        if (keyNames.length > 1) {
            Checks.error(this,
                    "Error ! An hasa association can't be defined with a table having a composite key.");
        }
        // So now we know the name of the attribute that we must have in this
        // table.
        String hasAName = keyNames[0];

        // Determine if hasAName is in the Key section of THIS table.
        AlmaTable table = (AlmaTable) Class();
        keyNames = ((TableKey) table.Key().get(0)).getNames();

        boolean foundInKey = false;
        for (int i = 0; i < keyNames.length; i++) {
            if (keyNames[i].equals(hasAName)) {
                foundInKey = true;
                break;
            }
        }

        // Is it optionnal ?
        optional = false;

        if (min == 0) {
            // If optional it can't be in the Key section of THIS table.
            optional = true;
            if (foundInKey) {
                Checks.error(this,"Error ! " + hasAName
                        + " is in the Key section of " + Class()
                        + ". It can't be optional.");

            }
        }

        // If it's not in the Key section then we must add this as a new
        // extrinsic attribute.
        ExtrinsicAttribute e = null;
        if (!foundInKey) {
            ASDMAttribute a = target.getAttribute(hasAName);
            e = new ExtrinsicAttribute(hasAName, a, table);
            System.out.println("Added an extrinsic attribute " + hasAName);
            table.addExtrinsicAttribute(e);
        }

        if (optional) {
            ((ExtrinsicAttribute) table.getAttributeSimple(hasAName))
                    .setOptional();
            System.out.println("It is optional.");
        }

        // And in any case we must enter it into the attribute array of this
        // TableLink
        attribute = new ASDMAttribute[1];
        attribute[0] = table.getAttribute(hasAName);

        // Set targetKey
        targetKey = (TableKey) target.Key().get(0);

        System.out.println("Identified as a hasa on " + hasAName);
    }
*/
    
    void initializeHasA() {
        hasa = true;

        // Set the association.

        link = (AlmaLink) Association();

        // Check the multiplicity of the other extremity (0..* or 1..*) ?
        // Set the amultiplicity indicators.
        int max = Integer.parseInt(Opposite().MultiplicityMax());
        int min = Integer.parseInt(Opposite().MultiplicityMin());
        if (!((min == 0 || min == 1) && (max == 1))) {
            Checks.error(this,
                    "Error ! Association end multiplicity must be 0..1 or 1..1");
        }

        // Has the target table a simple key ?

        StringBuffer keyBuffer = new StringBuffer();
        String targetKeyNames[] = ((TableKey) target.Key().get(0)).getNames();
        if (targetKeyNames.length > 1) {
            Checks.error(this,
                    "Error ! An hasa association can't be defined with a table having a composite key.");
        }

        // Use the name of the association end on the side of the opposite table for our extrinsic attribute
        // or the name of the unique attribute in the key section of the target table.
        String hasAName = null;
        if (Opposite().Name() != null)
            hasAName = Opposite().NameS();
        else
            hasAName = targetKeyNames[0];

        // Determine if hasAName is in the Key section of THIS table.
        AlmaTable table = (AlmaTable) Class();
        String keyNames[] = ((TableKey) table.Key().get(0)).getNames();

        boolean foundInKey = false;
        for (int i = 0; i < keyNames.length; i++) {
            if (keyNames[i].equals(hasAName)) {
                foundInKey = true;
                break;
            }
        }

        // Is it optionnal ?
        optional = false;

        if (min == 0) {
            // If optional it can't be in the Key section of THIS table.
            optional = true;
            if (foundInKey) {
                Checks.error(this,"Error ! " + hasAName
                        + " is in the Key section of " + Class()
                        + ". It can't be optional.");

            }
        }

        // If it's not in the Key section then we must add this as a new
        // extrinsic attribute.
        ExtrinsicAttribute e = null;
        if (!foundInKey) {
            if (table.getAttribute(hasAName) != null) 
                Checks.error(this,"Error ! " + hasAName
                        + " has been already used as an attribute for " + Class());
            ASDMAttribute a = target.getAttribute(targetKeyNames[0]);
            e = new ExtrinsicAttribute(hasAName, a, table);
 
            table.addExtrinsicAttribute(e);
            // System.out.println("Added an extrinsic attribute " + hasAName + " to the table " + table.Name);            
        }

        if (optional) {
            ((ExtrinsicAttribute) table.getAttributeSimple(hasAName))
                    .setOptional();
            //System.out.println("It is optional.");
        }

        // And in any case we must enter it into the attribute array of this
        // TableLink
        attribute = new ASDMAttribute[1];
        attribute[0] = table.getAttribute(hasAName);

        // Set targetKey
        targetKey = (TableKey) target.Key().get(0);

        //System.out.println("Identified as a hasa on " + hasAName);
        // And yes it's a Tag attribute.
        ((ExtrinsicAttribute)table.getAttribute(hasAName)).isTag(true);
        ((ExtrinsicAttribute)table.getAttribute(hasAName)).targetTable(target.Name);
        ((ExtrinsicAttribute)table.getAttribute(hasAName)).setDocumentation(link.Documentation());
        
        if (((ExtrinsicAttribute)table.getAttribute(hasAName)).isTag()) {
        	;//System.out.println("Table " + table.Name + ": " +hasAName +" is a Tag to " + target.Name);
        }
    }
    
    // The code below should be seriously rewritten. Michel Caillat Feb, 9 2009.
    
    void initializeSlice() {

        slice = true;

        // Set the association.
        // Has it a name ?
        link = (AlmaLink) Association();
        if (Opposite().Name() == null
                || Opposite().NameS().trim().equals("")) {
            Checks.error(this,
                    "Error ! A slice link must have a named association end.");
        }
        String sliceName = Opposite().NameS();       
        String sliceNames[] = Opposite().NameS().split(",");

        // Check the multiplicity of the other extremity (0..* or 1..*) ?
        // Set the amultiplicity indicators.
        int max = Integer.parseInt(Opposite().MultiplicityMax());
        int min = Integer.parseInt(Opposite().MultiplicityMin());
        if (!((min == 0 || min == 1) && ((max > min) || (max == -1)))) {
            Checks.error(this,
                    "Error ! Association end multiplicity must be 0..* or 1..*");
        }

        // Is each part in the association end's name the name of a column in
        // the key section
        // of the target table ?
        // Set the target table.
        target = (AlmaTable) (Opposite().Class());
        Iterator iter;
        ASDMAttribute a = null;
        for (int i = 0; i < sliceNames.length; i++) {
            a = target.getAttribute(sliceNames[i]);
            if (a == null) {
                Checks.error(this,"Error ! " + sliceNames[i]
                        + " is not the name of an attribute in the table "
                        + Opposite().Class());
            }
        }

        StringBuffer keyBuffer = new StringBuffer();
        String keyNames[] = ((TableKey) target.Key().get(0)).getNames();
        keyBuffer.append(keyNames[0]);
        for (int i = 1; i < keyNames.length; i++) {
            keyBuffer.append(",").append(keyNames[i]);
        }

        boolean foundInKey = (keyBuffer.toString().indexOf(sliceName) != -1);
        /*
         * for (int i = 0; i < keyNames.length; i++) { if
         * (keyNames[i].equals(sliceName)) { foundInKey = true; break; } }
         */
        if (!foundInKey) {
            Checks.error(this,"Error ! " + sliceName
                    + " is not in the key section of " + Opposite().Class());
        }

        // Determine if sliceName is in the Key section of THIS table.
        AlmaTable table = (AlmaTable) Class();
        keyNames = ((TableKey) table.Key().get(0)).getNames();

        for (int k = 0; k < sliceNames.length; k++) {
            foundInKey = false;
            for (int i = 0; i < keyNames.length; i++) {
                if (keyNames[i].equals(sliceNames[k])) {
                    foundInKey = true;
                    break;
                }
            }

            // Is it optionnal ?
            optional = false;

            if (min == 0) {
                if (sliceNames.length > 1)
                    // Slice with more than one dimension can't be optional.
                    Checks.error(this,"Error ! " + sliceName
                            + " can't be optional");
                else {
                    // If optional it can't be in the Key section of THIS table.
                    optional = true;
                    if (foundInKey) {
                        Checks.error(this,"Error ! " + sliceName
                                + " is in the Key section of " + Class()
                                + ". It can't be optional.");
                    }
                }
            }

            // If it's not in the Key section then we must add this as a new
            // extrinsic attribute.
            String theAttributeName = null;
//            System.out.println(this.Class().NameS() + " <-----> " + Opposite().Class().NameS());
            if (this.Class().NameS().equals(Opposite().Class().NameS()))
                theAttributeName = "assoc" + MMUtil.UpperCaseName(sliceNames[k]);
            else
                theAttributeName = sliceName;
            
            ExtrinsicAttribute e = null;
            if (!foundInKey || this.Class().NameS().equals(Opposite().Class().NameS())) {
                e = new ExtrinsicAttribute(theAttributeName, a, table);
               // System.out.println("Added an extrinsic attribute "
                //        + theAttributeName);
                table.addExtrinsicAttribute(e);
                e.setDocumentation(link.Documentation());
            }

            if (optional) {
                ((ExtrinsicAttribute) table.getAttributeSimple(theAttributeName))
                        .setOptionalOneToMany();
                //System.out.println("It is optional.");
            }

            // And in any case we must enter it into the attribute array of this
            // TableLink
            attribute = new ASDMAttribute[1];
            attribute[0] = table.getAttribute(theAttributeName);
            
            // And yes it's an Id attribute.
            ((ExtrinsicAttribute)table.getAttribute(theAttributeName)).isId(true);
            ((ExtrinsicAttribute)table.getAttribute(theAttributeName)).targetTable(target.Name);

            // And attach  the Documentation to this new attribute.
            ((ExtrinsicAttribute)table.getAttribute(theAttributeName)).setDocumentation(link.Documentation());
            
//            if (((ExtrinsicAttribute)table.getAttribute(theAttributeName)).isId()) {
//            	System.out.println("Table " + table.Name + ": " +theAttributeName +" is an Id to " + target.Name);
//            }
        }

        // Set targetKey
        targetKey = (TableKey) target.Key().get(0);

        //System.out.println("Identified as a slice on " + theAttributeName);
    }

    void initializeHasMany() {
        hasmany = true;

        // Has it a name at its end?
        link = (AlmaLink) Association();

        if (Opposite().Name() == null
                || Opposite().NameS().trim().equals("")) {
            Checks.error(this,
                    "Error ! A \"hasmany\" link must have a named association end.");
        }

        // What is our current table ?
        AlmaTable table = (AlmaTable) Class();
        
        // What is the table this array is referring to ?
        AlmaTable foreignTable = this.target;
        
        // Is the key of foreignTable a simple one of type Tag ?
        String keyNames[] = ((TableKey) target.Key().get(0)).getNames();
        if (keyNames.length == 0) 
        	Checks.error(this, "Error ! Apparently no key defined in table '" + target.NameS() + "'.");
        String keyFields[] = keyNames[0].split(",");
        if ( keyFields.length == 0 )
        	Checks.error(this, "Error ! I found a key definition without any field in table '" + target.NameS() + "'.");

        if ( keyFields.length > 1 )
        	Checks.error(this, "Error ! I found a compound key definition '" + keyNames[0] + "' in table '" + target.NameS() + ". It cannot be the target of an 'hasmany' association.");
        	
        // At this point we know for sure that the target table has a simple key.
        // We still have to check that this key is "autoincrementable", we consider that 
        // in such a case it will be for sure a Tag.
        ASDMAttribute foreignTableKeyAttribute = foreignTable.getAttribute(keyFields[0].trim());
        if (!foreignTableKeyAttribute.Type().NameS().equals("Tag")) {
        	Checks.error(this, "Error ! The simple key attribute '" + 
        			foreignTableKeyAttribute.NameS() + 
        			"' is not of type Tag (found type '" + 
        			foreignTableKeyAttribute.Type().NameS() + 
        			"' in table '" + 
        			target.NameS() + 
        			"'. It cannot be the target of an 'hasmany' association.");
        }
        
        // What is the name of the association end ?
        String hasManyName = Opposite().NameS();

        // if no such name is defined then pick up the name of the key in the target table
        if (hasManyName == null ) {
        	hasManyName = foreignTableKeyAttribute.NameS();
        }
        
        // By convention if the relation is defined between a class and itself then prefix the
        // capitalized version of hasManyName x with "assoc".
        //
        String theAttributeName = null;
        if (this.Class().NameS().equals(Opposite().Class().NameS()))
            theAttributeName = "assoc" + MMUtil.UpperCaseName(hasManyName);
        else
            theAttributeName = hasManyName;

        // Is there already an attribute with this name ?
        if (table.getAttribute(theAttributeName) != null){
        	Checks.error(this, "Error ! '" + hasManyName + "' is already in declared in the table '" + table.NameS() +"'");
        }

        // Check the multiplicity of the other extremity (0..* or 1..*) ?
        // Set the multiplicity indicators.
        int max = Integer.parseInt(Opposite().MultiplicityMax());
        int min = Integer.parseInt(Opposite().MultiplicityMin());
        if (!((min == 0 || min == 1) && ((max > min) || (max == -1)))) {
            Checks.error(this,
                    "Error ! Association end multiplicity must be 0..* or 1..*");
        }

        // Is it optionnal ?
        optional = min == 0;

        // Determine if theAttributeName is in the Key section of THIS table.
        // If yes then throw a design error.
        keyNames = ((TableKey) table.Key().get(0)).getNames();

        boolean foundInKey = false;
        for (int i = 0; i < keyNames.length; i++) {
            if (keyNames[i].equals(theAttributeName)) {
                foundInKey = true;
                break;
            }
        }
        if (foundInKey)
            Checks.error(this,"Error ! \"" + theAttributeName
                    + "\" cannot be the key or a part of the key in  "
                    + this.Class().NameS());

        // We can now create a new attribute.
        ASDMAttribute a = foreignTableKeyAttribute;        
        ExtrinsicAttribute e = new ExtrinsicAttribute(theAttributeName, a,
                table);
        //System.out.println("Added an extrinsic attribute " +theAttributeName+". It will be an array");
        e.setArray();
        e.setDocumentation(link.Documentation());
        table.addExtrinsicAttribute(e);

        if (optional) {
            ((ExtrinsicAttribute) table.getAttributeSimple(theAttributeName))
                    .setOptionalOneToMany();
            // System.out.println("It is optional.");
        }

        // And in all cases declare it as an array (1D) and set its shape.
        ((ExtrinsicAttribute) table.getAttribute(theAttributeName)).setArray();
        String s = null;
        if ((s = link.getShape()) != null) {
//            System.out.println("Setting " + theAttributeName + "'s shape to "
//                    + s);
            ((ExtrinsicAttribute) table.getAttribute(theAttributeName))
                    .setShape(s);
        } else {
//            System.out
//                    .println("Setting " + theAttributeName + "'s shape to []");
            ((ExtrinsicAttribute) table.getAttribute(theAttributeName))
                    .setShape("[]");
        }

        // And in any case we must enter it into the attribute array of this
        // TableLink
        attribute = new ASDMAttribute[1];
        attribute[0] = table.getAttribute(theAttributeName);
        // System.out.println("This link has the attribute "
        // + attribute[0].NameS());

        // Set targetKey
        targetKey = (TableKey) target.Key().get(0);
        // System.out.println("Identified as a hasmany on " + hasManyName);
        
        // And yes it's a Tag attribute.
        ((ExtrinsicAttribute)table.getAttribute(theAttributeName)).isTag(true);
        ((ExtrinsicAttribute)table.getAttribute(theAttributeName)).targetTable(target.Name);
        
//        if (((ExtrinsicAttribute)table.getAttribute(theAttributeName)).isTag()) {
//        	System.out.println("Table " + table.Name + ": " +theAttributeName +" is a Tag to " + target.Name);
//        }
    }

    void initializeHasManyAsSet() {
        hasmanyAsSet = true;

        // Has it a name at its end?
        link = (AlmaLink) Association();

        if (Opposite().Name() == null
                || Opposite().NameS().trim().equals("")) {
            Checks.error(this,
                    "Error ! A \"hasmanyAsSet\" relation must have a named association end.");
        }

        // What is the name of the association end ?
        String hasManyAsSetName = Opposite().NameS();

        // Is this name the Key in the Opposite table and is the Key in the
        // opposite table composite ?
        String keyNames[] = ((TableKey) target.Key().get(0)).getNames();
        if (keyNames.length != 1 || !keyNames[0].equals(hasManyAsSetName))
            Checks.error(this,"Error ! \"" + hasManyAsSetName
                    + "\" is not the key in " + Opposite().NameS()
                    + " or the key in " + Opposite().NameS()
                    + " is composite.");

        AlmaTable table = (AlmaTable) Class();

        // Derive the name of the attribute from hasmanyName, Opposite and
        // this.Class()
        // If the relation is defined between a class and itself then prefix the
        // capitalized
        // version of hasManyName x with "assoc".
        String theAttributeName = null;
        if (this.Class().NameS().equals(Opposite().Class().NameS()))
            theAttributeName = "assoc" + MMUtil.UpperCaseName(hasManyAsSetName);
        else
            theAttributeName = hasManyAsSetName;

        // Determine if theAttributeName is in the Key section of THIS table.
        keyNames = ((TableKey) table.Key().get(0)).getNames();

        boolean foundInKey = false;
        for (int i = 0; i < keyNames.length; i++) {
            if (keyNames[i].equals(theAttributeName)) {
                foundInKey = true;
                break;
            }
        }

        // Check the multiplicity of the other extremity (0..* or 1..*) ?
        // Set the multiplicity indicators.
        int max = Integer.parseInt(Opposite().MultiplicityMax());
        int min = Integer.parseInt(Opposite().MultiplicityMin());

        if (foundInKey) {
            if (!((min == 1) && ((max > min) || (max == -1)))) {
                Checks.error(this,
                        "Error ! Association end multiplicity must be  1..*");
            }
        } else {
            if (!((min == 0 || min == 1) && ((max > min) || (max == -1)))) {
                Checks.error(this,
                        "Error ! Association end multiplicity must be 0..* or 1..*");
            }
        }
        
        // Is it optionnal ?
        optional = min == 0;
        // We can now create a new attribute if it has not yet been created 
        // because of its presence in the key section.
        //
        if (!foundInKey) {
            ASDMAttribute a = target.getAttribute(hasManyAsSetName);
            ExtrinsicAttribute e = new ExtrinsicAttribute(theAttributeName, a,
                table);
            // System.out.println("Added an extrinsic attribute " +
            // theAttributeName);
        
        // Declare that it is a set.
        // e.setSet();
            table.addExtrinsicAttribute(e);

            if (optional) {
                ((ExtrinsicAttribute) table.getAttributeSimple(theAttributeName))
                .setOptionalOneToMany();
            // System.out.println("It is optional.");
            }
        }

        // And in all cases declare it as set.
        ((ExtrinsicAttribute) table.getAttribute(theAttributeName)).setSet();
 
        // And in any case we must enter it into the attribute array of this
        // TableLink
        attribute = new ASDMAttribute[1];
        attribute[0] = table.getAttribute(theAttributeName);
        // System.out.println("This link has the attribute "
        // + attribute[0].NameS());

        // Set targetKey
        targetKey = (TableKey) target.Key().get(0);
        // System.out.println("Identified as a hasmany on " + hasManyName);
    }

    void initializeSlices() {

        slices = true;

        // Set the association.
        // Has it a name at its end?
        link = (AlmaLink) Association();
        if (Opposite().Name() == null
                || Opposite().NameS().trim().equals("")) {
            Checks.error(this,
                    "Error ! A slice link must have a named association end.");
        }

        String slicesName = Opposite().NameS();

        // Check the multiplicity of the other extremity (0..* or 1..*) ?
        // Set the multiplicity indicators.
        int max = Integer.parseInt(Opposite().MultiplicityMax());
        int min = Integer.parseInt(Opposite().MultiplicityMin());
        if (!((min == 0 || min == 1) && ((max > min) || (max == -1)))) {
            Checks.error(this,
                    "Error ! Association end multiplicity must be 0..* or 1..*");
        }

        // Is each part in the association end's name the name of a column in
        // the key section
        // of the target table ?
        // Set the target table.
        target = (AlmaTable) (Opposite().Class());
        Iterator iter;
        ASDMAttribute a = null;
        a = target.getAttribute(slicesName);
        if (a == null) {
            Checks.error(this,"Error ! " + slicesName
                    + " is not the name of an attribute in the table "
                    + Opposite().Class());
        }

        StringBuffer keyBuffer = new StringBuffer();
        String keyNames[] = ((TableKey) target.Key().get(0)).getNames();

        keyBuffer.append(keyNames[0]);
        for (int i = 1; i < keyNames.length; i++) {
            keyBuffer.append(",").append(keyNames[i]);
        }

        boolean foundInKey = (keyBuffer.toString().indexOf(slicesName) != -1);

        if (!foundInKey) {
            Checks.error(this,"Error ! " + slicesName
                    + " is not in the key section of " + Opposite().Class());
        }

        // Determine if slicesName is in the Key section of THIS table.
        AlmaTable table = (AlmaTable) Class();
        keyNames = ((TableKey) table.Key().get(0)).getNames();

        foundInKey = false;
        for (int i = 0; i < keyNames.length; i++) {
            if (keyNames[i].equals(slicesName)) {
                foundInKey = true;
                break;
            }
        }

        // Is it optionnal ?
        optional = false;

        if (min == 0) {
            // If optional it can't be in the Key section of THIS table.
            optional = true;
            if (foundInKey) {
                Checks.error(this,"Error ! " + slicesName
                        + " is in the Key section of " + Class()
                        + ". It can't be optional.");
            }
        }

        // If it's not in the Key section then we must add this as a new
        // extrinsic attribute.
        ExtrinsicAttribute e = null;
        String s = slicesName;
        if (!foundInKey) {
            if (ordered) {
                if (unique)
                    s += "";
                else
                    s += "";
            } else {
                if (unique)
                    s += "";
                else
                    Checks.error(this,"One to many link from table "
                            + ((AlmaTable) Class()).Name() + " to table "
                            + target.Name()
                            + " cannot be both unordered and not unique.");
            }
            e = new ExtrinsicAttribute(s, a, table);
            //System.out.println("Added an extrinsic attribute " + s);
            e.setArray();
            e.setDocumentation(link.Documentation());
            table.addExtrinsicAttribute(e);
        }

        if (optional) {
            ((ExtrinsicAttribute) table.getAttributeSimple(s))
                    .setOptionalOneToMany();
           // System.out.println("It is optional.");
        }

        // And in all cases declare it as an array (1D) and set its shape
        ((ExtrinsicAttribute) table.getAttribute(slicesName)).setArray();
        String s1 = null;
        if ((s1 = link.getShape()) != null) {
            //System.out.println("Setting " + slicesName + "'s shape to " + s1);
            ((ExtrinsicAttribute) table.getAttribute(slicesName)).setShape(s1);
        } else {
            //System.out.println("Setting " + slicesName + "'s shape to []");
            ((ExtrinsicAttribute) table.getAttribute(slicesName))
                    .setShape("[]");
        }

        // And in any case we must enter it into the attribute array of this
        // TableLink
        attribute = new ASDMAttribute[1];
        attribute[0] = table.getAttribute(s);
//        System.out.println("This link has the attribute "
//                + attribute[0].NameS());

        // Set targetKey
        targetKey = (TableKey) target.Key().get(0);
        
        // And yes it's an Id attribute.
        ((ExtrinsicAttribute)table.getAttribute(slicesName)).isId(true);
        ((ExtrinsicAttribute)table.getAttribute(slicesName)).targetTable(target.Name);
        
        if (((ExtrinsicAttribute)table.getAttribute(slicesName)).isId()) {
        	;//System.out.println("Table " + table.Name + ": " +slicesName +" is an Id to " + target.Name);
        }

        //System.out.println("Identified as a hasmany on " + slicesName);
    }

    void initialize() {
        //System.out.println("Association from table" + Class() + " to "
        //        + Opposite().Class());
        if (!Opposite().isNavigable())
            Checks.error(this,"Association from table " + Class() + " to "
                    + Opposite().Class() + " is not navigable.");
        if (!(Opposite().Class() instanceof AlmaTable))
            Checks.error(this,"Association from table " + Class() + " to "
                    + Opposite().Class() + ": " + Opposite().Class()
                    + " is not an AlmaTable.");
        if (!(Association() instanceof AlmaLink))
            Checks.error(this,"Association from table " + Class() + " to "
                    + Opposite().Class() + " is not an AlmaLink.");
        // Set the target table.
        target = (AlmaTable) (Opposite().Class());

        link = (AlmaLink) Association();

        // Is it an association with name "hasa"
        if (link.Name() != null && link.NameS().trim().equals("hasa")) {
            //System.out.println("Assoc name=" + link.NameS());
            initializeHasA();
            return;
        }

        // Is it an association with name "slice"
        if (link.Name() != null && link.NameS().trim().equals("slice")) {
            //System.out.println("Assoc name=" + link.NameS());
            initializeSlice();
            return;
        }

        // Is it an association with name "slices"
        if (link.Name() != null && link.NameS().trim().equals("slices")) {
            //System.out.println("Assoc name=" + link.NameS());
            initializeSlices();
            return;
        }

        // Is it an association with name "hasmany"
        if (link.Name() != null && link.NameS().trim().equals("hasmany")) {
           // System.out.println("Assoc name=" + link.NameS());
            initializeHasMany();
            return;
        }
        
        // Is it an association with name "hasmanyAsSet"
        if (link.Name() != null && link.NameS().trim().equals("hasmanyAsSet")) {
            //System.out.println("Assoc name=" + link.NameS());
            initializeHasManyAsSet();
            return;
        }        

        // Set the multiplicity indicators.
        int max = Integer.parseInt(Opposite().MultiplicityMax());
        int min = Integer.parseInt(Opposite().MultiplicityMin());
        if (max == 1 && min == 1) {
            oneToOne = true;
        } else if (max == 1 && min == 0) {
            optional = true;
        } else {
            if (max == -1 || max > 1)
                oneToMany = true;
            if (min == 1)
                oneToOneOrMany = true;
            if (min == 0)
                oneToZeroOrMany = true;
        }
        // Set the ordered and unique attributes based on the target end.
        ordered = ((TableLink) Opposite()).isOrdered();
        unique = ((TableLink) Opposite()).isUnique();
        // Set the association.
        link = (AlmaLink) Association();
        // Set the get-only property.
        isGetOnly = link.GetOnly();
        // Set the no-extrinsic property.
        isNoExtrinsic = link.NoExtrinsic();
        // Set the target role name.
        targetRoleName = null;
        // If the target rolename is the same as the target class name, then
        // it's null.
        if (Opposite().RoleName() != null) {
            if (!Opposite().RoleName()
                    .equals(Opposite().Class().NameS()))
                targetRoleName = Opposite().RoleName();
        }
        // Set the target key.
        if (target.numberKey() > 1) {
            // There is more than one key.
            // The "key" tag on the "alink" stereotype tells which one to use.
            if (link.Key == null || link.Key.length() == 0) {
                Checks.error(this,
                        "Error! Key tag is required on the link from table "
                                + Class().Name() + " to table " + target.Name()
                                + " to designate which key to use.");
            }
            Iterator iter = target.Key().iterator();
            TableKey k = null;
            while (iter.hasNext()) {
                k = (TableKey) iter.next();
                if (k.NameS().equals(link.Key))
                    targetKey = k;
            }
            if (targetKey == null) {
                Checks.error(this,"Error! Key tag on the link from table "
                        + Class().Name() + " to table " + target.Name()
                        + " does not match any key in the target table.");
            }
        } else {
            targetKey = (TableKey) target.Key().get(0);
        }
        // OK, we have gathered all the information and assigned it.
        // Now we must add any link attributes needed to traverse this link
        // and set the references to those attributes.
        if (isNoExtrinsic)
            getLinkAttributeNoExtrinsic();
        else
            getLinkAttributeExtrinsic();
        
        
    }

    private void getLinkAttributeNoExtrinsic() {
        // Get the key fields of the target.
        ASDMAttribute[] targetField = targetKey.getField();

        // Form the names by which these fields are known in this table.
        String[] targetName = new String[targetKey.numberFields()];
        for (int i = 0; i < targetName.length; ++i) {
            targetName[i] = formTargetName(targetField[i]);
        }

        // Search through the list of attributes and save the matching ones.
        // We stop at the first attribute that doesn't match.
        ArrayList a = new ArrayList();
        ASDMAttribute x = null;
        AlmaTable table = (AlmaTable) Class();
        for (int i = 0; i < targetName.length; ++i) {
            // We only match on the simple name forms.
            x = table.getAttributeSimple(targetName[i]);
            if (x == null)
                break;
            // We do not want any 1-to-many attributes.
            if (x.NameS().endsWith("SortedSet")
                    || x.NameS().endsWith("Set")
                    || x.NameS().endsWith("List"))
                break;
            // Check the types to see if they match.
            if (!x.SimpleJavaType().equals(targetField[i].SimpleJavaType())) {
                Checks.error(this,"Attributes in table " + table.Name()
                        + " used to link to table " + target.Name()
                        + "  do not match those in the target table.");
            }
            a.add(x);
        }

        // If there are no names, it's an error.
        if (a.size() == 0)
            Checks.error(this,
                    "Error! There are no matching link attributes on the link from table "
                            + Class().Name() + " to table " + target.Name());

        // OK, we're done. Save the attributes.
        attribute = new ASDMAttribute[a.size()];
        attribute = (ASDMAttribute[]) a.toArray(attribute);
    }

    private void getLinkAttributeExtrinsic() {

        // Create the array of references to the attributes.
        attribute = new ASDMAttribute[targetKey.numberFields()];

        // Get the key fields of the target.
        ASDMAttribute[] targetField = targetKey.getField();

        // Form the names by which these fields are known in this table.
        String[] targetName = new String[targetKey.numberFields()];
        for (int i = 0; i < targetName.length; ++i) {
            targetName[i] = formTargetName(targetField[i]);
        }

        AlmaTable table = (AlmaTable) Class();

        // For each target name, if it's not already in the table, add it.
        ASDMAttribute x = null;
        for (int i = 0; i < targetName.length; ++i) {
            x = table.getAttribute(targetName[i]);
            if (x != null) {
                // Check the types to see if they match.
                if (!x.SimpleJavaType().equals(targetField[i].SimpleJavaType())) {
                    Checks.error(this,"Attributes in table " + table.Name()
                            + " used to link to table " + target.Name()
                            + "  do not match those in the target table.");
                }
                attribute[i] = x;
            } else {
                // We must add this as a new extrinsic attribute.
                ExtrinsicAttribute a = new ExtrinsicAttribute(targetName[i],
                        targetField[i], table);
                if (optional)
                    a.setOptional();
                if (oneToMany)
                    a.setArray();
                a.setDocumentation(link.Documentation());
                //System.out.println("Adding attribute " + targetName[i]);
                table.addExtrinsicAttribute(a);
                attribute[i] = a;
            }
        }
    }

    private String formTargetName(ASDMAttribute base) {
        String s = null;
        String b = base.NameS();
        if (targetRoleName == null) {
            // If there is no role name, we use the base attribute name.
            s = b;
        } else {
            // If there is a role name, . . .
            if (targetRoleName.endsWith("-")) {
                // . . . we prepend it to the base name.
                s = targetRoleName.substring(0, targetRoleName.length() - 1)
                        + b.substring(0, 1).toUpperCase() + b.substring(1);
            } else {
                // Otherwise, we just use the role name.
                s = targetRoleName;
            }
        }

        // Make sure the name ends in "Id".
        if (!s.endsWith("Id"))
            s += "Id";

        // If this link is to many rows of the opposite table,
        // then we append either "Array", "Set", or "List" to the name.
        if (oneToMany) {
            if (ordered) {
                if (unique)
                    s += "SortedSet";
                else
                    s += "List";
            } else {
                if (unique)
                    s += "Set";
                else
                    Checks.error(this,"One to many link from table "
                            + ((AlmaTable) Class()).Name() + " to table "
                            + target.Name()
                            + " cannot be both unordered and not unique.");
            }
        }

        return s;
    }

    /**
     * Return a name used as a basis for methods that manage this link. For
     * example: ZZZRow getZZZZ(parm-list).
     * 
     * @return
     */
    public String MethodName() {
        // If there is an association name, we will use it.
        if (link.Name() != null) {
            return MMUtil.UpperCaseName(link.NameS());
        }
        // Otherwise, we will use the role name, if there is one.
        if (targetRoleName != null) {
            String s = targetRoleName;
            // If the name ends in "-", we drop it.
            if (s.endsWith("-"))
                s = targetRoleName.substring(0, targetRoleName.length() - 1);
            return MMUtil.UpperCaseName(s);
        }
        // Otherwise, we just use the target table name.
        return MMUtil.UpperCaseName(target.NameS());
    }

    /**
     * Return the association name as upper case.
     * 
     * @return
     */
    public String LinkNameUpperCase() {
        if (link.Key.length() == 0)
            return "";
        return MMUtil.UpperCaseName(link.Key);
    }

    // ///////////////////////////////////////

    // arg0, arg1, arg2
    public String ArgList() {
        String s = "";
        for (int i = 0; i < attribute.length; ++i) {
            s += attribute[i].Name();
            if (i < (attribute.length - 1))
                s += ", ";
        }
        return s;
    }

    // type0 arg0, type1 arg1, type2 arg2
    public String ArgDeclaration() {
        String s = "";
        for (int i = 0; i < attribute.length; ++i) {
            s += attribute[i].JavaType() + " " + attribute[i].Name();
            if (i < (attribute.length - 1))
                s += ", ";
        }
        return s;
    }

    // arg0Exists && arg1Exists && arg2Exists
    public String ArgExists() {
        String s = "";
        for (int i = 0; i < attribute.length; ++i) {
            s += attribute[i].Name() + "Exists";
            if (i < (attribute.length - 1))
                s += " && ";
        }
        return s;
    }

    // arg0, arg1, arg2
    public String ArgListSimple() {
        String s = "";
        for (int i = 0; i < attribute.length; ++i) {
            s += attribute[i].SimpleName();
            if (i < (attribute.length - 1))
                s += ", ";
        }
        return s;
    }

    // tmpArg0, tempArg1, tmpArg2
    public String ArgListSimple2() {
        String s = "";
        for (int i = 0; i < attribute.length; ++i) {
            s += "tmp" + attribute[i].UpperCaseName();
            if (i < (attribute.length - 1))
                s += ", ";
        }
        return s;
    }

    // type0 arg0, type1 arg1, type2 arg2
    public String ArgDeclarationSimple() {
        String s = "";
        for (int i = 0; i < attribute.length; ++i) {
            s += attribute[i].SimpleJavaType() + " "
                    + attribute[i].SimpleName();
            if (i < (attribute.length - 1))
                s += ", ";
        }
        return s;
    }

    /**
     * type0[] arg0, type1[] arg1, type2[] arg2
     */
    public String ArgArrayDeclarationSimple() {
        String s = "";
        for (int i = 0; i < attribute.length; ++i) {
            s += attribute[i].SimpleJavaType() + "[] "
                    + attribute[i].SimpleName();
            if (i < (attribute.length - 1))
                s += ", ";
        }
        return s;
    }

    public String CppArgArrayDeclarationSimple() {
        String s = "";
        for (int i = 0; i < attribute.length; ++i) {
            s += "vector<" + attribute[i].SimpleJavaType() + "> "
                    + attribute[i].SimpleName();
            if (i < (attribute.length - 1))
                s += ", ";
        }
        return s;
    }

    // arg0Exists && arg1Exists && arg2Exists
    public String ArgExistsSimple() {
        String s = "";
        for (int i = 0; i < attribute.length; ++i) {
            s += attribute[i].SimpleName() + "Exists";
            if (i < (attribute.length - 1))
                s += " && ";
        }
        return s;
    }

    // x.getName(),x.getSequence(),x.getVersion()
    public String FieldNames() {
        String s = "";
        for (int i = 0; i < attribute.length; ++i) {
            if (i == attribute.length - 1)
                s += "x.get" + MMUtil.UpperCaseName(attribute[i].SimpleName())
                        + "()";
            else
                s += "x.get" + MMUtil.UpperCaseName(attribute[i].SimpleName())
                        + "(), ";
        }
        return s;
    }

    // List of field names: name + "|" + sequence + "|" + version
    public String FieldNames2() {
        String s = "";
        for (int i = 0; i < attribute.length; ++i) {
            if (i == attribute.length - 1)
                s += attribute[i].SimpleName();
            else
                s += attribute[i].SimpleName() + " + \"|\" + ";
        }
        return s;
    }

    // x.getName().equals(name) && x.getSequence() == sequence &&
    // x.getVersion().equals(version)
    public String FieldComparison() {
        String s = "";
        for (int i = 0; i < attribute.length; ++i) {
            if (attribute[i].isExtendedType()
                    || MMUtil.equalsComparison(attribute[i].SimpleJavaType()))
                s += "x.get" + MMUtil.UpperCaseName(attribute[i].SimpleName())
                        + "().equals(" + attribute[i].SimpleName() + ")";
            else
                s += "x.get" + MMUtil.UpperCaseName(attribute[i].SimpleName())
                        + "() == " + attribute[i].SimpleName();
            if (i < attribute.length - 1)
                s += " && ";
        }
        return s;
    }

    // Return the first name in this attribute list.
    public String FirstNameSimple() {
        return attribute[0].SimpleName();
    }

    // ///////////////////////////////////////////////////

    public boolean GetOnly() {
        return isGetOnly;
    }

    public boolean NoExtrinsic() {
        return isNoExtrinsic;
    }

    /**
     * @return Returns the link.
     */
    public AlmaLink getLink() {
        return link;
    }

    public boolean isOneToMany() {
        return oneToMany;
    }

    public boolean isOneToZeroOrMany() {
        return oneToZeroOrMany;
    }

    public boolean isOneToOneOrMany() {
        return oneToOneOrMany;
    }

    /**
     * @return Returns the oneToOne.
     */
    public boolean isOneToOne() {
        return oneToOne;
    }

    /**
     * @return Returns the optional.
     */
    public boolean isOptional() {
        return optional;
    }

    /**
     * @return Returns if this link a slice.
     */
    public boolean isSlice() {
        return slice;
    }

    /**
     * @return Returns if this link a slices.
     */
    public boolean isSlices() {
        return slices;
    }

    /**
     * @return Returns if this link a hasa.
     */
    public boolean isHasa() {
        return hasa;
    }

    /**
     * @return Returns if this link a hasmany.
     */
    public boolean isHasmany() {
        return hasmany;
    }
    
    /**
     * @return Returns if this link a hasmanyAsSet.
     */
    public boolean isHasmanyAsSet() {
        return hasmanyAsSet;
    }

    /**
     * @return Returns the ordered.
     */
    public boolean isOrdered() {
        return ordered;
    }

    /**
     * @return Returns the target.
     */
    public AlmaTable getTarget() {
        return target;
    }

    public AlmaTable Target() {
        return target;
    }

    /**
     * @return Returns the targetKey.
     */
    public TableKey getTargetKey() {
        return targetKey;
    }

    public TableKey TargetKey() {
        return targetKey;
    }

    /**
     * @return Returns the targetRoleName.
     */
    public String getTargetRoleName() {
        return targetRoleName;
    }

    /**
     * @return Returns the unique.
     */
    public boolean isUnique() {
        return unique;
    }

    public ASDMAttribute TheAttribute() {
        return attribute[0];
    }

    public ElementSet Attribute() {
        ElementSet x = new ElementSet();
        for (int i = 0; i < attribute.length; ++i)
            x.add(attribute[i]);
        return x;
    }

    public boolean NumberAttributesLessThanTarget() {
        return attribute.length < targetKey.getField().length;
    }

}
