/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File AlmaModel.java
 */
package alma.hla.datamodel.meta.asdm;

//import de.bmiag.genfw.meta.ElementSet;
//import de.bmiag.genfw.meta.Element;
//import de.bmiag.genfw.meta.core.Model;
//import de.bmiag.genfw.meta.DesignError;

import java.util.Iterator;

import org.openarchitectureware.core.constraint.Checks;
import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.core.meta.core.Model;
import org.openarchitectureware.core.meta.util.MMUtil;

import alma.hla.datamodel.enumerations.meta.AlmaEnumeration;


/**
 * The AlmaModel class houses the model as a whole.  One of its major
 * methods is "checkConstraints".  This method must be called as the
 * first step in the code generation templates.  It not only does
 * constraint checking but actually initializes the AlmaTableContainer
 * and all the AlmaTables.
 * 
 * @version 1.10 Oct 1, 2004
 * @author Allen Farris
 */
public class AlmaModel extends Model {

	public AlmaModel() {
	}
	
	/**
	 * Check the constraints on the model and initialize everything.
	 */
	public void checkConstraints() {	
		/*
		// This test merely prints all owned elements of the model
		// and any children of AlmaAttributes.
		System.out.println("Alma Model test");
		Iterator iterx = OwnedElement().iterator();
		while (iterx.hasNext()) {
			Object o = iterx.next();
			if (o instanceof AlmaAttribute) {
				AlmaAttribute a = (AlmaAttribute)o;
				Map c = this.getChildren();
				System.out.println("Attribute children: " + c);				
			}
			System.out.println(o.toString());
		}
		System.out.println("End of Alma Model test");
		*/
		
		// There will probably be other constraints to check.
		// ... Add these later.
		
		
		return;
	}
	
    protected void initializeModelDependencies() {
		
		// Make sure there is one and only one table container.
		Element container = AlmaTableContainer();
		
		// It's only once the AlmaTableContainer is built that we can set its version number
		// to the value found in its version attribute.
		((AlmaTableContainer) container).setVersion();
		
		// Set the XML ns prefix
		((AlmaTableContainer) container).setXMLnsPrefix();		
		
		// Get all the ALMA tables.
		ElementSet table = SortedAlmaTable();
		
		// Set the tables in the container.
		((AlmaTableContainer)container).setTable(table);
		
		// Now initialize all the tables, which is a multi-step process.
		Iterator iter = table.iterator();
		AlmaTable t = null;
		while (iter.hasNext()) {
			t = (AlmaTable)iter.next();
			t.setContainer((AlmaTableContainer)container);
			t.InitializeOne();
		}
		System.out.println("AlmaModel: Phase 1 initialization complete.");
		iter = table.iterator();
		while (iter.hasNext()) {
			t = (AlmaTable)iter.next();
			t.InitializeTwo();
		}
		System.out.println("AlmaModel: Phase 2 initialization complete.");
		iter = table.iterator();
		while (iter.hasNext()) {
			t = (AlmaTable)iter.next();
			t.InitializeThree();
		}
		System.out.println("AlmaModel: Phase 3 initialization complete.");
		iter = table.iterator();
		while (iter.hasNext()) {
			t = (AlmaTable)iter.next();
			t.InitializeFour();
		}
		
		// There can't be more than ONE autoincrementable attribute.
		if (t.AutoIncrementableAttribute().size()>1) {
			Checks.error(this, "There can't be more than one autoincrementable attribute in a table.");
		}
		
		// The autoincrementable attribute if any, HAS TO BE a part of the Key.
		else if (t.AutoIncrementableAttribute().size()==1){
			if ((t.Key().size()==0) ||
					!t.Key().contains(t.AutoIncrementableAttribute().get(0))) {
				Checks.error(this,"The autoincrementable attribute must be a part of the key.");
			}
		}	
		System.out.println("AlmaModel: Phase 4 initialization complete.");
		
		iter = table.iterator();
		while (iter.hasNext()) {
			t = (AlmaTable)iter.next();
			t.InitializeFive();
		}
		System.out.println("AlmaModel: Phase 5 initialization complete.");
        
        iter = table.iterator();
        while (iter.hasNext()) {
            t = (AlmaTable)iter.next();
            t.InitializeSix(AlmaTable());
        }
        System.out.println("AlmaModel: Phase 6 initialization complete.");
//        
//        ElementSet enumerations = EEnumerations();
//        Iterator enumIter = enumerations.iterator();
//        while (enumIter.hasNext()) {
//        	((EEnumeration)enumIter.next()).initialize();
//        }
        System.out.println("AlmaModel : EEnumerations initialized.");
		
		System.out.println("AlmaModel: constraint checking complete.");
	}
	
	/**
	 * Return all AlmaTables as an ElementSet.
	 * @return
	 */
	public ElementSet AlmaTable() {
		// Go through the OwnedElements and return all AlmaTables.
		ElementSet x = new ElementSet ();
		Iterator iter = OwnedElement().iterator();
		while (iter.hasNext()) {
			Object o = iter.next();
			if (o instanceof AlmaTable) {
				x.add(o);
			}
		}
		return x;
	}

	
	/**
	 * Return all AlmaTables as a sorted ElementSet.
	 * @return
	 */
	public ElementSet SortedAlmaTable() {
		ElementSet x = AlmaTable();
		if (x.size() == 0 || x.size() == 1)
			return x;
		AlmaTable[] y = new AlmaTable [x.size()];
		Iterator iter = x.iterator();
		for (int i = 0; i < y.length; ++i) {
			y[i] = (AlmaTable)iter.next();
		}
		AlmaTable tmp = null;
		for (int i = 0; i < y.length; ++i) {
			if (y[i].NameS().equals("Main")) {
				tmp = y[0];
				y[0] = y[i];
				y[i] = tmp;
			}
		}
		int pos = 0;
		if (y.length > 2) {
			for (int i = 1; i < y.length; ++i) {
				pos = i;
				for (int j = i + 1; j < y.length; ++j) {
					if (y[j].NameS().compareTo(y[pos].NameS()) < 1) {
						pos = j;
					}
				}
				tmp = y[i];
				y[i] = y[pos];
				y[pos] = tmp;
			}
		}
		
		ElementSet z = new ElementSet ();
		for (int i = 0; i < y.length; ++i) {
			z.add(y[i]);
		}
		return z;
	}

	
	/**
	 * Return the AlmaTableContainer as an Element.
	 * @return
	 */
	public Element AlmaTableContainer() {
		// Go through the OwnedElements and return all AlmaTables.
		AlmaTableContainer x = null;
		Iterator iter = OwnedElement().iterator();
		int count = 0;
		while (iter.hasNext()) {
			Object o = iter.next();
			if (o instanceof AlmaTableContainer) {
				x = (AlmaTableContainer)o;
				++count;
			}
		}
		if (count == 0) {
			Checks.error(this,"There is no table container in the model.");
		}
		if (count > 1) {
			Checks.error(this,"There are multiple table containers in the model.");
		}
		return x;
	}
	
	public ElementSet Enumerations() {
		return MMUtil.findAllInstances(AlmaEnumeration.class);
	}
	
//	/**
//	 * Return the defined enumerations as a set of Element.
//	 * 
//	 */
//	public ElementSet EEnumerations() {
//		ElementSet enumSet = new ElementSet();
//		
//		Iterator iter = OwnedElement().iterator();
//		while (iter.hasNext()) {
//			Object o = iter.next();
//			if (o instanceof EEnumeration) {
//				enumSet.add(o);
//			}
//		}
//		return enumSet;
//	}
}
