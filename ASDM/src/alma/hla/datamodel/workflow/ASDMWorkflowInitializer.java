package alma.hla.datamodel.workflow;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.openarchitectureware.workflow.WorkflowContext;
import org.openarchitectureware.workflow.issues.Issues;
import org.openarchitectureware.workflow.lib.AbstractWorkflowComponent;
import org.openarchitectureware.workflow.monitor.ProgressMonitor;

public class ASDMWorkflowInitializer extends AbstractWorkflowComponent {
	private String AsdmFile;
	private String cvsRevision;
	private String cvsBranch;
	
	public void checkConfiguration(Issues issues) {
		// TODO Auto-generated method stub

	}

	public void invoke(WorkflowContext ctx, ProgressMonitor monitor,
			Issues issues) {
		System.setProperty("alma.asdm.cvs.revision", cvsRevision);
		System.setProperty("alma.asdm.cvs.branch", cvsBranch);
	}

	public void setAsdmFile(String cf) {
		this.AsdmFile = cf;
		File f = new File(cf);
	    File entries = new File(f.getParent()+File.separator+"CVS"+File.separator+"Entries");
	    String modelName = f.getName();
	    BufferedReader rdr;
		try {
			rdr = new BufferedReader(new FileReader(entries));
		} catch (FileNotFoundException e) {
			cvsRevision = "-1";
			cvsBranch = "";
			System.out.println("CVS Entries file not found");
			return;
		}
	    String line;
	    try {
			while ((line = rdr.readLine()) != null) {
				if (line.contains(modelName)) {
					String[] fields = line.split("/");
					cvsRevision = fields[2];
					System.out.println("ASDM CVS revision is: "+cvsRevision);
					/*
					 * Do we have a possible branch  field ?
					 */
					if (fields.length > 5) {
						cvsBranch = fields[5];
						System.out.println(cvsBranch);
						/*
						 * Is it really a branch ?
						 */
						if (cvsBranch.charAt(0) == 'T') {
							cvsBranch = cvsBranch.substring(1);
							System.out.println("ASDM CVS branch is:"+cvsBranch);
						}
						else {
							cvsBranch = "";
							System.out.println("Tag information does represent a branch");
						}						
					}
					/*
					 * We don't have a branch field.
					 */
					else {
						cvsBranch = "HEAD";
					}
					return;
				}
			}
			cvsRevision = "-1";
			System.out.println("Couldn't find filename "+modelName+" in CVS/Entries");
			return;
		} catch (IOException e) {
			cvsRevision = "-1";
			cvsBranch = "";
			System.out.println("Error in readLine from CVS/Entries");
			return;
		}		
	}
}
