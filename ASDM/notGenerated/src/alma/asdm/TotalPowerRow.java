/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File TotalPowerRow.java
 */
package alma.asdm;

import alma.asdmIDL.*;
import alma.hla.runtime.asdm.types.*;
import alma.asdmIDLTypes.*;
import alma.hla.runtime.asdm.ex.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * The TotalPowerRow class is a row of a TotalPowerTable.
 */
public class TotalPowerRow extends ASDMRow {
	/** 
	 * This piece of code is not clean at all...should be improved in the future...
	 */
	private Tag wrap(Tag t) {
		return t;
	}

	private Tag unwrap(Tag t) {
		return t;
	}

	private Integer wrap(int i) {
		return new Integer(i);
	}

	private Float wrap(float f) {
		return new Float(f);
	}

	private Double wrap(double d) {
		return new Double(d);
	}

	private Boolean wrap(boolean b) {
		return new Boolean(b);
	}

	private int unwrap(Integer I) {
		return I.intValue();
	}

	/**
	 * End of poor piece of code...
	 */

	/**
	 * Create a TotalPowerRow.
	 * <p>
	 * This constructor has package access because only the
	 * table can create rows.  All rows know the table
	 * to which they belong.
	 * @param table The table to which this row belongs.
	 */
	TotalPowerRow(TotalPowerTable table) {
		this.table = table;
		this.hasBeenAdded = false;
	}

	/**
	 * Creates a TotalPowerRow using a copy constructor mechanism.
	 * <p>
	 * Given a TotalPowerRow row and a TotalPowerTable table, the method creates a new
	 * TotalPowerRow owned by table. Each attribute of the created row is a copy (deep)
	 * of the corresponding attribute of row. The method does not add the created
	 * row to its table, its simply parents it to table, a call to the add method
	 * has to be done in order to get the row added (very likely after having modified
	 * some of its attributes).
	 * If row is null then the method returns a row with default values for its attributes. 
	 *
	 * This constructor has package access because only the
	 * table can create rows.  All rows know the table
	 * to which they belong.
	 * @param table The table to which this row belongs.
	 * @param row  The row which is to be copied.
	 */
	TotalPowerRow(TotalPowerTable table,
			TotalPowerRow row) {
		this.table = table;
		this.hasBeenAdded = false;

		if (row == null)
			return;

		// configDescriptionId is a Tag, a new Tag is required.
		this.configDescriptionId = new Tag(row.configDescriptionId);

		// fieldId is a Tag, a new Tag is required.
		this.fieldId = new Tag(row.fieldId);

		// time is a ArrayTime, a new ArrayTime is required.
		this.time = new ArrayTime(row.time);

		// execBlockId is a Tag, a new Tag is required.
		this.execBlockId = new Tag(row.execBlockId);

		// stateId is a an ArrayList , let's populate this.stateId element by element.
		for (int i = 0; i < row.stateId.size(); i++)

			this.stateId.add(new Tag((Tag) row.stateId.get(i)));

		// scanNumber is a int, a simple assignment (=) is ok.
		this.scanNumber = row.scanNumber;

		// subscanNumber is a int, a simple assignment (=) is ok.
		this.subscanNumber = row.subscanNumber;

		// integrationNumber is a int, a simple assignment (=) is ok.
		this.integrationNumber = row.integrationNumber;

		// uvw is an array , let's use the copyUvw method.
		this.uvw = copyUvw(row.uvw);

		// exposure is an array , let's use the copyExposure method.
		this.exposure = copyExposure(row.exposure);

		// timeCentroid is an array , let's use the copyTimeCentroid method.
		this.timeCentroid = copyTimeCentroid(row.timeCentroid);

		// floatData is an array , let's use the copyFloatData method.
		this.floatData = copyFloatData(row.floatData);

		// flagAnt is an array , let's use the copyFlagAnt method.
		this.flagAnt = copyFlagAnt(row.flagAnt);

		// flagPol is an array , let's use the copyFlagPol method.
		this.flagPol = copyFlagPol(row.flagPol);

		// flagRow is a boolean, a simple assignment (=) is ok.
		this.flagRow = row.flagRow;

		// interval is a Interval, a new Interval is required.
		this.interval = new Interval(row.interval);

		if (row.subintegrationNumberExists) {

			// subintegrationNumber is a int, a simple assignment (=) is ok.
			this.subintegrationNumber = row.subintegrationNumber;

			this.subintegrationNumberExists = true;
		}

	}

	/**
	 * The table to which this row belongs.
	 */
	private TotalPowerTable table;

	/**
	 * Whether this row has been added to the table or not.
	 */
	private boolean hasBeenAdded;

	// This method has package access and is used by the Table class
	// when it is added to the table.
	void isAdded() {
		hasBeenAdded = true;
	}

	/**
	 * Return the table to which this row belongs.
	 */
	public TotalPowerTable getTable() {
		return table;
	}

	/**
	 * Return this row in the form of an IDL struct.
	 * @return The values of this row as a TotalPowerRowIDL struct.
	 */
	public TotalPowerRowIDL toIDL() {
		TotalPowerRowIDL x = new TotalPowerRowIDL();

		// Fill the IDL structure.

		x.time = getTime().toIDLArrayTime();

		x.scanNumber = getScanNumber();

		x.subscanNumber = getSubscanNumber();

		x.integrationNumber = getIntegrationNumber();

		Length[][] tmpUvw = getUvw();
		x.uvw = new IDLLength[tmpUvw.length][tmpUvw[0].length];
		for (int i = 0; i < x.uvw.length; ++i)
			for (int j = 0; j < x.uvw[i].length; ++j)
				x.uvw[i][j] = tmpUvw[i][j].toIDLLength();

		Interval[][] tmpExposure = getExposure();
		x.exposure = new IDLInterval[tmpExposure.length][tmpExposure[0].length];
		for (int i = 0; i < x.exposure.length; ++i)
			for (int j = 0; j < x.exposure[i].length; ++j)
				x.exposure[i][j] = tmpExposure[i][j].toIDLInterval();

		ArrayTime[][] tmpTimeCentroid = getTimeCentroid();
		x.timeCentroid = new IDLArrayTime[tmpTimeCentroid.length][tmpTimeCentroid[0].length];
		for (int i = 0; i < x.timeCentroid.length; ++i)
			for (int j = 0; j < x.timeCentroid[i].length; ++j)
				x.timeCentroid[i][j] = tmpTimeCentroid[i][j].toIDLArrayTime();

		x.floatData = getFloatData();

		x.flagAnt = getFlagAnt();

		x.flagPol = getFlagPol();

		x.flagRow = getFlagRow();

		x.interval = getInterval().toIDLInterval();

		x.subintegrationNumberExists = subintegrationNumberExists;
		if (subintegrationNumberExists) {
			try {

				x.subintegrationNumber = getSubintegrationNumber();

			} catch (IllegalAccessException e) {
			}
		} else {

			// subintegrationNumber is not an array.

		}

		x.configDescriptionId = getConfigDescriptionId().toIDLTag();

		x.execBlockId = getExecBlockId().toIDLTag();

		x.fieldId = getFieldId().toIDLTag();

		Tag[] tmpStateId = getStateId();
		x.stateId = new IDLTag[tmpStateId.length];
		for (int i = 0; i < x.stateId.length; ++i)
			x.stateId[i] = tmpStateId[i].toIDLTag();

		return x;
	}

	/**
	 * Fill the values of this row from the IDL struct TotalPowerRowIDL.
	 * @param x The IDL struct containing the values used to fill this row.
	 */
	public void setFromIDL(TotalPowerRowIDL x)
			throws ConversionException {
		try {
			// Fill the values from x.

			setTime(new ArrayTime(x.time));

			setScanNumber(x.scanNumber);

			setSubscanNumber(x.subscanNumber);

			setIntegrationNumber(x.integrationNumber);

			Length[][] tmpUvw = new Length[x.uvw.length][x.uvw[0].length];
			for (int i = 0; i < x.uvw.length; ++i)
				for (int j = 0; j < x.uvw[i].length; ++j)
					tmpUvw[i][j] = new Length(x.uvw[i][j]);
			setUvw(tmpUvw);

			Interval[][] tmpExposure = new Interval[x.exposure.length][x.exposure[0].length];
			for (int i = 0; i < x.exposure.length; ++i)
				for (int j = 0; j < x.exposure[i].length; ++j)
					tmpExposure[i][j] = new Interval(x.exposure[i][j]);
			setExposure(tmpExposure);

			ArrayTime[][] tmpTimeCentroid = new ArrayTime[x.timeCentroid.length][x.timeCentroid[0].length];
			for (int i = 0; i < x.timeCentroid.length; ++i)
				for (int j = 0; j < x.timeCentroid[i].length; ++j)
					tmpTimeCentroid[i][j] = new ArrayTime(x.timeCentroid[i][j]);
			setTimeCentroid(tmpTimeCentroid);

			setFloatData(x.floatData);

			setFlagAnt(x.flagAnt);

			setFlagPol(x.flagPol);

			setFlagRow(x.flagRow);

			setInterval(new Interval(x.interval));

			if (x.subintegrationNumberExists) {

				setSubintegrationNumber(x.subintegrationNumber);

			}

			setConfigDescriptionId(new Tag(x.configDescriptionId));

			setExecBlockId(new Tag(x.execBlockId));

			setFieldId(new Tag(x.fieldId));

			Tag[] tmpStateId = new Tag[x.stateId.length];
			for (int i = 0; i < x.stateId.length; ++i)
				tmpStateId[i] = new Tag(x.stateId[i]);
			setStateId(tmpStateId);

		} catch (IllegalAccessException err) {
			throw new ConversionException(err.toString(),
					"TotalPower");
		}
	}

	/**
	 * Return this row in the form of an XML string.
	 * @return The values of this row as an XML string.
	 * @throws ConversionException.
	 */
	public String toXML() throws ConversionException {
		StringBuffer buf = new StringBuffer();
		buf.append("<row> ");

		Parser.toXML(time, "time", buf);

		Parser.toXML(scanNumber, "scanNumber", buf);

		Parser.toXML(subscanNumber, "subscanNumber", buf);

		Parser.toXML(integrationNumber, "integrationNumber", buf);

		Parser.toXML(uvw, "uvw", buf);

		Parser.toXML(exposure, "exposure", buf);

		Parser.toXML(timeCentroid, "timeCentroid", buf);

		Parser.toXML(floatData, "floatData", buf);

		Parser.toXML(flagAnt, "flagAnt", buf);

		Parser.toXML(flagPol, "flagPol", buf);

		Parser.toXML(flagRow, "flagRow", buf);

		Parser.toXML(interval, "interval", buf);

		if (subintegrationNumberExists) {

			Parser.toXML(subintegrationNumber, "subintegrationNumber", buf);

		}

		Parser.toXML(configDescriptionId, "configDescriptionId", buf);

		Parser.toXML(execBlockId, "execBlockId", buf);

		Parser.toXML(fieldId, "fieldId", buf);

		Parser.toXML(getStateId(), "stateId", buf);

		buf.append("</row>");
		return buf.toString();
	}

	/**
	 * Fill the values of this row from an XML string 
	 * that was produced by the toXML() method.
	 * @param x The XML string being used to set the values of this row.
	 * @throws ConversionException.
	 */
	public void setFromXML(String rowDoc) throws ConversionException {
		Parser row = new Parser(rowDoc);
		String s = null;
		try {

			setTime(Parser.getArrayTime("time", "TotalPower", rowDoc));

			setScanNumber(Parser.getInteger("scanNumber",
					"TotalPower", rowDoc));

			setSubscanNumber(Parser.getInteger("subscanNumber",
					"TotalPower", rowDoc));

			setIntegrationNumber(Parser.getInteger("integrationNumber",
					"TotalPower", rowDoc));

			setUvw(Parser.get2DLength("uvw", "TotalPower", rowDoc));

			setExposure(Parser.get2DInterval("exposure",
					"TotalPower", rowDoc));

			setTimeCentroid(Parser.get2DArrayTime("timeCentroid",
					"TotalPower", rowDoc));

			setFloatData(Parser.get3DFloat("floatData", "TotalPower",
					rowDoc));

			setFlagAnt(Parser.get1DInteger("flagAnt", "TotalPower",
					rowDoc));

			setFlagPol(Parser.get2DInteger("flagPol", "TotalPower",
					rowDoc));

			setFlagRow(Parser.getBoolean("flagRow", "TotalPower",
					rowDoc));

			setInterval(Parser.getInterval("interval", "TotalPower",
					rowDoc));

			if (row.isStr("<subintegrationNumber>")) {

				setSubintegrationNumber(Parser.getInteger(
						"subintegrationNumber", "TotalPower", rowDoc));

			}

			setConfigDescriptionId(Parser.getTag("configDescriptionId",
					"Processor", rowDoc));

			setExecBlockId(Parser.getTag("execBlockId", "TotalPower",
					rowDoc));

			setFieldId(Parser.getTag("fieldId", "Field", rowDoc));

			setStateId(Parser.get1DTag("stateId", "TotalPower",
					rowDoc));

		} catch (IllegalAccessException err) {
			throw new ConversionException(err.toString(),
					"TotalPower");
		}
	}

	/**
	 * Writes the serialized binary representation of this to DataOutputStream.
	 * @param dos The DataOutputStream to be written to.
	 * 
	 */
	public void toBin(DataOutputStream dos) throws IOException {
		time.toBin(dos);
		dos.writeInt(scanNumber);
		dos.writeInt(subscanNumber);
		dos.writeInt(integrationNumber);
		Length.toBin(uvw, dos);
		Interval.toBin(exposure, dos);
		ArrayTime.toBin(timeCentroid, dos);
		
		dos.writeInt(floatData.length);
		dos.writeInt(floatData[0].length);
		dos.writeInt(floatData[0][0].length);
		for (int i = 0; i < floatData.length; i ++)
			for (int j = 0; j < floatData[0].length; j++)
				for (int k = 0; k < floatData[0][0].length; k++)
					dos.writeFloat(floatData[i][j][k]);
		
		dos.writeInt(flagAnt.length);
		for (int i = 0; i < flagAnt.length; i++)
			dos.writeInt(flagAnt[i]);
			
		dos.writeInt(flagPol.length);
		dos.writeInt(flagPol[0].length);		
		for (int i = 0; i < flagPol.length; i++)
			for (int j = 0; j < flagPol[0].length; j++)
				dos.writeInt(flagPol[i][j]);
			
		dos.writeBoolean(flagRow);
		interval.toBin(dos);
		
		dos.writeBoolean(subintegrationNumberExists);
		if (subintegrationNumberExists)
			dos.writeInt(subintegrationNumber);
		
		configDescriptionId.toBin(dos);
		execBlockId.toBin(dos);
		fieldId.toBin(dos);
		Tag.toBin(this.getStateId(), dos);
	}
	

	public static TotalPowerRow  fromBin(DataInputStream dis, TotalPowerTable table) throws IOException, TagFormatException {
		TotalPowerRow row = new TotalPowerRow(table);
		
		row.time = ArrayTime.fromBin(dis); 
		row.scanNumber = dis.readInt(); 
		row.subscanNumber = dis.readInt(); 
		row.integrationNumber = dis.readInt(); 
		row.uvw = Length.from2DBin(dis);
		row.exposure = Interval.from2DBin(dis);
		row.timeCentroid = ArrayTime.from2DBin(dis);
		
		int dim1 = 0;
		int dim2 = 0;
		int dim3 = 0;
		
		dim1 = dis.readInt();
		dim2 = dis.readInt();
		dim3 = dis.readInt();
		
		row.floatData = new float[dim1][dim2][dim3];
		for (int i = 0; i < dim1; i ++)
			for (int j = 0; j < dim2; j++)
				for (int k = 0; k < dim3; k++)
					row.floatData[i][j][k] = dis.readFloat();
		
		dim1 = dis.readInt();
		row.flagAnt = new int[dim1];
		for (int i = 0; i < dim1; i++)
			row.flagAnt[i] = dis.readInt();
		
		dim1 = dis.readInt();
		dim2 = dis.readInt();
		row.flagPol = new int[dim1][dim2];
		for (int i = 0; i < dim1; i++)
			for (int j = 0;  j < dim2; j++)
				row.flagPol[i][j] = dis.readInt();
		
		row.flagRow = dis.readBoolean(); 
		row.interval = Interval.fromBin(dis); 
		
		row.subintegrationNumberExists = dis.readBoolean();
		if (row.subintegrationNumberExists) row.subintegrationNumber = dis.readInt();
		
		row.configDescriptionId = Tag.fromBin(dis);
		row.execBlockId = Tag.fromBin(dis);
		row.fieldId = Tag.fromBin(dis);
		row.setStateId(Tag.from1DBin(dis));
		return row;
	}

	////////////////////////////////
	// Intrinsic Table Attributes //
	////////////////////////////////

	// ===> Attribute time

	private ArrayTime time;

	/**
	 * Get time.
	 * @return time as ArrayTime
	 */
	public ArrayTime getTime() {

		return new ArrayTime(time);

	}

	/**
	 * Set time with the specified ArrayTime value.
	 * @param time The ArrayTime value to which time is to be set.
	 
	 * @throw IllegalAccessException If an attempt is made to change this field after is has been added to the table.
	 */

	public void setTime(ArrayTime time) throws IllegalAccessException {
		if (hasBeenAdded) {
			throw new IllegalAccessException(
					"Attempt to change the time field, that is part of a key, after this row has been added to this table.");
		}

		this.time = new ArrayTime(time);

	}

	// ===> Attribute scanNumber

	private int scanNumber;

	/**
	 * Get scanNumber.
	 * @return scanNumber as int
	 */
	public int getScanNumber() {

		return scanNumber;

	}

	/**
	 * Set scanNumber with the specified int value.
	 * @param scanNumber The int value to which scanNumber is to be set.
	 
	 */
	public void setScanNumber(int scanNumber) {

		this.scanNumber = scanNumber;

	}

	// ===> Attribute subscanNumber

	private int subscanNumber;

	/**
	 * Get subscanNumber.
	 * @return subscanNumber as int
	 */
	public int getSubscanNumber() {

		return subscanNumber;

	}

	/**
	 * Set subscanNumber with the specified int value.
	 * @param subscanNumber The int value to which subscanNumber is to be set.
	 
	 */
	public void setSubscanNumber(int subscanNumber) {

		this.subscanNumber = subscanNumber;

	}

	// ===> Attribute integrationNumber

	private int integrationNumber;

	/**
	 * Get integrationNumber.
	 * @return integrationNumber as int
	 */
	public int getIntegrationNumber() {

		return integrationNumber;

	}

	/**
	 * Set integrationNumber with the specified int value.
	 * @param integrationNumber The int value to which integrationNumber is to be set.
	 
	 */
	public void setIntegrationNumber(int integrationNumber) {

		this.integrationNumber = integrationNumber;

	}

	// ===> Attribute uvw

	private Length[][] uvw;

	private Length[][] copyUvw(Length[][] a) {

		int dim1 = a.length;
		if (dim1 == 0)
			return new Length[0][0];

		int dim2 = a[0].length;
		if (dim2 == 0)
			return new Length[0][0];

		Length[][] result = new Length[dim1][dim2];
		for (int i = 0; i < dim1; i++)
			for (int j = 0; j < dim2; j++)

				result[i][j] = new Length(a[i][j]);

		return result;
	}

	/**
	 * Get uvw.
	 * @return uvw as Length[][]
	 */
	public Length[][] getUvw() {

		return copyUvw(uvw);

	}

	/**
	 * Set uvw with the specified Length[][] value.
	 * @param uvw The Length[][] value to which uvw is to be set.
	 
	 */
	public void setUvw(Length[][] uvw) {

		this.uvw = copyUvw(uvw);

	}

	// ===> Attribute exposure

	private Interval[][] exposure;

	private Interval[][] copyExposure(Interval[][] a) {

		int dim1 = a.length;
		if (dim1 == 0)
			return new Interval[0][0];

		int dim2 = a[0].length;
		if (dim2 == 0)
			return new Interval[0][0];

		Interval[][] result = new Interval[dim1][dim2];
		for (int i = 0; i < dim1; i++)
			for (int j = 0; j < dim2; j++)

				result[i][j] = new Interval(a[i][j]);

		return result;
	}

	/**
	 * Get exposure.
	 * @return exposure as Interval[][]
	 */
	public Interval[][] getExposure() {

		return copyExposure(exposure);

	}

	/**
	 * Set exposure with the specified Interval[][] value.
	 * @param exposure The Interval[][] value to which exposure is to be set.
	 
	 */
	public void setExposure(Interval[][] exposure) {

		this.exposure = copyExposure(exposure);

	}

	// ===> Attribute timeCentroid

	private ArrayTime[][] timeCentroid;

	private ArrayTime[][] copyTimeCentroid(ArrayTime[][] a) {

		int dim1 = a.length;
		if (dim1 == 0)
			return new ArrayTime[0][0];

		int dim2 = a[0].length;
		if (dim2 == 0)
			return new ArrayTime[0][0];

		ArrayTime[][] result = new ArrayTime[dim1][dim2];
		for (int i = 0; i < dim1; i++)
			for (int j = 0; j < dim2; j++)

				result[i][j] = new ArrayTime(a[i][j]);

		return result;
	}

	/**
	 * Get timeCentroid.
	 * @return timeCentroid as ArrayTime[][]
	 */
	public ArrayTime[][] getTimeCentroid() {

		return copyTimeCentroid(timeCentroid);

	}

	/**
	 * Set timeCentroid with the specified ArrayTime[][] value.
	 * @param timeCentroid The ArrayTime[][] value to which timeCentroid is to be set.
	 
	 */
	public void setTimeCentroid(ArrayTime[][] timeCentroid) {

		this.timeCentroid = copyTimeCentroid(timeCentroid);

	}

	// ===> Attribute floatData

	private float[][][] floatData;

	private float[][][] copyFloatData(float[][][] a) {

		int dim1 = a.length;
		if (dim1 == 0)
			return new float[0][0][0];

		int dim2 = a[0].length;
		if (dim2 == 0)
			return new float[0][0][0];

		int dim3 = a[0][0].length;
		if (dim3 == 0)
			return new float[0][0][0];

		float[][][] result = new float[dim1][dim2][dim3];
		for (int i = 0; i < dim1; i++)
			for (int j = 0; j < dim2; j++)
				for (int k = 0; k < dim3; k++)

					result[i][j][k] = a[i][j][k];

		return result;
	}

	/**
	 * Get floatData.
	 * @return floatData as float[][][]
	 */
	public float[][][] getFloatData() {

		return copyFloatData(floatData);

	}

	/**
	 * Set floatData with the specified float[][][] value.
	 * @param floatData The float[][][] value to which floatData is to be set.
	 
	 */
	public void setFloatData(float[][][] floatData) {

		this.floatData = copyFloatData(floatData);

	}

	// ===> Attribute flagAnt

	private int[] flagAnt;

	private int[] copyFlagAnt(int[] a) {

		int dim1 = a.length;
		if (dim1 == 0)
			return new int[0];

		int[] result = new int[dim1];
		for (int i = 0; i < result.length; i++)

			result[i] = a[i];

		return result;
	}

	/**
	 * Get flagAnt.
	 * @return flagAnt as int[]
	 */
	public int[] getFlagAnt() {

		return copyFlagAnt(flagAnt);

	}

	/**
	 * Set flagAnt with the specified int[] value.
	 * @param flagAnt The int[] value to which flagAnt is to be set.
	 
	 */
	public void setFlagAnt(int[] flagAnt) {

		this.flagAnt = copyFlagAnt(flagAnt);

	}

	// ===> Attribute flagPol

	private int[][] flagPol;

	private int[][] copyFlagPol(int[][] a) {

		int dim1 = a.length;
		if (dim1 == 0)
			return new int[0][0];

		int dim2 = a[0].length;
		if (dim2 == 0)
			return new int[0][0];

		int[][] result = new int[dim1][dim2];
		for (int i = 0; i < dim1; i++)
			for (int j = 0; j < dim2; j++)

				result[i][j] = a[i][j];

		return result;
	}

	/**
	 * Get flagPol.
	 * @return flagPol as int[][]
	 */
	public int[][] getFlagPol() {

		return copyFlagPol(flagPol);

	}

	/**
	 * Set flagPol with the specified int[][] value.
	 * @param flagPol The int[][] value to which flagPol is to be set.
	 
	 */
	public void setFlagPol(int[][] flagPol) {

		this.flagPol = copyFlagPol(flagPol);

	}

	// ===> Attribute flagRow

	private boolean flagRow;

	/**
	 * Get flagRow.
	 * @return flagRow as boolean
	 */
	public boolean getFlagRow() {

		return flagRow;

	}

	/**
	 * Set flagRow with the specified boolean value.
	 * @param flagRow The boolean value to which flagRow is to be set.
	 
	 */
	public void setFlagRow(boolean flagRow) {

		this.flagRow = flagRow;

	}

	// ===> Attribute interval

	private Interval interval;

	/**
	 * Get interval.
	 * @return interval as Interval
	 */
	public Interval getInterval() {

		return new Interval(interval);

	}

	/**
	 * Set interval with the specified Interval value.
	 * @param interval The Interval value to which interval is to be set.
	 
	 */
	public void setInterval(Interval interval) {

		this.interval = new Interval(interval);

	}

	// ===> Attribute subintegrationNumber, which is optional

	private boolean subintegrationNumberExists = false;

	private int subintegrationNumber;

	/**
	 * The attribute subintegrationNumber is optional. Return true if this attribute exists.
	 * @return true if and only if the subintegrationNumber attribute exists. 
	 */
	public boolean isSubintegrationNumberExists() {
		return subintegrationNumberExists;
	}

	/**
	 * Get subintegrationNumber, which is optional.
	 * @return subintegrationNumber as int
	 * @throws IllegalAccessException If subintegrationNumber does not exist.
	 */
	public int getSubintegrationNumber() throws IllegalAccessException {
		if (!subintegrationNumberExists) {
			throw new IllegalAccessException(
					"Attempt to access a non-existent attribute.  The "
							+ subintegrationNumber
							+ " attribute in table TotalPower does not exist!");
		}

		return subintegrationNumber;

	}

	/**
	 * Set subintegrationNumber with the specified int value.
	 * @param subintegrationNumber The int value to which subintegrationNumber is to be set.
	 
	 */
	public void setSubintegrationNumber(int subintegrationNumber) {

		this.subintegrationNumber = subintegrationNumber;

		subintegrationNumberExists = true;

	}

	/**
	 * Mark subintegrationNumber, which is an optional field, as non-existent.
	 */
	public void clearSubintegrationNumber() {
		subintegrationNumberExists = false;
	}

	////////////////////////////////
	// Extrinsic Table Attributes //
	////////////////////////////////

	// ===> Attribute configDescriptionId

	private Tag configDescriptionId;

	/**
	 * Get configDescriptionId.
	 * @return configDescriptionId as Tag
	 */
	public Tag getConfigDescriptionId() {

		return new Tag(configDescriptionId);

	}

	/**
	 * Set configDescriptionId with the specified Tag value.
	 * @param configDescriptionId The Tag value to which configDescriptionId is to be set.
	 
	 * @throw IllegalAccessException If an attempt is made to change this field after is has been added to the table.
	 */

	public void setConfigDescriptionId(Tag configDescriptionId)
			throws IllegalAccessException {
		if (hasBeenAdded) {
			throw new IllegalAccessException(
					"Attempt to change the configDescriptionId field, that is part of a key, after this row has been added to this table.");
		}

		this.configDescriptionId = new Tag(configDescriptionId);

	}

	// ===> Attribute execBlockId

	private Tag execBlockId;

	/**
	 * Get execBlockId.
	 * @return execBlockId as Tag
	 */
	public Tag getExecBlockId() {

		return new Tag(execBlockId);

	}

	/**
	 * Set execBlockId with the specified Tag value.
	 * @param execBlockId The Tag value to which execBlockId is to be set.
	 
	 */
	public void setExecBlockId(Tag execBlockId) {

		this.execBlockId = new Tag(execBlockId);

	}

	// ===> Attribute fieldId

	private Tag fieldId;

	/**
	 * Get fieldId.
	 * @return fieldId as Tag
	 */
	public Tag getFieldId() {

		return new Tag(fieldId);

	}

	/**
	 * Set fieldId with the specified Tag value.
	 * @param fieldId The Tag value to which fieldId is to be set.
	 
	 * @throw IllegalAccessException If an attempt is made to change this field after is has been added to the table.
	 */

	public void setFieldId(Tag fieldId) throws IllegalAccessException {
		if (hasBeenAdded) {
			throw new IllegalAccessException(
					"Attempt to change the fieldId field, that is part of a key, after this row has been added to this table.");
		}

		this.fieldId = new Tag(fieldId);

	}

	// ===> Attribute stateId

	private ArrayList stateId;

	/**
	 * Get stateId.
	 * @return stateId as Tag[]
	 */
	public Tag[] getStateId() {

		Tag[] result = new Tag[this.stateId.size()];
		for (int i = 0; i < result.length; i++)
			result[i] = unwrap((Tag) stateId.get(i));
		return result;

	}

	/**
	 * Set stateId with the specified Tag[] value.
	 * @param stateId The Tag[] value to which stateId is to be set.
	 
	 */
	public void setStateId(Tag[] stateId) {

		if (this.stateId == null)
			this.stateId = new ArrayList();
		else
			this.stateId.clear();
		for (int i = 0; i < stateId.length; i++)
			this.stateId.add(wrap(stateId[i]));

	}

	///////////
	// Links //
	///////////

	/**
	 * Set stateId[i] with the specified Tag value.
	 * @param i The index in stateId where to set the Tag value.
	 * @param stateId The Tag value to which stateId[i] is to be set. 
	 * @throws IndexOutOfBoundsException
	 */
	public void setStateId(int i, Tag stateId) throws IndexOutOfBoundsException {
		this.stateId.set(i, wrap(stateId));
	}

	// ===> hasmany link from a row of TotalPower table to many rows of State table.

	/**
	 * Append a Tag to stateId
	 * @param id the Tag to be appended to stateId
	 *
	 */
	void addStateId(Tag id) {
		if (stateId == null)
			stateId = new ArrayList();
		this.stateId.add(wrap(id));
	}

	/**
	 * Append an array of Tag to stateId
	 * @param id an array of Tag to be appended to stateId
	 */
	void addStateId(Tag[] id) {
		if (stateId == null)
			stateId = new ArrayList();
		for (int i = 0; i < id.length; i++)
			this.stateId.add(wrap(id[i]));
	}

	/**
	 * Returns the Tag stored in stateId at position i.
	 *
	 */
	Tag getStateId(int i) throws IndexOutOfBoundsException {
		return (Tag) stateId.get(i);
	}

	/**
	 * Returns the StateRow linked to this row via the Tag stored in stateId
	 * at position i.
	 */
	StateRow getState(int i) throws IndexOutOfBoundsException, NoSuchRow {
		return table.getContainer().getState()
				.getRowByKey((Tag) stateId.get(i));
	}

	/**
	 * Returns the array of StateRow linked to this row via the Tags stored in stateId
	 *
	 */
	StateRow[] getStates() throws NoSuchRow {
		StateRow[] x = new StateRow[stateId.size()];
		for (int i = 0; i < x.length; i++)
			x[i] = table.getContainer().getState().getRowByKey(
					(Tag) stateId.get(i));
		return x;
	}

	/**
	 * Returns the pointer to the row in the Field table having Field.fieldId == fieldId
	 * @return a FieldRow
	 * 
	 
	 */
	public FieldRow getFieldUsingFieldId() {

		return table.getContainer().getField().getRowByKey(fieldId);
	}

	/**
	 * Returns the pointer to the row in the ConfigDescription table having ConfigDescription.configDescriptionId == configDescriptionId
	 * @return a ConfigDescriptionRow
	 * 
	 
	 */
	public ConfigDescriptionRow getConfigDescriptionUsingConfigDescriptionId() {

		return table.getContainer().getConfigDescription().getRowByKey(
				configDescriptionId);
	}

	// ===> One to one link from a row of TotalPower table to a row of ExecBlock table.

	/**
	 * Get the row in table ExecBlock by traversing the defined link to that table.
	 * @return A row in ExecBlock table.
	 * @throws NoSuchRow if there is no such row in table ExecBlock.
	 */
	public ExecBlockRow getExecBlock() throws NoSuchRow {
		return table.getContainer().getExecBlock().getRowByKey(execBlockId);
	}

	/**
	 * Compare each attribute except the autoincrementable one of this TotalPowerRow with 
	 * the corresponding parameters and return true if there is a match and false otherwise.
	 */
	public boolean compareNoAutoInc(Tag configDescriptionId, Tag fieldId,
			ArrayTime time, Tag execBlockId, Tag[] stateId, int scanNumber,
			int subscanNumber, int integrationNumber, Length[][] uvw,
			Interval[][] exposure, ArrayTime[][] timeCentroid,
			float[][][] floatData, int[] flagAnt, int[][] flagPol,
			boolean flagRow, Interval interval) {

		// configDescriptionId is a Tag, compare using the equals method.
		if (!(this.configDescriptionId.equals(configDescriptionId)))
			return false;

		// fieldId is a Tag, compare using the equals method.
		if (!(this.fieldId.equals(fieldId)))
			return false;

		// time is a ArrayTime, compare using the equals method.
		if (!(this.time.equals(time)))
			return false;

		// execBlockId is a Tag, compare using the equals method.
		if (!(this.execBlockId.equals(execBlockId)))
			return false;

		// stateId is an extrinsic attribute which is an array of Tag.
		// Let's compare an array with an ArrayList ...
		if (this.stateId.size() != stateId.length)
			return false;

		// ... using the equals method.		
		for (int i = 0; i < stateId.length; i++) {
			if (!((Tag) this.stateId.get(i)).equals(stateId[i]))
				return false;
		}

		// scanNumber is a int, compare using the == operator.	
		if (!(this.scanNumber == scanNumber))
			return false;

		// subscanNumber is a int, compare using the == operator.	
		if (!(this.subscanNumber == subscanNumber))
			return false;

		// integrationNumber is a int, compare using the == operator.	
		if (!(this.integrationNumber == integrationNumber))
			return false;

		// We compare two 2D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1uvw = this.uvw.length;
		if (dim1uvw != uvw.length)
			return false;
		if (dim1uvw > 0) {
			int dim2uvw = this.uvw[0].length;
			if (dim2uvw != uvw[0].length)
				return false;
			for (int i = 0; i < dim1uvw; i++)
				for (int j = 0; j < dim2uvw; j++) {

					// uvw is an array of Length, compare using equals method.
					if (!(this.uvw[i][j].equals(uvw[i][j])))
						return false;

				}
		}

		// We compare two 2D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1exposure = this.exposure.length;
		if (dim1exposure != exposure.length)
			return false;
		if (dim1exposure > 0) {
			int dim2exposure = this.exposure[0].length;
			if (dim2exposure != exposure[0].length)
				return false;
			for (int i = 0; i < dim1exposure; i++)
				for (int j = 0; j < dim2exposure; j++) {

					// exposure is an array of Interval, compare using equals method.
					if (!(this.exposure[i][j].equals(exposure[i][j])))
						return false;

				}
		}

		// We compare two 2D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1timeCentroid = this.timeCentroid.length;
		if (dim1timeCentroid != timeCentroid.length)
			return false;
		if (dim1timeCentroid > 0) {
			int dim2timeCentroid = this.timeCentroid[0].length;
			if (dim2timeCentroid != timeCentroid[0].length)
				return false;
			for (int i = 0; i < dim1timeCentroid; i++)
				for (int j = 0; j < dim2timeCentroid; j++) {

					// timeCentroid is an array of ArrayTime, compare using equals method.
					if (!(this.timeCentroid[i][j].equals(timeCentroid[i][j])))
						return false;

				}
		}

		// We compare two 3D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1floatData = this.floatData.length;
		if (dim1floatData != floatData.length)
			return false;
		if (dim1floatData > 0) {
			int dim2floatData = this.floatData[0].length;
			if (dim2floatData != floatData[0].length)
				return false;
			if (dim2floatData > 0) {
				int dim3floatData = this.floatData[0][0].length;
				if (dim3floatData != floatData[0][0].length)
					return false;
				for (int i = 0; i < dim1floatData; i++)
					for (int j = 0; j < dim2floatData; j++)
						for (int k = 0; k < dim3floatData; k++) {

							// floatData is an array of float, compare using == operator.
							if (!(this.floatData[i][j][k] == floatData[i][j][k]))
								return false;

						}
			}
		}

		// We compare two 1D arrays.
		// Compare firstly their dimensions and then their values.
		if (this.flagAnt.length != flagAnt.length)
			return false;
		for (int i = 0; i < flagAnt.length; i++) {

			// flagAnt is an array of int, compare using == operator.
			if (!(this.flagAnt[i] == flagAnt[i]))
				return false;

		}

		// We compare two 2D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1flagPol = this.flagPol.length;
		if (dim1flagPol != flagPol.length)
			return false;
		if (dim1flagPol > 0) {
			int dim2flagPol = this.flagPol[0].length;
			if (dim2flagPol != flagPol[0].length)
				return false;
			for (int i = 0; i < dim1flagPol; i++)
				for (int j = 0; j < dim2flagPol; j++) {

					// flagPol is an array of int, compare using == operator.
					if (!(this.flagPol[i][j] == flagPol[i][j]))
						return false;

				}
		}

		// flagRow is a boolean, compare using the == operator.	
		if (!(this.flagRow == flagRow))
			return false;

		// interval is a Interval, compare using the equals method.
		if (!(this.interval.equals(interval)))
			return false;

		return true;
	}

	/**
	 * Return true if all required attributes of the value part are equal to their homologues
	 * in x and false otherwise.
	 *
	 * @param x the TotalPowerRow whose required attributes of the value part 
	 * will be compared with those of this.
	 * @return a boolean.
	 */
	public boolean equalByRequiredValue(TotalPowerRow x) {

		return compareRequiredValue(

		x.getExecBlockId(), x.getStateId(), x.getScanNumber(), x
				.getSubscanNumber(), x.getIntegrationNumber(), x.getUvw(), x
				.getExposure(), x.getTimeCentroid(), x.getFloatData(), x
				.getFlagAnt(), x.getFlagPol(), x.getFlagRow(), x.getInterval()

		);

	}

	public boolean compareRequiredValue(Tag execBlockId, Tag[] stateId,
			int scanNumber, int subscanNumber, int integrationNumber,
			Length[][] uvw, Interval[][] exposure, ArrayTime[][] timeCentroid,
			float[][][] floatData, int[] flagAnt, int[][] flagPol,
			boolean flagRow, Interval interval) {

		// execBlockId is a Tag, compare using the equals method.
		if (!(this.execBlockId.equals(execBlockId)))
			return false;

		// stateId is an extrinsic attribute which is an array of Tag.
		// Let's compare an array with an ArrayList ...
		if (this.stateId.size() != stateId.length)
			return false;

		// ... using the equals method.		
		for (int i = 0; i < stateId.length; i++) {
			if (!((Tag) this.stateId.get(i)).equals(stateId[i]))
				return false;
		}

		// scanNumber is a int, compare using the == operator.	
		if (!(this.scanNumber == scanNumber))
			return false;

		// subscanNumber is a int, compare using the == operator.	
		if (!(this.subscanNumber == subscanNumber))
			return false;

		// integrationNumber is a int, compare using the == operator.	
		if (!(this.integrationNumber == integrationNumber))
			return false;

		// We compare two 2D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1uvw = this.uvw.length;
		if (dim1uvw != uvw.length)
			return false;
		if (dim1uvw > 0) {
			int dim2uvw = this.uvw[0].length;
			if (dim2uvw != uvw[0].length)
				return false;
			for (int i = 0; i < dim1uvw; i++)
				for (int j = 0; j < dim2uvw; j++) {

					// uvw is an array of Length, compare using equals method.
					if (!(this.uvw[i][j].equals(uvw[i][j])))
						return false;

				}
		}

		// We compare two 2D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1exposure = this.exposure.length;
		if (dim1exposure != exposure.length)
			return false;
		if (dim1exposure > 0) {
			int dim2exposure = this.exposure[0].length;
			if (dim2exposure != exposure[0].length)
				return false;
			for (int i = 0; i < dim1exposure; i++)
				for (int j = 0; j < dim2exposure; j++) {

					// exposure is an array of Interval, compare using equals method.
					if (!(this.exposure[i][j].equals(exposure[i][j])))
						return false;

				}
		}

		// We compare two 2D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1timeCentroid = this.timeCentroid.length;
		if (dim1timeCentroid != timeCentroid.length)
			return false;
		if (dim1timeCentroid > 0) {
			int dim2timeCentroid = this.timeCentroid[0].length;
			if (dim2timeCentroid != timeCentroid[0].length)
				return false;
			for (int i = 0; i < dim1timeCentroid; i++)
				for (int j = 0; j < dim2timeCentroid; j++) {

					// timeCentroid is an array of ArrayTime, compare using equals method.
					if (!(this.timeCentroid[i][j].equals(timeCentroid[i][j])))
						return false;

				}
		}

		// We compare two 3D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1floatData = this.floatData.length;
		if (dim1floatData != floatData.length)
			return false;
		if (dim1floatData > 0) {
			int dim2floatData = this.floatData[0].length;
			if (dim2floatData != floatData[0].length)
				return false;
			if (dim2floatData > 0) {
				int dim3floatData = this.floatData[0][0].length;
				if (dim3floatData != floatData[0][0].length)
					return false;
				for (int i = 0; i < dim1floatData; i++)
					for (int j = 0; j < dim2floatData; j++)
						for (int k = 0; k < dim3floatData; k++) {

							// floatData is an array of float, compare using == operator.
							if (!(this.floatData[i][j][k] == floatData[i][j][k]))
								return false;

						}
			}
		}

		// We compare two 1D arrays.
		// Compare firstly their dimensions and then their values.
		if (this.flagAnt.length != flagAnt.length)
			return false;
		for (int i = 0; i < flagAnt.length; i++) {

			// flagAnt is an array of int, compare using == operator.
			if (!(this.flagAnt[i] == flagAnt[i]))
				return false;

		}

		// We compare two 2D arrays.
		// Compare firstly their dimensions and then their values.
		int dim1flagPol = this.flagPol.length;
		if (dim1flagPol != flagPol.length)
			return false;
		if (dim1flagPol > 0) {
			int dim2flagPol = this.flagPol[0].length;
			if (dim2flagPol != flagPol[0].length)
				return false;
			for (int i = 0; i < dim1flagPol; i++)
				for (int j = 0; j < dim2flagPol; j++) {

					// flagPol is an array of int, compare using == operator.
					if (!(this.flagPol[i][j] == flagPol[i][j]))
						return false;

				}
		}

		// flagRow is a boolean, compare using the == operator.	
		if (!(this.flagRow == flagRow))
			return false;

		// interval is a Interval, compare using the equals method.
		if (!(this.interval.equals(interval)))
			return false;

		return true;
	}

	////////////////////////////////////////// 
	//Attributes values as array of Objects.//
	//////////////////////////////////////////

	// Row display related stuff
	Object[] getAttributesValues() {
		Object[] r = new Object[getTable().getAttributesNames().length];
		int i = 0;

		r[i++] = getConfigDescriptionId();

		r[i++] = getFieldId();

		r[i++] = getTime();

		r[i++] = getExecBlockId();

		//r[i++] = "["+getStateId().length+"]";
		r[i++] = getStateId();

		r[i++] = wrap(getScanNumber());

		r[i++] = wrap(getSubscanNumber());

		r[i++] = wrap(getIntegrationNumber());

		;
		r[i++] = getUvw();

		;
		r[i++] = getExposure();

		;
		r[i++] = getTimeCentroid();

		;
		r[i++] = getFloatData();

		;
		r[i++] = getFlagAnt();

		;
		r[i++] = getFlagPol();

		r[i++] = wrap(getFlagRow());

		r[i++] = getInterval();

		if (isSubintegrationNumberExists()) {
			try {

				r[i++] = wrap(getSubintegrationNumber());

			} catch (IllegalAccessException e) {
				;
			}
		} else {
			r[i++] = null;
		}

		return r;

	}

}
