�IMPORT org::openarchitectureware::core::meta::core�
�IMPORT org::openarchitectureware::meta::uml�
�IMPORT org::openarchitectureware::meta::uml::classifier�
�IMPORT alma::hla::datamodel::meta::asdm�

�DEFINE Root FOR AlmaTableContainer�
�FILE "src/"+DirPath+"/"+NameS+".java"�
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File �NameS�.java
 */
package �Path�;

import alma.hla.runtime.asdm.ex.*;

import �this.Path�IDL.*;
import �this.ExtendedTypes�.*;
import �this.ExtendedIDLTypes�.*;

import alma.xmlstore.ArchiveConnectionPackage.ArchiveException;
import alma.xmlstore.IdentifierPackage.NotAvailable;

import java.io.*;
import java.util.Calendar;
import java.util.Hashtable;

import org.slf4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The �NameS� class is the container for all tables.  Its instantation
 * create a complete set of tables.
 *
 * Generated from model's revision �this.CVSRevision�, branch �this.CVSBranch�
 *
 */
public class �NameS� implements Representable {

	private boolean archiveAsBin = false;  // If true archive binary else archive XML
	private boolean fileAsBin        = false;   // If true file binary else file XML
	private boolean hasBeenAdded = false;  // Just to be "compatible" with tables generated code.

				
�FOREACH (Set[AlmaTable])AlmaTable AS table �
	/**
	 * The table �table.NameS�
	 */
	private �table.NameS�Table �table.LowerCase�;
�ENDFOREACH�

	/**
	 * Provide a way to inject an IASDMCorder here.
	 */ 
	protected IASDMCorder asdmCorder = null;  // The IASDMCorder attached to this. 
	                                        // By default nada.
	/**
	 * Inject the IASDMCorder which will be used 
	 * for the recording.
	 *
	 * @param asdmCorder. An implementation of IASDMCorder.
	 */
	public void recordWith (IASDMCorder asdmCorder) {
		this.asdmCorder = asdmCorder;
	}
		
	/**
	 * Returns its tables as an array of Object.
	 * @return an array of Object.
	 *
	 */
	public Object[] getTables() {
		return new Object[] {
�FOREACH (Set[AlmaTable])AlmaTable AS table SEPARATOR ","�
	 �table.LowerCase�
�ENDFOREACH�
		};	
	}
 
	/**
	 * The list of tables as Representable.
	 */
	private Representable[] table;
	
	/**
	 * The list of Entity objects representing the tables.
	 */
	// private Entity[] tableEntity;
	private Hashtable tableEntity;
	
	/**
	 * This Container's entity.
	 */
	private Entity entity;

	/**
	 * Create an instance of the tables that belong to this model.
	 * 
	 */
	public �NameS� () {
		java.util.ArrayList x = new java.util.ArrayList ();
		tableEntity = new Hashtable();
�FOREACH (Set[AlmaTable])AlmaTable AS table �
		�table.LowerCase� = new �table.NameS�Table (this);
		x.add((Representable)�table.LowerCase�);
		tableEntity.put("�table.NameS�", new Entity());
�ENDFOREACH�

		//tableEntity = new Entity [table.length];
		//for (int i = 0; i < tableEntity.length; ++i)
		//	tableEntity[i] = new Entity ();
		
		table = new Representable [x.size()];
		table = (Representable[])x.toArray(table);
	
		
		// Define a default entity.
		entity = new Entity();		
		entity.setEntityId((new EntityId("uid://X0/X0/X0")));
		entity.setEntityIdEncrypted("na");
		entity.setEntityTypeName("�NameS�");
		entity.setEntityVersion("1");
		entity.setInstanceVersion("1");
		
		// Define a default creation time.		
		Calendar rightNow = Calendar.getInstance();
		timeOfCreation = new ArrayTime(rightNow.get(Calendar.YEAR), 
		                               							 rightNow.get(Calendar.MONTH)+1, 
		                                						 rightNow.get(Calendar.DAY_OF_MONTH), 
		                                                         rightNow.get(Calendar.HOUR_OF_DAY), 
		                                                         rightNow.get(Calendar.MINUTE), 
		                                                         (double) rightNow.get(Calendar.SECOND));
		                                
		// Archive  XML
		archiveAsBin = false;
		
		// File XML
		fileAsBin = false;				
	}

�FOREACH (Set[AlmaTable])AlmaTable AS table �
	/**
	 * Get the table �table.NameS�.
	 * @return The table �table.NameS� as a �table.NameS�Table.
	 */
	public �table.NameS�Table get�table.NameS� () {
		return �table.LowerCase�;
	}
�ENDFOREACH�

�EXPAND JavaTableAttribute::ReadWriteAttribute FOREACH (Set[ASDMAttribute])ASDMAttributes�

	public byte[] toFITS() throws ConversionException {
		// TODO 
		return null;
	}

	public void fromFITS(byte[] fits) throws ConversionException {
		// TODO 
	}

	public String toVOTable() throws ConversionException {
		// TODO 
		return null;
	}

	public void fromVOTable(String vo) throws ConversionException {
		// TODO 
	}

	private void error() throws ConversionException {
		throw new ConversionException("Invalid xml document","ASDM");
	}
	
	�EXPAND toArchive FOR this�
	�EXPAND fromArchive FOR this�
	
	�EXPAND toXML FOR this�
	�EXPAND fromXML FOR this�
	
	�EXPAND toRelDB FOR this�
	�REM��EXPAND setFromRelDB FOR this��ENDREM�
	
	/**
	 * Get an �NameS� dataset, given the full path name of the 
	 * directory containing the XML version of the dataset.
	 * @param xmlDirectory The full path name of the directory
	 * containing this dataset.
	 * @return The complete dataset that belongs to the container
	 * in this directory.
	 * @throws ConversionException If any error occurs reading the 
	 * files in the directory or in converting the tables from XML.
	 */
	static public �NameS� getFromXML(String xmlDirectory) throws ConversionException {
		File dir = new File(xmlDirectory);
		if (!dir.isDirectory())
			throw new ConversionException ("Directory " + 
					xmlDirectory + " does not exist.", null);
		File file = new File(dir,"�NameS�.xml");
		if (!file.exists())
			throw new ConversionException ("File �NameS�.xml " + 
					"in directory " + xmlDirectory + " does not exist.", 
			"�NameS�.xml");
		BufferedReader in = null;
		StringBuffer xmlDoc = null;
		String line = null;
		try {
			in = new BufferedReader(new FileReader(file));
			//Read the entire file and store in it xmlDoc.
			xmlDoc = new StringBuffer ();
			line = in.readLine();
			while (line != null) {
				xmlDoc.append(line);
				line = in.readLine();
			}
			in.close();
		} catch (IOException e) {
			throw new ConversionException(e.getMessage(),"�NameS�.xml");
		}
		�NameS� dataset = new �NameS� ();
		dataset.fromXML(xmlDoc.toString());

		Entity entity = null; 
 �FOREACH (Set[AlmaTable])AlmaTable AS table�
 		xmlDoc.delete(0, xmlDoc.length());
 		entity = (Entity) dataset.tableEntity.get("�table.NameS�");
 		if (entity.getEntityId() != null) {
 			file = new File(dir, "�table.NameS�.xml");
 			if (!file.exists())
 				throw new ConversionException ("File �table.NameS�.xml  in directory " + xmlDirectory + " does not exist.",  "�table.NameS�");	
 			try {
 				in = new BufferedReader(new FileReader(file));
				//Read the entire file and store in it xmlDoc.
				line = in.readLine();
				while (line != null) {
					xmlDoc.append(line);
					line = in.readLine();
				}
				in.close();
				dataset.get�table.NameS�().fromXML(xmlDoc.toString());			
 			}
 			catch (IOException e) {
				throw new ConversionException(e.getMessage(), "�table.NameS�");
			}
 		}		
 �ENDFOREACH�
 		return dataset;  		
	}
	
   /**
	 * Serialize this into a stream of bytes and encapsulates that stream into a MIME message.
	 * @returns an array of bytes containing the MIME message.
	 * @throws ConversionException.
	 * 
	 */
	public byte[] toMIME() throws ConversionException {
		throw new ConversionException ("The toMIME method is not implemented", "�NameS�");
	}
	
   /*
     * Extracts the binary part of a MIME message and deserialize its content
	 * to fill this with the result of the deserialization. 
	 * @param mimeMsg the array of byte containing the MIME message.
	 * @throws ConversionException
	 */
	 void setFromMIME(byte[]  mimeMsg) throws ConversionException {
	 	throw new ConversionException ("The setFromMIME method is not implemented", "�NameS�");
	 }
	 	
	�EXPAND toFile FOR this�
	�EXPAND setFromFile FOR this�
		
	public Entity getEntity() {
		return entity;
	}

	public void setEntity(Entity e) {
		this.entity = e; 
	}
	
	/**
	 * Meaningless, but required for the Representable interface.
	 */
	public String getName() {
		return null;
	}
	
	/**
	 * Meaningless, but required for the Representable interface.
	 */
	public int size() {
		return 0;
	}

	static private String getXMLEntity(EntityId id) throws ConversionException {
		// TODO		
		return null;
	}
	static private void putXMLEntity(String xml) throws ConversionException {
		// TODO
	}
	

	/**
	 * Update an �NameS� dataset that already exists in the ALMA archive.
	 */
	public void updateArchive() throws ConversionException {
		// Assumption: Entity objects have already been assigned.
		String xml = null;
		// Convert each table to an XML document and write it to the archive.
		for (int i = 0; i < table.length; ++i) {
			xml = table[i].toXML();
			putXMLEntity(xml);
		}
		// Now, convert the container and write it to the archive.
		xml = toXML();
		putXMLEntity(xml);
	}
	
	/**
	 * Return the table, as a Representable object, with the
	 * specified name.
	 */
	public Representable getTable(String tableName) {
		for (int i = 0; i < table.length; ++i)
			if (table[i].getName().equals(tableName))
				return table[i];
		return null;
	}

	/**
	  * Converts this �NameS� into an �NameS�DataSetIDL CORBA structure
	  * @return a �NameS�DataSetIDL.
	  */
	 public �NameS�DataSetIDL toIDL() {
	 	�NameS�DataSetIDL result = new �NameS�DataSetIDL();
 		�FOREACH (Set[AlmaTable])AlmaTable AS t �
  		result.�t.LowerCase� = this.get�t.NameS�().toIDL();
         �ENDFOREACH�
         return result;	 	
	 }
	 
	 /**
	   * Builds an �NameS� out of its IDL representation.
	   * @param x the IDL representation of the �NameS�
	   *
	   * @throws DuplicateKey 
	   * @throws ConversionException
	   * @throws UniquenessViolationException
	   */
	 public void fromIDL(�NameS�DataSetIDL x) throws DuplicateKey,ConversionException, UniquenessViolationException {
		�FOREACH (Set[AlmaTable])AlmaTable AS t �
  		this.�t.LowerCase�.fromIDL(x.�t.LowerCase�);
         �ENDFOREACH�	 	   		
	   }
}
�ENDFILE�
�ENDDEFINE�

�DEFINE toArchive FOR AlmaTableContainer�
	/**
	** Stores an XML representation of the ASDM into the Archive.
	** Each table of the ASDM with a non null number of rows is transformed into its XML representation and stored into the Archive as
	** a separate document. Then the ASDM's container is converted into an XML document containing informations
	** about the tables that it contains (Name, Number of rows, and Archive UIDs for the tables with a non null number of rows) 
	** and stored into the Archive.<br/>
	** The Archive identifiers for the the container and the tables are provided internally.
	**
	** @param ar an IArchiver
	** @return the Archive UID assigned to the XML representation of the container.
	**
	** @note As a side effect the ASDM and its archived tables have their entity private field updated with an entityId assigned to 
	** their XML representations. 
	*/ 
   public EntityId toArchive(IArchiver ar) throws ConversionException, ArchiverException{
        String UID = null;
        Entity e = null;
        try {
            // Get an entityID for the container.
            UID = ar.getID();
            System.out.println("�NameS�.toArchive : UID " + UID + " has been allocated for the container.");
            
            // Update container's entityID.
            e = this.getEntity();
            System.out.println("(Previous container's entityId was "+e.getEntityId().toString()+")");
            e.setEntityId(new EntityId(UID));
            // this.setEntity(e);
        } catch (NotAvailable e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        try {
            // Store each table other than the Pointing table.
	�FOREACH (Set[AlmaTable])AlmaTable AS t �
		 �IF t.NameS != "Pointing"�	 
            if (this.get�t.NameS�().size() > 0) {
 				System.out.print("�NameS�.toArchive : about to archive table �t.NameS�Table");
 				this.get�t.NameS�().toArchive(ar);
 				System.out.println("...archived with Archive uid "+this.get�t.NameS�().getEntity().getEntityId().toString());
 			}
		�ENDIF�	          
	�ENDFOREACH�

	    // And only then the Pointing table.
	    if (this.getPointing().size() > 0) {
 		System.out.print("�NameS�.toArchive : about to archive table PointingTable");
 		this.getPointing().toArchive(ar);
 		System.out.println("...archived with Archive uid "+this.getPointing().getEntity().getEntityId().toString());
 	    }
            
            // Finally store the container.
            System.out.print("�NameS�.toArchive : About to archive the container");
            ar.store(this.toXML(), "�NameS�", UID);
            System.out.println(" ...archived with UID " + UID);
        } catch (ArchiveException e2) {
            // TODO Auto-generated catch block
            throw new ArchiverException("Archiver exception while archiving a ASDMTable. Message was " + e2.getMessage());
        }
 
        return new EntityId(UID);
    }
    
    /**
	** Stores an XML representation of the ASDM into the Archive.
	** This method behaves exactly like toArchive(IArchiver ar) except that it expects the Archive UID of
	** the ASDM's container to be provided by the caller (The Archive identifiers for the tables 
	** are provided internally as in toArchive(IArchiver ar).
	**
	** @param ar an IArchiver
	** @param UID an entityId constructed from an Archive UID.
	**
	** @note As a side effect the ASDM and its archived tables have their entity private field updated with an entityId assigned to 
	** their XML representations. 
	*/ 
    
    public void toArchive(IArchiver ar, EntityId entUID) throws ConversionException, ArchiverException{
        String UID = entUID.toString();
        Entity e = null;

        System.out.print("�NameS�.toArchive : the ASDM's container will be saved with Archive UID "+UID);
            
        // Update container's entityID.
        e = this.getEntity();
        System.out.println("(Previous container's entityId was "+e.getEntityId().toString()+")");
        e.setEntityId(entUID);
        //this.setEntity(e);
 
        try {
            // Store each table.
	�FOREACH (Set[AlmaTable])AlmaTable AS t�
		 �IF t.NameS != "Pointing"�	 
            if (this.get�t.NameS�().size() > 0) {
 				System.out.print("�NameS�.toArchive : About to archive table �t.NameS�Table");
 				this.get�t.NameS�().toArchive(ar);
 				System.out.println("...archived with Archive uid "+this.get�t.NameS�().getEntity().getEntityId().toString());
 			}
		�ENDIF�          
	�ENDFOREACH�

	    // And only then the Pointing table.
	    if (this.getPointing().size() > 0) {
 		System.out.print("�NameS�.toArchive : about to archive table PointingTable");
 		this.getPointing().toArchive(ar);
 		System.out.println("...archived with Archive uid "+this.getPointing().getEntity().getEntityId().toString());
 	    }
            
            // Finally store the container.
            System.out.print("�NameS�.toArchive : About to archive the container");
            ar.store(this.toXML(), "�NameS�", UID);
            System.out.println(" ...archived with UID " + UID);
        } catch (ArchiveException e2) {
            // TODO Auto-generated catch block
            throw new ArchiverException("Archiver exception while archiving a ASDMTable. Message was " + e2.getMessage());
        }
 
        return;
    }
�ENDDEFINE�

�DEFINE fromArchive FOR AlmaTableContainer�	
	/**
	 * Create an �NameS� dataset from the ALMA archive, given the
	 * entityId of its container.
	 * @param ar the IArchiver used to access the Archive.
	 * @param datasetUID The entityId of the container of the dataset.
	 */
	static public �NameS� fromArchive(IArchiver ar, EntityId datasetUID) throws ArchiverException, ConversionException {		
        // Get the xml representation of the container.
        String xml = null;
        try {
        	xml = ar.retrieve(datasetUID.toString());
        }
        catch (ArchiveException e) {
        	throw new ArchiverException("Archiver exception while retrieving a �NameS�  with UID " +datasetUID.toString() +". The message was " + e.getMessage());
        }
        System.out.println("fromArchive: retrieved the ASDM with Archive uid :" + datasetUID.toString());
        
        // Build an empty ASDM...
        �NameS� container = new ASDM();		
		// ... and populate it with parsed XML.
		container.fromXML(xml);
		
		// Now build the tables
		Entity entity = null;
	�FOREACH (Set[AlmaTable])AlmaTable AS table�
		entity = (Entity) container.tableEntity.get("�table.NameS�");
		if (entity.getEntityId() != null) {
			container.get�table.NameS� ().setEntity(entity);
			String tableUID = entity.getEntityId().toString();
			container.get�table.NameS�().fromArchive(ar, tableUID);
			System.out.println("�NameS�.fromArchive: retrieved its "
                    + "�table.NameS�Table" + " using Archive uid :"
                    + tableUID);
		}
		else {
			System.out.println("�NameS�.fromArchive: �table.NameS� is empty.");     		
		}
	�ENDFOREACH�
		return container;
	}
�ENDDEFINE�

�DEFINE toXML FOR AlmaTableContainer�
	/**
	  * Produces the XML representation of this.
	  * @return a string containing the XML representation of this.
	  * @throws ConversionException.
	  */
	public String toXML() throws ConversionException {
		StringBuffer out = new StringBuffer ();
		out.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?> ");
		out.append("<�NameS� xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:�this.xmlnsPrefix�=\"http://Alma/XASDM/ASDM\" xsi:schemaLocation=\"http://Alma/XASDM/�this.NameS� http://almaobservatory.org/XML/XASDM/�this.version�/�this.NameS�.xsd\" schemaVersion=\"�this.version�\" schemaRevision=\"�this.CVSRevision�\"> ");

		if (entity == null)
			throw new ConversionException("Container entity cannot be null.","Container");
		out.append(entity.toXML());
		out.append(" ");
		out.append("<TimeOfCreation> ");
		out.append(timeOfCreation.toFITS());
		out.append(" ");
		out.append("</TimeOfCreation>");
		for (int i = 0; i < table.length; ++i) {
			out.append("<Table> ");
			out.append("<Name> ");
			out.append(table[i].getName());
			out.append(" ");
			out.append("</Name> ");
			out.append("<NumberRows> ");
			out.append(Integer.toString(table[i].size()));
			out.append(" ");
			out.append("</NumberRows> ");
			if (table[i].size() > 0) {
				if (table[i].getEntity() == null)
					throw new ConversionException("Table entity is null.",table[i].getName());
				out.append(table[i].getEntity().toXML());
			}
			out.append("</Table> ");
		}
		out.append("</�NameS�>");
		return out.toString();
	}
�ENDDEFINE�

�DEFINE fromXML FOR AlmaTableContainer�
/**
	 * Parses the  XML representation of an ASDM stored in a string and fills this 
	 * (supposedly empty) with the result of the parsing.
	 * @param xmlDoc The XML representation of the ASDM.
	 * @throws ConversionException If any error occurs reading the 
	 * files in the directory or in converting the tables from XML.
	 */
	public void fromXML(String xmlDoc) throws ConversionException {
		Parser xml = new Parser(xmlDoc);
		if (!xml.isStr("<�NameS�")) 
			error();
		String s = xml.getElement("<Entity",">");
		if (s == null) 
			error();
		Entity e = new Entity();
		e.setFromXML(s);
		if (!e.getEntityTypeName().equals("�NameS�"))
			error();
		setEntity(e);

		s = xml.getElementContent("<TimeOfCreation>","</TimeOfCreation>");
		if (s == null) 
			error();
		timeOfCreation = new ArrayTime(s);

		// Do we have an element startTimeDurationInXML
		s = xml.getElement("<startTimeDurationInXML",">");
		if (s != null) 
			ArrayTimeInterval.readStartTimeDurationInXML(true);
			
		// Do we have an element startTimeDurationInBin
		s = xml.getElement("<startTimeDurationInBin",">");
		if (s != null) 
			ArrayTimeInterval.readStartTimeDurationInBin(true);
				
		// Get each table in the dataset.
		s = xml.getElementContent("<Table>","</Table>");
		Parser tab = null;
		String tableName = null;
		int numberRows = 0;
		Representable entry = null;
		while (s != null) {
			tab = new Parser(s);
			s = tab.getElementContent("<Name>","</Name>");
			if (s == null) 
				error();
			tableName = s;
			s = tab.getElementContent("<NumberRows>","</NumberRows>");
			if (s == null) 
				error();
			try {
				numberRows = Integer.parseInt(s);
			} catch (NumberFormatException err) {
				error();
			}
			if (numberRows > 0 ) {
				s = tab.getElementContent("<Entity",">");
				if (s == null) 
					error();
				e = new Entity();
				e.setFromXML(s);
				if (!e.getEntityTypeName().equals(tableName + "Table"))
					error();
				tableEntity.put(tableName, e);
				// Get the table entry.
				//int i = 0;
				//tableEntity
				//for (; i < table.length; ++i)
				//	if (table[i].getName().equals(tableName)) {
				//		tableEntity[i] = e;
				//		break;
				//	}
				//if (i == table.length)
				//	error();
			}
			s = xml.getElementContent("<Table>","</Table>");
		}
		if (!xml.isStr("</�NameS�>")) 
			error();	
	}
�ENDDEFINE�

�DEFINE toFile FOR AlmaTableContainer�
	/**
	 * Write this ASDM dataset to the specified directory
	 * as a collection of files.
	 *
	 * The container itself is written into an XML file. Each table of the container
	 * having at least one row is written into a binary or an XML file depending on
	 * the value of its "fileAsBin" private field.
	 * 
	 * @param directory The directory to which this dataset is written.
	 * @throws ConversionException If any error occurs in converting the
	 * container or any of its table.  This method will
	 * not overwrite any existing file; a ConversionException is also
	 * thrown in this case.
	 */
	public void toFile(String directory)  throws ConversionException {
	  	// Check if the directory exists
	  	File directoryFile = new File(directory);
	  	if (directoryFile.exists() && !directoryFile.isDirectory())
	  		throw new ConversionException("Cannot write into directory " 
	  			+  directoryFile.getAbsolutePath() 
	  			+ ". This file already exists and is not a directory", "�NameS�");
	  			
	  	//if not let's create it.		
	  	if (!directoryFile.exists()) {
	  		if (!directoryFile.mkdir())
	  			throw new ConversionException("Could not create directory " + directoryFile.getAbsolutePath(), "�NameS�");
	  	}

	  	String fileName = null;
	  	BufferedWriter out = null;
	  	if (fileAsBin) {
	  		// write the bin serialized.
	  		fileName = directory+"/�NameS�.bin";
	  	}
	  	else {
	  		// write the XML
	  		fileName = directory+"/�NameS�.xml";
	  	}
	  	
	  	File file = new File(fileName);
	  	
	  	if (fileAsBin) {
			;
	  	}
	  	else {
	  		try {
	  			out = new BufferedWriter(new FileWriter(file));
	  			out.write(toXML());
	  			out.close();
	  		}
	  		catch (IOException e) {
	  			throw new ConversionException("Problem while writing the XML representation, the message was : " + e.getMessage(), "�NameS�");
	  		}
	  	}
	  	
	  	// Then send each of its table to its own file.
	�FOREACH (Set[AlmaTable])AlmaTable AS table�
		if (get�table.NameS�().size() > 0) {
			get�table.NameS�().toFile(directory);
		}
	�ENDFOREACH�		  			
	  			
	}
�ENDDEFINE�

�DEFINE setFromFile FOR AlmaTableContainer�
	/**
	 * Reads and parses a collection of files as those produced by the toFile method.
	 * This dataset is populated with the result of the parsing.
	 * @param directory The name of the directory containing the files.
	 * @throws ConversionException If any error occurs while reading the 
	 * files in the directory or parsing them.
	 *
	 */	
	 public void setFromFile(String directory) throws ConversionException {
	 	 	File directoryFile = new File(directory);

			if (!directoryFile.isDirectory())
				throw new ConversionException ("Directory " + directory + " does not exist.", "�NameS�");
				
			String fileName = null;
			
			if ( fileAsBin) 
				fileName = directory+"/�NameS�.bin";
			else 
				fileName  = directory+"/�NameS�.xml";
			
			File file = new File(fileName);
				
			if (!file.exists())
				throw new ConversionException ("File " + fileName + " does not exists", "�NameS�");
				
			if (fileAsBin) {
				byte[] bytes = null;
       			try {
					InputStream is = new FileInputStream(file);
					long length = file.length();
					if (length > Integer.MAX_VALUE) 
						throw new ConversionException ("File " + fileName + " is too large", "�NameS�");
				
					bytes = new byte[(int)length];
					int offset = 0;
        			int numRead = 0;
 
        			while (offset < bytes.length && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            			offset += numRead;
        			}
        		
        			if (offset < bytes.length) {
            			throw new ConversionException("Could not completely read file "+file.getName(), "�NameS�");
            		}
            		is.close();
            	}
        		catch (IOException e) {
        			throw new ConversionException("Error while reading "+file.getName()+". The message was " + e.getMessage(),
        																	  "�NameS�");
        		}	
        		
            	setFromMIME(bytes);
			}
			else {
				try {
 					BufferedReader in = new BufferedReader(new FileReader(file));	
 					StringBuffer xmlDoc = new StringBuffer();
 					String line = in.readLine();
					while (line != null) {
						xmlDoc.append(line);
						line = in.readLine();
					}
					in.close();
					fromXML(xmlDoc.toString());
				}
				 catch (IOException e) {
					throw new ConversionException(e.getMessage(), "�NameS�");
				}	
			} 
			// Now read and parse all files for the tables whose number of rows appear as
			// non null in the container just built.
			Entity entity = null;
	�FOREACH (Set[AlmaTable])AlmaTable AS table�
			entity = (Entity) tableEntity.get("�table.NameS�");
			if (entity.getEntityId() != null) {
				get�table.NameS�().setFromFile(directory);
			}
	�ENDFOREACH� 	
	 }	
�ENDDEFINE�

�DEFINE toRelDB FOR AlmaTableContainer�
	public void toRelDB(String UID, boolean commitEveryTable, Logger LOG) {

		/*
		 * Firstly send ASDM's informations to the Rel DB.
		 */
		StringBuffer sb = new StringBuffer("insert into �this.SQLName�");
		sb.append("(");
		sb.append("ID");
		sb.append(", ARCHIVE_UID, TIMEOFCREATION");
		
		�FOREACH (Set[AlmaTable])AlmaTable AS t �
		if (this.get�t.NameS�().size() > 0) {
			sb.append(",�t.SQLName�_NR");
		}
			�IF t.archiveAsBin�
		sb.append(",�t.SQLName�_SQL");
			�ENDIF�
		�ENDFOREACH�
		
		sb.append(")");
		sb.append(" VALUES ");
		sb.append("(");
		sb.append(" asdm_seq.NEXTVAL ");
		sb.append(", '"+UID+"'");
		{
			int[] utc = this.getTimeOfCreation().getDateTime();
			String utc_s = String.format("%d %d %d %d %d %d %d",
						utc[0], utc[1], utc[2], utc[3], utc[4], utc[5], utc[6]);
			sb.append(", to_timestamp('" + utc_s +"', 'YYYY MM DD HH24 MI SS FF9')");					
		}
		
		�FOREACH (Set[AlmaTable])AlmaTable AS t �
		if (this.get�t.NameS�().size() > 0) {
			sb.append(","+this.get�t.NameS�().size());
		}
			�IF t.archiveAsBin�
		sb.append(",0");
			�ENDIF�	
		�ENDFOREACH�
		sb.append(")");
		
	
		final String sql = sb.toString();
		LOG.info("About to execute '"+sql+"'");
		LOG.info("About to record "+UID+" in �this.SQLName�");
		long asdmId = -1;
		try (Connection c = Archive.getConnection()) {
			try {
				PreparedStatement st = c.prepareStatement(sql,  new String[]{"ID"});
				int executeUpdate = st.executeUpdate();
				ResultSet rS = st.getGeneratedKeys();
				rS.next();
				asdmId = rS.getLong(1);
				rS.close();
    			LOG.info("executeUpdate: " + executeUpdate + ", asdmId: " + asdmId);			
			}
			catch (Exception e) {
				LOG.error("Rolling back" , e);
				c.rollback();
			}
		}
		catch (Exception e) {
			LOG.error(" rollback failed?", e);
		}
		
		
		/*
         * ... Then send the tables to the Rel DB.
         */
        �FOREACH (Set[AlmaTable])AlmaTable AS t �
		 	�REM��IF t.NameS != "Pointing" && t.NameS != "TotalPower" �	 �ENDREM�
		if (this.get�t.NameS�().size() > 0) {
			LOG.info("�NameS�.toRelDB : about to archive table �t.NameS�Table");
 			this.get�t.NameS�().toRelDB(asdmId, commitEveryTable, LOG);
 			LOG.info("...archived with ASMD_ID = " + asdmId);
 		}
			�REM��ENDIF��ENDREM�	          
		�ENDFOREACH�
		 
		return;		
	}
�ENDDEFINE�

�DEFINE setFromRelDB FOR AlmaTableContainer�
void setFromRelDB(String UID, Logger LOG) throws ConversionException {
	/*
	 * Obtain the informations about this dataset from �this.SQLName�
	 */
	 final String query = "select * from �this.SQLName� where ARCHIVE_UID ='"+UID+"'";
	 System.out.println("About to execute '"+query+"'");
	 try {
	 	Connection c = Archive.getConnection();
	 	Statement st = c.createStatement();
	 	ResultSet rs = st.executeQuery(query);
	 	if (rs.next()) {
	 		String archiveUID = rs.getString("ARCHIVE_UID");
	 		this.entity = new Entity(archiveUID, "na", "ASDM", "3", "1");
	 	
	 		String timeOfCreation = rs.getTimestamp("TIMEOFCREATION").toString();
	 		this.timeOfCreation = new ArrayTime(timeOfCreation);
	 		
	 		System.out.println(archiveUID + " was created on " + timeOfCreation);
	 		String storageFormat = "";
	�FOREACH (Set[AlmaTable])AlmaTable AS t �
			if (rs.getInt("�this.SQLName�_SQL") != 0 ) {
				storageFormat = "SQL";
			}
			else {
				storageFormat = "NGAS";
			}
	 		System.out.println(archiveUID + " has " + rs.getInt("�this.SQLName�_NR") + " rows in its table �t.NameS� which is stored in " + storageFormat + ".");
	 		if (rs.getInt("�t.SQLName�_NR") != 0) 
				get�t.NameS�().setFromRelDB(archiveUID, LOG);

	�ENDFOREACH�
	 	}
	 	else {
	 		System.out.println("No row found in �this.SQLName� for ARCHIVE_UID='"+UID+"'.");
	 	}
	 }
	 catch (Exception e) {
	 	System.out.println("Big ERROR");
	 	LOG.error("Failed to execute '"+query+"'.", e);
	 	throw new ConversionException("Failed to retrieve the dataset '"+UID+"' from the relational database.", "�this.SQLName�");
	 }
	 
}
�ENDDEFINE�
