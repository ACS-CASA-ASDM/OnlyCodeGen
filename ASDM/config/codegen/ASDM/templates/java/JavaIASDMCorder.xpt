�IMPORT org::openarchitectureware::core::meta::core�
�IMPORT org::openarchitectureware::meta::uml�
�IMPORT org::openarchitectureware::meta::uml::classifier�
�IMPORT alma::hla::datamodel::meta::enumeration�
�IMPORT alma::hla::datamodel::meta::asdm�


�DEFINE Root FOR AlmaTableContainer� 
	�FILE "src/"+DirPath+"/IASDMCorder.java"�
 /* ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File IASDMCorder.java
 */
 
package alma.asdm;
/**
 * An interface to declare the behaviour of a mechanism
 * specialized in the recording of an ASDM to some external
 * format. 
 * The interface's name, after CAMCorder, suggests
 * the idea that an implementation IASDMCorder can record 
 * "on the fly" the ASDM being filled in memory.   
 */
public interface IASDMCorder {
	/**
	 * An embedded enumeration to encode the two possible states 
	 * of an IASDMCorder : active (ON) or inactive (OFF)
	 * When in an active state a IASDMCorder is expected 
	 * to execute the recording statements while when in an
	 * inactive state  it is expected to translate them in noop.
	 */
	public enum State {ON, OFF}; 

	/**
	 * Set the corder to an active state, i.e. a call to a record method
	 * will trigger an actual recording.
	 */
	 public void on();
	 
	/**
	 * Returns true if and only if the IASDMCorder is an 
	 * active state.
	 */
	 public boolean isOn();
	
	/**
	 * Set the corder to a inactive state, i.e. a call to a record method
	 * will be followed by an immediate return without any effect.
	 */
	public void off();
	
	/**
	 * Returns true if and only if the IASDMCorder is in an 
	 * inactive state.
	 */
	public boolean isOff();
	
	/**
	 * Returns the current state of the corder as an IASDMCorder.Status
	 * 
	 */
	public IASDMCorder.State getState();
	
	/**
	 * Ensures that all the contents prepared to be recorded is actually sent.
	 * (in the spirit of a commit executed on a connection for a relational database)
	 * It my happen that this is a NOOP depending on the kind of implementation.
	 */
	public void flush();
	
	/**
	 * Declares tables to be excluded from the record.
	 * 
	 * @param ignored. A string containing of comma separated list
	 * of regular expressions representing table names. The tables
	 * whose names will match one regular expression will be added
	 * to the set of tables excluded from the record already defined.
	 *
	 * Examples :
     * "Pointing, SysCal, CalAtm*" -> Pointing, SysCal and CalAtmosphere
     * "*" -> all the tables
     * "" (or null) -> no table
	 */
	public void ignore(String ignored);
	

	/**
	 * Declares tables to be excluded from the record.
	 * 
	 * @param ignored. A string containing of comma separated list
	 * of regular expressions representing table names. The tables
	 * whose names will match one regular expression will be added
	 * to the set of tables excluded from the record already defined.
	 *
	 * Examples :
     * "Pointing, SysCal, CalAtm*" -> Pointing, SysCal and CalAtmosphere
     * "*" -> all the tables
     * "" (or null) -> no table
	 */	
	public void select(String selected);
	
	/**
	 * Ends a recording session.
	 *
	 * Performs the required tasks to finish correctly
	 * the recording. These tasks depend on the implementation,
	 * but the call should be done systematically. 
	 * (Example : in the case of a recording in a database
	 * via a connection, some last update may have to be done
	 * and then the connection closed).
	 */
	 void done();
	 
		�FOREACH (Set[AlmaTable])AlmaTable AS table�
	/**
	 * Record a row of �table.NameS�
	 */
	public void record(�table.NameS�Row row);
		�ENDFOREACH� 
}	 
	�ENDFILE� 
�ENDDEFINE�