�IMPORT org::openarchitectureware::core::meta::core�
�IMPORT org::openarchitectureware::meta::uml�
�IMPORT org::openarchitectureware::meta::uml::classifier�
�IMPORT alma::hla::datamodel::meta::asdm�

�DEFINE Root FOR TableLink�
	�IF isHasmany || isSlices�
 		�IF !isOptional�
 	/**
 	 * Set �TheAttribute.NameS�[i] with the specified �TheAttribute.SimpleCppType�.
 	 * @param i The index in �TheAttribute.NameS� where to set the �TheAttribute.SimpleCppType� value.
 	 * @param �TheAttribute.NameS� The �TheAttribute.SimpleCppType� value to which �TheAttribute.NameS�[i] is to be set. 
	 		�IF TheAttribute.isKeyPart�
	 * @throws IllegalException
			�ENDIF�
 	 * @throws IndexOutOfBoundsException
  	 */
  	void �Class.NameS�Row::set�TheAttribute.UpperCaseName� (int i, �TheAttribute.SimpleCppType� �TheAttribute.NameS�)  {
  	  	if (hasBeenAdded) {
  	  		�IF TheAttribute.isKeyPart�
			throw IllegalAccessException();
			�ENDIF�
  		}
  		if ((i < 0) || (i > ((int) this->�TheAttribute.NameS�.size())))
  			throw OutOfBoundsException("Index out of bounds during a set operation on attribute �TheAttribute.NameS� in table �Class.NameS�Table");
  		�TheAttribute.CppType�::iterator iter = this->�TheAttribute.NameS�.begin();
  		int j = 0;
  		while (j < i) {
  			j++; iter++;
  		}
  		this->�TheAttribute.NameS�.insert(this->�TheAttribute.NameS�.erase(iter), �TheAttribute.NameS�); 
  	}	
 		�ELSE�
 	/**
 	 * Set �TheAttribute.NameS�[i] with the specified �TheAttribute.SimpleCppType�.
 	 * @param i The index in �TheAttribute.NameS� where to set the �TheAttribute.SimpleCppType� value.
 	 * @param �TheAttribute.NameS� The �TheAttribute.SimpleCppType� value to which �TheAttribute.NameS�[i] is to be set. 
 	 * @throws OutOfBoundsException
  	 */
  	void �Class.NameS�Row::set�TheAttribute.UpperCaseName� (int i, �TheAttribute.SimpleCppType� �TheAttribute.NameS�) {
  		if ((i < 0) || (i > ((int) this->�TheAttribute.NameS�.size())))
  			throw OutOfBoundsException("Index out of bounds during a set operation on attribute �TheAttribute.NameS� in table �Class.NameS�Table");
  		�TheAttribute.CppType�::iterator iter = this->�TheAttribute.NameS�.begin();
  		int j = 0;
  		while (j < i) {
  			j++; iter++;
  		}
  		this->�TheAttribute.NameS�.insert(this->�TheAttribute.NameS�.erase(iter), �TheAttribute.NameS�); 	
  	}
 		�ENDIF�	
	�ENDIF�
	
	�IF isHasa�
		�EXPAND CaseHasa�
	�ELSEIF isSlice�
		�EXPAND CaseSlice�
	�ELSEIF isSlices�
		�EXPAND CaseSlices�
	�ELSEIF isHasmany�
		�EXPAND CaseHasmany�
	�ELSEIF isHasmanyAsSet�
		�EXPAND CaseHasmanyAsSet�
	�ELSE�	
		�IF isOneToOne�
			�EXPAND CaseOne�
		�ENDIF�
		�IF isOptional�
			�EXPAND CaseOptional�
		�ENDIF�
		�IF isOneToMany�
			�EXPAND CaseMany�
		�ENDIF�
	�ENDIF�
�ENDDEFINE�
�REM� -------------- hasa link -----------------------------�ENDREM�
�DEFINE CaseHasa FOR TableLink�

	/**
	 * Returns the pointer to the row in the �Target.NameS� table having �Target.NameS�.�TheAttribute.NameS� == �TheAttribute.NameS�
	 * @return a �Target.NameS�Row*
	 * 
	 �IF isOptional�
	 * throws IllegalAccessException
	 �ENDIF�
	 */
	 �Target.NameS�Row* �Class.NameS�Row::get�Target.NameS�Using�TheAttribute.UpperCaseName�() {
	 �IF isOptional�
	 	if (!�TheAttribute.NameS�Exists)
	 		throw IllegalAccessException();	 		 
	 �ENDIF�
	 	return table.getContainer().get�Target.NameS�().getRowByKey(�TheAttribute.NameS�);
	 }
	 
�ENDDEFINE�

�REM� -------------- Slices link ---------------------------�ENDREM�
�DEFINE CaseSlices FOR TableLink�
�IF !Target.NoAutoIncrementableAttribute�
	// ===> Slices link from a row of �Class.NameS� table to a collection of row of �Target.NameS� table.	
	// vector <int> �TheAttribute.NameS�;
	
	/*
	 ** Append a new id to �TheAttribute.NameS�
	 */
	void �Class.NameS�Row::add�TheAttribute.UpperCaseName�(int id) {
		�TheAttribute.NameS�.push_back(id);
	}
	
	/*
	 ** Append an array of ids to �TheAttribute.NameS�
	 */ 
	void �Class.NameS�Row::add�TheAttribute.UpperCaseName�(vector<int> id) {
		for (unsigned int i = 0; i < id.size(); i++) 
			�TheAttribute.NameS�.push_back(id[i]);
	}
	/**
	 * Get the collection of rows in the �Target.NameS� table having �Opposite.NameS� == �TheAttribute.NameS�[i]
	 */	 
	const vector <�Target.NameS�Row *> �Class.NameS�Row::get�Target.NameS�s(int i) {
		�IF isOptional�
			if (�TheAttribute.NameS�Exists) {
				return table.getContainer().get�Target.NameS�().getRowBy�Target.TheAutoIncrementableAttribute.UpperCaseName�(�TheAttribute.NameS�.at(i));
			}
			else 
				throw IllegalAccessException();
		�ELSE�
			return table.getContainer().get�Target.NameS�().getRowBy�Target.TheAutoIncrementableAttribute.UpperCaseName�(�TheAttribute.NameS�.at(i));
		�ENDIF�		
	}
	
	/** 
	 * Get the collection of pointers to rows in the �Target.NameS� table having �Opposite.NameS� == �TheAttribute.NameS�[i]
	 * for any i in [O..�TheAttribute.NameS�.size()-1].
	 */
	const vector <�Target.NameS�Row *> �Class.NameS�Row::get�Target.NameS�s() {
		�IF isOptional�
			if (!�TheAttribute.NameS�Exists) throw IllegalAccessException();

		�ENDIF�
			vector <�Target.NameS�Row *> result;
			for (unsigned int i=0; i < �TheAttribute.NameS�.size(); i++) {
				vector <�Target.NameS�Row *> current = table.getContainer().get�Target.NameS�().getRowBy�Target.TheAutoIncrementableAttribute.UpperCaseName�(�TheAttribute.NameS�.at(i));
				for (unsigned int j = 0; j < current.size(); j++) 
					result.push_back(current.at(j));
			}					
			return result;
	}
�ENDIF�
�ENDDEFINE�

�REM� -------------- Slice link ---------------------------�ENDREM�
�DEFINE CaseSlice FOR TableLink�
�IF !Target.NoAutoIncrementableAttribute�
	// ===> Slice link from a row of �Class.NameS� table to a collection of row of �Target.NameS� table.
	
	/**
	 * Get the collection of row in the �Target.NameS� table having their attribut �Opposite.NameS� == this->�Opposite.NameS�
	 */
	vector <�Target.NameS�Row *> �Class.NameS�Row::get�Target.NameS�s() {
		�IF isOptional�
			if (�Opposite.NameS�Exists) {
				return table.getContainer().get�Target.NameS�().getRowBy�Target.TheAutoIncrementableAttribute.UpperCaseName�(�Target.TheAutoIncrementableAttribute.NameS�);
			}
			else 
				throw IllegalAccessException();
		�ELSE�
			return table.getContainer().get�Target.NameS�().getRowBy�Target.TheAutoIncrementableAttribute.UpperCaseName�(�Target.TheAutoIncrementableAttribute.NameS�);
		�ENDIF�
	}
�ENDIF�	
�ENDDEFINE�
�REM� -------------- hasmany link ------------------------------�ENDREM�
�DEFINE CaseHasmany FOR TableLink�
/**
 * Append a �TheAttribute.SimpleCppType� to �TheAttribute.NameS�.
 * @param id the �TheAttribute.SimpleCppType� to be appended to �TheAttribute.NameS�
 */
 void �Class.NameS�Row::add�TheAttribute.UpperCaseName�(�TheAttribute.SimpleCppType� id){
 	�TheAttribute.NameS�.push_back(id);
}

/**
 * Append an array of �TheAttribute.SimpleCppType� to �TheAttribute.NameS�.
 * @param id an array of �TheAttribute.SimpleCppType� to be appended to �TheAttribute.NameS�
 */
 void �Class.NameS�Row::add�TheAttribute.UpperCaseName�(const �TheAttribute.CppType�& id) {
 	for (unsigned int i=0; i < id.size(); i++)
 		�TheAttribute.NameS�.push_back(id.at(i));
 }
 

 /**
  * Returns the �TheAttribute.SimpleCppType� stored in �TheAttribute.NameS� at position i.
  *
  */
 const �TheAttribute.SimpleCppType� �Class.NameS�Row::get�TheAttribute.UpperCaseName�(int i) {
 	return �TheAttribute.NameS�.at(i);
 }
 
 /**
  * Returns the �Target.NameS�Row linked to this row via the �TheAttribute.SimpleCppType� stored in �TheAttribute.NameS�
  * at position i.
  */
 �Target.NameS�Row* �Class.NameS�Row::get�Target.NameS�Using�TheAttribute.UpperCaseName�(int i) {
 	return table.getContainer().get�Target.NameS�().getRowByKey(�TheAttribute.NameS�.at(i));
 } 
 
 /**
  * Returns the vector of �Target.NameS�Row* linked to this row via the �TheAttribute.SimpleCppType�s stored in �TheAttribute.NameS�
  *
  */
 vector<�Target.NameS�Row *> �Class.NameS�Row::get�Target.NameS�sUsing�TheAttribute.UpperCaseName�() {
 	vector<�Target.NameS�Row *> result;
 	for (unsigned int i = 0; i < �TheAttribute.NameS�.size(); i++)
 		result.push_back(table.getContainer().get�Target.NameS�().getRowByKey(�TheAttribute.NameS�.at(i)));
 		
 	return result;
 }
  
�ENDDEFINE�

�REM� -------------- hasmanyAsSet ------------------------------�ENDREM�
�DEFINE CaseHasmanyAsSet FOR TableLink�
/**
 * Append a �TheAttribute.SimpleCppType� to �TheAttribute.NameS�.
 * @param id the �TheAttribute.SimpleCppType� to be appended to �TheAttribute.NameS�
 */
 void �Class.NameS�Row::add�TheAttribute.UpperCaseName�(�TheAttribute.SimpleCppType� id) {
 	�TheAttribute.NameS�.insert(id);
 }
 
 /**
 * Append an vector of �TheAttribute.SimpleCppType� to �TheAttribute.NameS�.
 * @param id a vector of �TheAttribute.SimpleCppType� to be appended to �TheAttribute.NameS�
 */
 void �Class.NameS�Row::add�TheAttribute.UpperCaseName�(const vector<�TheAttribute.SimpleCppType�>& id) {
 	for (int i = 0; i < id.size(); i++)
 		�TheAttribute.NameS�.insert(id.at(i));
 } 
 
  /**
  * Returns the vector of �Target.NameS�Row* linked to this row via the �TheAttribute.SimpleCppType�s stored in �TheAttribute.NameS�
  * @return an array of pointers on �Target.NameS�Row.
  */
 vector<�Target.NameS�Row *> �Class.NameS�Row::get�Target.NameS�s() {
 	vector< �Target.NameS�Row* > result;
 	�TheAttribute.CppType�::iterator iter;
 	for (iter=�TheAttribute.NameS�.begin(); iter != �TheAttribute.NameS�.end(); iter++)
 		result.push_back(table.getContainer().get�Target.NameS�().getRowByKey(*iter));
 	return result;	
 } 
�ENDDEFINE�
�REM� -------------- One to One link ---------------------------�ENDREM�

�DEFINE CaseOne FOR TableLink�
	// ===> One to one link from a row of �Class.NameS� table to a row of �Target.NameS� table.
	
	/**
	 * Get the row in table �Target.NameS� by traversing the defined link to that table.
	 * @return A row in �Target.NameS� table.
	 * @throws NoSuchRow if there is no such row in table �Target.NameS�.
	 */
	�Target.NameS�Row *�Class.NameS�Row::get�MethodName�() const {
		return table.getContainer().get�Target.NameS�().getRowBy�TargetKey.UpperCaseName�(�FOREACH (Set[ASDMAttribute])Target.KeyAttributesSet AS attr SEPARATOR ","� �attr.NameS� �ENDFOREACH�);
	}
	
�ENDDEFINE�

�REM� -------------- Optional link ---------------------------�ENDREM�

�DEFINE CaseOptional FOR TableLink�
	// ===> Optional link from a row of �Class.NameS� table to a row of �Target.NameS� table.

	/**
	 * The link to table �Target.NameS� is optional. Return true if this link exists.
	 * @return true if and only if the �Target.NameS� link exists. 
	 */
	bool �Class.NameS�Row::is�MethodName�Exists() const {
		return �ArgExists�;
	}

	/**
	 * Get the optional row in table �Target.NameS� by traversing the defined link to that table.
	 * @return A row in �Target.NameS� table.
	 * @throws NoSuchRow if there is no such row in table �Target.NameS� or the link does not exist.
	 */
	�Target.NameS�Row *�Class.NameS�Row::get�MethodName�() const {
�FOREACH (Set[ASDMAttribute])Attribute AS attr �
		if (!�attr.NameS�Exists) {
			throw NoSuchRow("�Target.NameS�","�Class.NameS�",true);
		}
�ENDFOREACH�
		return table.getContainer().get�Target.NameS�().getRowBy�TargetKey.UpperCaseName�(�FOREACH (Set[ASDMAttribute])Target.KeyAttributesSet AS attr SEPARATOR ","� �attr.NameS� �ENDFOREACH�);
	}
	
	/**
	 * Set the values of the link attributes needed to link this row to a row in table �Target.NameS�.
	 */
	void �Class.NameS�Row::set�MethodName�Link(�ArgDeclaration�) {
�FOREACH (Set[ASDMAttribute])Attribute AS attr �
		this->�attr.NameS� = �attr.NameS�;
		�attr.NameS�Exists = true;
�ENDFOREACH�
	}


�ENDDEFINE�

�REM� -------------- One to Many link ---------------------------�ENDREM�

�DEFINE CaseMany FOR TableLink�
  �IF isOneToZeroOrMany�
	// ===> One to many link from a row of �Class.NameS� table to 0 or more rows of �Target.NameS� table.
  �ENDIF�
  �IF isOneToOneOrMany�
	// ===> One to many link from a row of �Class.NameS� table to 1 or more rows of �Target.NameS� table.
  �ENDIF�
	
�IF !NoExtrinsic�
	/**
	 * Get the number of links to �MethodName� in this row.
	 */
	unsigned int �Class.NameS�Row::getSize�MethodName�() const {
		return linkTo�MethodName�List.size();
	}
	
	/**
	 * Get a row of �Target.NameS� linked to this row using link number N.
	 * @param n The number of the link to be used in getting the row.
	 */
	�Target.NameS�Row *�Class.NameS�Row::get�MethodName�(unsigned int n) const  {
		if (n < 0 || n > linkTo�MethodName�List.size())
			throw NoSuchRow(n,"�Target.NameS�","�Class.NameS�");
		LinkTo�MethodName� x = (LinkTo�MethodName�)linkTo�MethodName�List[n];
		return table.getContainer().get�Target.NameS�().get�TargetKey.UpperCaseName�(�FieldNames�);
	}
	
	/**
	 * Get all rows of �Target.NameS� that are linked to this row.
	 */
	vector<�Target.NameS�Row *> �Class.NameS�Row::get�MethodName�() const  {
		vector<�Target.NameS�Row *> result;
		LinkTo�MethodName� x;
		for (unsigned int i = 0; i < linkTo�MethodName�List.size(); ++i) {
			x = (LinkTo�MethodName�)linkTo�MethodName�List[i];
			result.push_back(table.getContainer().get�Target.NameS�().get�TargetKey.UpperCaseName�(�FieldNames�));
		}
		return result;
	}
	
	/**
	 * Get row of �Target.NameS� linked to this row using values for �TargetKey.UpperCaseName�.
	 */
	�Target.NameS�Row *�Class.NameS�Row::get�MethodName��TargetKey.UpperCaseName�(�ArgDeclarationSimple�) const  {
		LinkTo�MethodName� x;
		for (unsigned int i = 0; i < linkTo�MethodName�List.size(); ++i) {
			x = (LinkTo�MethodName�)linkTo�MethodName�List[i];
			if (�FieldComparison�)
				return table.getContainer().get�Target.NameS�().get�TargetKey.UpperCaseName�(�FieldNames�);
		}
		throw NoSuchRow("","�Target.NameS�","�Class.NameS�");
	}

�IF !GetOnly�
	/**
	 * Delete link number N to table �Target.NameS�.
	 * @param n The number of the link to be deleted.
	 */
	void �Class.NameS�Row::remove�MethodName�(unsigned int n) {
		vector<LinkTo�MethodName�>::iterator iter;
		iter = linkTo�MethodName�List.begin();
		for (unsigned int i = 0; i < linkTo�MethodName�List.size(); ++i) {
			if (i == n) {
				linkTo�MethodName�List.erase(iter);
				break;
			} else
				iter++;
		}
	}

	/**
	 * Delete all links to table �Target.NameS�.
	 */
	void �Class.NameS�Row::remove�MethodName�() {
		linkTo�MethodName�List.clear();
	}

	/**
	 * Delete link to table �Target.NameS� based on �TargetKey.UpperCaseName�.
	 */
	void �Class.NameS�Row::remove�MethodName��TargetKey.UpperCaseName�(�ArgDeclarationSimple�) {
		LinkTo�MethodName� x;
		for (unsigned int i = 0; i < linkTo�MethodName�List.size(); ++i) {
			x = (LinkTo�MethodName�)linkTo�MethodName�List[i];
			if (�FieldComparison�) {
				remove�MethodName�(i);
				return;
			}
		}
		throw NoSuchRow("","�Target.NameS�","�Class.NameS�");
	}

	/**
	 * Add a new link to a row of �Target.NameS� to this row.  Parameters to this method
	 * are the values of variables that make up the key used to traverse the link.
	 */
	void �Class.NameS�Row::add�MethodName�Link(�ArgDeclarationSimple�) {
		LinkTo�MethodName� *x = new LinkTo�MethodName� ();
�FOREACH (Set[ASDMAttribute])Attribute AS k �
		x->�k.SimpleName� = �k.SimpleName�;
�ENDFOREACH�
		linkTo�MethodName�List.push_back(*x);
	}

	/**
	 * Add an array of new links to a row of �Target.NameS� to this row.
	 * Parameters to this method are arrrays of values of variables that make 
	 * up the keys used to traverse the links.
	 */
	void �Class.NameS�Row::add�MethodName�Link(�CppArgArrayDeclarationSimple�) {
		unsigned int n = �FirstNameSimple�.size();
�FOREACH (Set[ASDMAttribute])Attribute AS k �
		if (�k.SimpleName�.size() != n)
			throw new InvalidArgumentException("In Config:addLinkToMain: All arrays must be the same length!");
�ENDFOREACH�
		LinkTo�MethodName� *x;
		for (unsigned int i = 0; i < n; ++i) {
			x = new LinkTo�MethodName�();
�FOREACH (Set[ASDMAttribute])Attribute AS k �
			x->�k.SimpleName� = �k.SimpleName�[i];
�ENDFOREACH�
			linkTo�MethodName�List.push_back(*x);
		}
	}
�ENDIF�

�FOREACH (Set[ASDMAttribute])Attribute AS k �
	/**
	 * Get all values of the link attibute �k.NameS�.
	 */
	�k.CppType� �Class.NameS�Row::get�k.UpperCaseName�() const {
		vector<�k.SimpleCppType�> x(linkTo�MethodName�List.size());
		for (unsigned int i = 0; i < x.size(); ++i)
			x[i] = ((LinkTo�MethodName�)linkTo�MethodName�List[i]).get�k.SimpleUpperCaseName�();
		return x;
	}
�ENDFOREACH�	

�ELSE�
  �IF NumberAttributesLessThanTarget�
	/**
	 * Get all rows of �Target.NameS� that are linked to this row.
	 */
	vector<�Target.NameS�Row *> �Class.NameS�Row::get�MethodName�() const  {
		return table.getContainer().get�Target.NameS�().get�TargetKey.UpperCaseName�(�ArgList�);
	}
  �ELSE�
	/**
	 * Get all rows of �Target.NameS� that are linked to this row.
	 */
	�Target.NameS�Row *�Class.NameS�Row::get�MethodName�() const  {
		return table.getContainer().get�Target.NameS�().get�TargetKey.UpperCaseName�(�ArgList�);
	}
  �ENDIF�
�ENDIF�

�ENDDEFINE�


