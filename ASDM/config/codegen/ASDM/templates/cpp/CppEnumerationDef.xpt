�IMPORT org::openarchitectureware::core::meta::core�
�IMPORT org::openarchitectureware::meta::uml�
�IMPORT org::openarchitectureware::meta::uml::classifier�
�IMPORT alma::hla::datamodel::meta::enumeration�

�DEFINE Root FOR EEnumeration�
	�FILE "include/"+NameS+".h"�
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File �NameS�.h
 */	
#ifndef �NameS�_CLASS
#define �NameS�_CLASS
#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <sstream>
using namespace std;

#include <�NameS�.h>
#include <IDLConversionException.h>
#include <XMLConversionException.h>
#ifndef WITHOUT_ACS
#include <enumerationsIDLC.h>
using namespace enumerationsIDL;
#endif

/**
  * \namespace enumerations
  * 
  * A namespace for all the classes related with enumerations.
  *
  */
namespace enumerations {
/**
   * �this.Documentation�
   *
   * <TABLE  BORDER="1">
   * <TR BGCOLOR="#AAAAAA"> <TD> Name </TD> <TD> Value returned by value() </TD> </TR>
   	�FOREACH this.EEnumerationLiteral AS literal�
   * 	<TR><TD> �literal.getName� </TD><TD>�IF this.isString�"�ENDIF��literal.getValue��IF this.isString�"�ENDIF�  </TD></TR>
   	�ENDFOREACH�
   *   <TR> <TD> UNKNOWN </TD><TD>�IF this.isString�"UNKNOWN"�ELSEIF this.isInt�-2**31�ENDIF�  </TD></TR>
   * </TABLE>
   */
class �NameS� {
 public:
 
	  /**
	    * Compares this enum with the specified object for order.
	    * 
	    * Returns a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified �NameS�. Enum constants are only comparable to other enum constants of the same enum type.
	    *  The natural order implemented by this method is the order in which the constants are declared.
	    *
	    * @param �NameS� e A pointer to the �NameS� to be compared.
	    * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified �NameS�.
	    */
	    int compareTo(�NameS�* e);
	    
 #ifndef WITHOUT_ACS  
 	  /**
	    * A static method which returns a pointer to a �NameS� from a �NameS�IDL.
	    *
	    * @throw IDLConversionException
	    */
	   static  const �NameS�* getFromIDL(const �NameS�IDL & ); 
 		
 	 	  	
	/**
	  * A method to "IDLify" this.
	  * @return a struct �NameS�IDL
	  *
	  */
	  �NameS�IDL toIDL() const;
#endif

 	/**
 	  * Returns the name of this enum constant, exactly as declared in its enum declaration.
 	  * 
 	  * @return a string
 	  */
 	  string name() const ;
 	  
 	 /**
 	   * Returns the ordinal of this enumeration constant (its position in its enum declaration, where the initial constant is assigned an ordinal of zero).
 	   *
 	   * @return an unsigned int.
 	   */
 	   unsigned int ordinal() ;
 	  
 	  	  	   
		/**
		  * Returns a string representation of this.
		  * The returned string has the form name[=value] (e.g. CROSS=0 or HANNING).
		  *
		  * @return a string.
		  */
 	 virtual string toString() const;
 	 

	  	  /** 
		  	* Returns an XML representation of this.
		  	*
			* the returned string has  the form &lt;<i>element</i>&gt; <i>literal_name</i> "&lt;<i>/element</i> &gt;
			* 
			* Note that the value (if any) associated to the literal does not appear in the instance. This value should  be defined in XML schema associated to this XML instance.
			* @param element The name of the XML element to use in the XML element representation.
			*/
	 virtual string toXML(const string & element) const;

	  /**
	    * A static method which returns a pointer to a �NameS� from its XML representation.
	    *
	    * @param element the name of the XML element to be parsed.
	    * @param xml a string containing the XML representation to be parsed, at its beginning position. 
	    * @return a �NameS�
	    *
	    * @throws XMLConversionException
	    */
	  static const �NameS�* getFromXML(const string& element, const string& xml);
	    		   
  	/**
	  * Returns a pointer to a  �NameS�  enum constant with the specified name.
	  * @param name The name of the constant to return.
	  * @return a pointer to a  �NameS� enum constant or 0 if no constant exists for that name.
	  *
	  */
  	static  const �NameS�* valueOf(const string& name);
 
 		�IF this.isString�
	/**
	  * Returns the value of this as a string.
	  * @return a string.
	  *
	  */
	string value() const  ;
		�ELSEIF this.isInt�
		
	/**
	  * Returns the value of this as an int.
	  *
	  */
	int value() const ;
		�ENDIF�  	
  	
  	/**
  	  * Returns a vector containing pointers to the constants of this enum type, in the order they're declared.
  	  *
  	  * @return a vector<�NameS�* >
  	  */
  	 static const vector<�NameS�*>& values();
  
  
 	/**
	  * The �NameS� enum constant for UNKNOWN.
	  */ 
  	static  �NameS�* const UNKNOWN;
  

 	�FOREACH this.EEnumerationLiteral AS literal�
 	/**
 	  * The �NameS� enum constant for �literal.getName� (�IF this.isString�"�ENDIF��literal.getValue��IF this.isString�"�ENDIF�)
 	  */
 	  static �NameS�* const  �literal.getName�;
   	�ENDFOREACH�
   	
   	/**
   	  * \enum E
   	  * An embedded  C++ enum to have the possibility of using switch control structure. The value returned by the method ordinal can be checked against its literal constants :
   	  * 
   	  * �this.NameS�* ec = one in { �FOREACH this.EEnumerationLiteral AS literal� �this.NameS�::�literal.getName� , �ENDFOREACH�  �this.NameS�::UNKNOWN}  ;
   	  *
   	  * switch (ec->ordinal()) {
   	  * �FOREACH this.EEnumerationLiteral AS literal�
   	  * case �this.NameS�::_�literal.NameS� : 
   	  * 	do something
   	  *    break;
   	  * �ENDFOREACH�
   	  * case �this.NameS�::_UNKOWN :
   	  * 	do something
   	  *		break;
   	  * }
   	  * 
   	  */
   	  enum E { �FOREACH this.EEnumerationLiteral AS literal� _�literal.getName� , �ENDFOREACH�  _UNKNOWN};
 

 private:
  string name_;
  �IF this.isInt�
  int value_;
  �ELSEIF this.isString�
  string value_;
  �ENDIF�
  
  static const int MIN_INT = 0x80000000;
  
 static unsigned int nextOrdinal;
  
  unsigned int ordinal_;
  
 static  ostringstream oss;
  
  static map<string,�NameS�*> name2�NameS�;
  static vector <�NameS� *> values_;
  
  virtual ~�NameS�() {};
  �IF this.isString�
  �NameS�(string name, string value);
  �ELSEIF this.isInt�
  �NameS�(string name, int value);
  �ENDIF�
  
  class �NameS�Mgr {
  public:
  	~�NameS�Mgr () {
  	if (�NameS�::UNKNOWN) delete �NameS�::UNKNOWN;
�FOREACH this.EEnumerationLiteral AS literal �
	if (�NameS�:: �literal.getName�) delete �NameS�:: �literal.getName�;
�ENDFOREACH�	  	
  	};
  	
  	�NameS�Mgr() {;};
  	
  private :
  	�NameS�Mgr(const �NameS�Mgr&);
  	�NameS�Mgr & operator=(const �NameS�Mgr&);
  };
  static �NameS�Mgr mgr;
  
};
} // end namespace enumerations
#endif /* �NameS�_CLASS */

�ENDFILE�
�ENDDEFINE�
