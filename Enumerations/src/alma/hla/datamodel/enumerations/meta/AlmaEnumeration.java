package alma.hla.datamodel.enumerations.meta;

import org.openarchitectureware.core.meta.core.Element;
import org.openarchitectureware.core.meta.core.ElementSet;
import org.openarchitectureware.meta.uml.classifier.Enumeration;
import org.openarchitectureware.meta.uml.classifier.EnumerationLiteral;

import alma.hla.datamodel.enumerations.util.AlmaModel;

import java.util.HashMap;
import java.util.Iterator;

public class AlmaEnumeration extends Enumeration {

	
	private ElementSet aeLits = new ElementSet();
	private HashMap<String, String> s2s = new HashMap<String, String>();
	
	// public boolean isInteger() {
	// String thisLit = "";
	// for (Iterator i=Literal().iterator();i.hasNext();) {
	// thisLit = ((EnumerationLiteral)i.next()).NameS();
	// try {
	// Integer.parseInt(thisLit);
	// } catch (NumberFormatException e) {
	// return false;
	// }
	// return true;
	// }
	// return false; // Don't see how this can happen. (alma.chk checks for > 0
	// literals)
	// }

	private EnumerationLiteral versionLiteral() {
		for (Iterator it = Literal().iterator(); it.hasNext();) {
			EnumerationLiteral eLit = (EnumerationLiteral) it.next();
			if (((AlmaEnumerationLiteral) eLit).isVersionLiteral()) {
				return eLit;
			}
		}
		return null;
	}

	public int version() {
		EnumerationLiteral eLit = versionLiteral();
		return Integer.parseInt(eLit.NameS().split("=")[1]);
	}
	
	public String xmlns() {
		for (Iterator it = Literal().iterator(); it.hasNext();) {
			EnumerationLiteral eLit = (EnumerationLiteral) it.next();
			if (((AlmaEnumerationLiteral) eLit).isXMLNSLiteral()) {
				return eLit.NameS().split("=")[1];
			}
		}
		return null;
	}

	public String revision() {
		return AlmaModel.CvsRevision();
	}

	public String release() {
		return AlmaModel.release;
	}
	
	/**
	 * 
	 * @return an ElementSet of literals which are "true" literals 
	 * i.e. their names do not begin with a '$' (a convention) .
	 * 
	 */
	public ElementSet AlmaLiteral() {
		if (aeLits.size() == 0) {
			int index = 0;
			for (Iterator it = Literal().iterator(); it.hasNext();) {
				Object o = it.next();
				if (!((AlmaEnumerationLiteral) o).NameS().startsWith("$")) {
					aeLits.add(o);
					((AlmaEnumerationLiteral) o).index(Integer.toString(index));
					index = index + 1;
				}
			}
		}
		return aeLits;
	}
	
	public String size() {
		return Integer.toString(AlmaLiteral().size());
	}

	public String LatexName() {
		return formatForLatex(NameS());
	}

	public String SQLName() {
		return "E_"+NameS();	
	}
	
	private String formatForLatex(String x) {

		return x.replaceAll("_", "\\\\_");
	}
	
	public String position(String literalName) {
		return (s2s.get(literalName));
	}
}
