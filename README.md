Synopsis.
=========

This project contains all the applications and source files required to construct the hierarchy of files related to the Enumerations and ASDM, in the form expected by :

* the ALMA development environment.
* the CASA development environment.

Motivation.
===========

To create one unique repository of files containing :
* the result of code generation applied to models (one for the Enumerations and one for the ASDM)
* a set of handwritten files.

that both ALMA and CASA can use to obtain their source files.

Requirements.
=============

A java SDK version 1.8 and the environment variable JAVA_HOME defined for this environment. E.g.

```
volte:OnlyCodegen michel$ printenv | grep JAVA_HOME
JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_60.jdk/Contents/Home
```

or

```
[caillat@romeo onlycodegen]$ printenv JAVA_HOME
/usr/java/jdk1.8.0_77/
[caillat@romeo onlycodegen]$ 
```

General structure.
==================

Here is an overview of the hierarchy of files under the top level directory of the project, (``OnlyCodeGen``) :
 
```
.
|-- ASDM                        <--- The directory dedicated to the production of the ASDM related files.
|   |-- build.xml                   <--- The ASDM specific ant file.
|   |-- config
|   |-- doc
|   |-- idl
|   |-- include
|   |-- lib
|   |-- notGenerated
|   |-- src
|-- Enumerations                <--- The directory dedicated to the production of the ASDM related files.
|   |-- build.xml                   <--- The Enumerations specific ant file.
|   |-- config
|   |-- doc
|   |-- idl
|   |-- include
|   |-- lib
|   |-- notGenerated
|   --- src
|-- README.md                   <--- This file.
|-- ant
|   |-- bin
|   --- lib
|-- build.xml                   <--- The toplevel ant file. It'll mostly delegate the work to the ASDM's and Enumeration's ant files.
--- lib                         <--- Utility java libraries.
    |-- antlr-3.0.1.jar
    |-- antlr-generator-3.0.1.jar
    |-- antlr.jar
    |-- castor.jar
    |-- commons-cli-1.2.jar
    |-- commons-collections-3.2.1.jar
    |-- commons-lang-2.5.jar
    |-- commons-logging-1.1.1.jar
    |-- dom4j-1.6.1.jar
    |-- endorsed
    |-- jACSUtil.jar
    |-- jakarta-oro-2.0.5.jar
    |-- jakarta-regexp-1.2.jar
    |-- jaxen-1.1.3.jar
    |-- jdom.jar
    |-- log4j-1.2.15.jar
    |-- xalan.jar
    --- xmljbind.jar
```

How to produce the code.
========================

* Change directory to the top level directory of the project:
```
cd <..>/OnlyCodeGen
```

* Launch the code production :
```
./ant/bin/ant -Denv.ANT_HOME=`pwd`/ant
```

* The result is a directory ``product`` located under the top level directory of the project. It contains two subdirectories :
	* one for the files presented *a la* ALMA :
	
```
./OnlyCodegen/product/ALMA
--- ICD
    --- HLA
        |-- ASDM
        |   |-- doc
        |   |-- idl
        |   |-- include
        |   --- src
        --- Enumerations
            |-- doc
            |-- idl
            |-- include
            --- src
	
```
	
   * one for the files presented *a la* CASA: 
	
```
./OnlyCodegen/product/CASA
--- alma
    |-- ASDM
    |-- Enumerations
    --- Enumtcl
```

